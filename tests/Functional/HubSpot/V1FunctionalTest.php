<?php

namespace Test\Authorify\Functional\HubSpot;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class V1FunctionalTest extends WebTestCase
{
    public function testSuccess()
    {
        $client = self::createClient();

        $params = [
            'firstname' => 'Bart-symfony-test',
            'zip' => '32233',
            'email' => 'bart.simpsons@smartagents.com',
            'phone' => '+1.3867526534',
            'best_meet_time' => 'asap',
            '_id' => 'CheckAvailability',
            '_pageUrl' => 'http://www.authorify.com/alt-integration-test',
            '_pageName' => 'Authorify (alt-integration-test)',
            'affiliate_source' => 'source 2'
        ];

        // TODO: mock HubSpot client requests.
        $client->xmlHttpRequest('POST', '/hubspot/forms/v1', $params);

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"status":"success","message":"Thank you!","data":{"result":null,"statusCode":204,"redirectUrl":"https:\/\/authorify-local.com\/forms\/check-availability-thank-you?msg=Thank+you%21"}}', $response->getContent());
    }
}