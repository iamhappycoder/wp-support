<?php

namespace Tests\Authorify\Functional\LimeLight;

use Authorify\Provider\LimeLight\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Authorify\Functional\WebTestCaseTrait;
use Tests\Authorify\Mother\Provider\LimeLight\OrdersEndpointMother;

class AddonMultipleFunctionalTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testAddonMultipleFailureEmailMissing(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['email']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testAddonMultipleFailureEmailEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['email'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testAddonMultipleFailureEmailInvalid(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['email'] = 'bart.simpsons[at]smartagents.com';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testAddonMultipleFailurePreviousOrderIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['previousOrderId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/ ', $params);

        $this->assertEquals('Please enter a valid previous order ID.', $response['message']);
    }

    public function testAddonMultipleFailurePreviousOrderIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['previousOrderId'] = 'x';
        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/x', $params);

        $this->assertEquals('Please enter a valid previous order ID.', $response['message']);
    }

    public function testAddonMultipleFailureProductIdEmptyString(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'] = ['' => $params['offers'][7]];

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }


    public function testAddonMultipleFailureProductIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'] = ['x' => $params['offers'][7]];

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testAddonMultipleFailureProductQtyMissing(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['offers'][7]['productQty']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonMultipleFailureProductQtyEmptyString(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'][7]['productQty'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonMultipleFailureProductQtyContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'][7]['productQty'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonMultipleFailureOfferIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['offers'][7]['offerId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testAddonMultipleFailureOfferIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['offers'][7]['offerId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testAddonMultipleFailureOfferIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'][7]['offerId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testAddonMultipleFailureBillingModelIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['offers'][7]['billingModelId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testAddonMultipleFailureBillingModelIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'][7]['billingModelId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testAddonMultipleFailureBillingModelIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['offers'][7]['billingModelId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testAddonMultipleFailureShippingIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['shippingId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testAddonMultipleFailureShippingIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['shippingId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testAddonMultipleFailureShippingIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['shippingId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testAddonMultipleFailureCampaignIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        unset($params['campaignId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testAddonMultipleFailureCampaignIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['campaignId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testAddonMultipleFailureCampaignIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonMultipleParamsOneProduct();
        $params['campaignId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon-multiple/1', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testAddonMultipleOrderSuccessSingleProduct(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16353',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'f10a651240a202c6e25128bb072a8018',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $this->client = self::createClient();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order_card_on_file',
                [
                    'campaignId' => 4,
                    'shippingId' => 4,
                    'previousOrderId' => 16353,
                    'products' =>
                        [
                            7 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                    'initializeNewSubscription' => 1,
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16354',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.99',
                    'orderSalesTaxPercent' => '0.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            7 => '787eb3e25d9759b104626dfc78d98339',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon-multiple/' . $orderId, OrdersEndpointMother::getAddonMultipleParamsOneProduct());

        $this->assertEquals('success', $addonResponse['status']);
        $this->assertEquals('Approved', $addonResponse['data']['resp_msg']);
    }

    public function testAddonMultipleOrderSuccessMultipleProducts(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16355',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'd36eb83d9079092cac70e3efc9b66008',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $this->client = self::createClient();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order_card_on_file',
                [
                    'campaignId' => 4,
                    'shippingId' => 4,
                    'previousOrderId' => $orderId,
                    'products' =>
                        [
                            7 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                            14 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                    'initializeNewSubscription' => 1,
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16356',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.99',
                    'orderSalesTaxPercent' => '0.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            7 => '05fc231e219501029579f4ad9f9d0a1a',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon-multiple/' . $orderId, OrdersEndpointMother::getAddonMultipleParamsTwoProducts());

        $this->assertEquals('success', $addonResponse['status']);
        $this->assertEquals('Approved', $addonResponse['data']['resp_msg']);
    }
}
