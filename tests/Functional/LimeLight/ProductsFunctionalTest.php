<?php

namespace Tests\Authorify\Functional\LimeLight;

use Authorify\Provider\LimeLight\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Authorify\Functional\WebTestCaseTrait;

class ProductsFunctionalTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testAddonFailureIdEmpty(): void
    {
        $response = $this->request('GET', '/v1/limelight/products/ ');

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testAddonFailureIdContainsNonDigitCharacter(): void
    {
        $response = $this->request('GET', '/v1/limelight/products/x');

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testGetByProductIdFailure(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'product_index',
                [
                    'product_id' =>
                        [
                            0 => 777777,
                        ],
                ]
            )
            ->willReturn(
                [
                    'response_code' => '600',
                    'product_name' => '',
                    'product_description' => '',
                    'product_sku' => '',
                    'product_price' => '',
                    'product_category_name' => '',
                    'vertical_name' => '',
                    'product_is_trial' => '',
                    'product_is_shippable' => '',
                    'product_rebill_product' => '',
                    'product_rebill_days' => '',
                    'product_max_quantity' => '',
                    'preserve_recurring_quantity' => '',
                    'subscription_type' => '',
                    'subscription_week' => '',
                    'subscription_day' => '',
                    'cost_of_goods_sold' => '',
                    'product_id' => '0',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('GET', '/v1/limelight/products/777777');

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('600', $response['data']['response_code']);
    }

    public function testGetByProductIdSuccess(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'product_index',
                [
                    'product_id' =>
                        [
                            0 => 7,
                        ],
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_products' => '1',
                    'product_id' =>
                        [
                            0 => '7',
                        ],
                    'products' =>
                        [
                            7 =>
                                [
                                    'response_code' => '100',
                                    'product_name' => 'Sandbox - Intel i7-8700k',
                                    'product_description' => 'IntelÂ® Core i7-8700K Processor (12M Cache, up to 4.70 GHz]',
                                    'product_sku' => 'sandbox-intel-i7-8700k',
                                    'product_price' => '0.99',
                                    'product_category_name' => 'Sandbox - Processors',
                                    'vertical_name' => 'Computer & Electronics',
                                    'product_is_trial' => '0',
                                    'product_is_shippable' => '0',
                                    'product_rebill_product' => '0',
                                    'product_rebill_days' => '0',
                                    'product_max_quantity' => '100',
                                    'preserve_recurring_quantity' => '0',
                                    'subscription_type' => '0',
                                    'subscription_week' => '',
                                    'subscription_day' => '',
                                    'cost_of_goods_sold' => '0.00',
                                    'product_id' => '7',
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('GET', '/v1/limelight/products/7');

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('100', $response['data']['response_code']);
    }
}
