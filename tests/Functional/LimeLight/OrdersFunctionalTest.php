<?php

namespace Tests\Authorify\Functional\LimeLight;

use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Exception\ServerException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Authorify\Functional\WebTestCaseTrait;
use Tests\Authorify\Mother\Endpoint\LimeLight\CalculateTotalEndpointMother;
use Tests\Authorify\Mother\Provider\LimeLight\OrdersEndpointMother;

class OrdersFunctionalTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testCreateOrderUnrecognizedParameter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['warpFactor'] = '7';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('The parameter warpFactor is not recognized.', $response['message']);
    }

    public function testCreateFailureFirstNameMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['firstName']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureFirstNameEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['firstName'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureFirstNameGreaterThanMaxCharacters(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['firstName'] = str_pad('bart', 51, 'x');

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureLastNameMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['lastName']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureLastNameEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['lastName'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureLastNameGreaterThanMaxCharacters(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['lastName'] = str_pad('simpson', 51, 'x');

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailurePhoneMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['phone']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailurePhoneEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['phone'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailurePhoneLessThanMinCharacters(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['phone'] = '123456789';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailurePhoneGreaterThanMaxCharacters(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['phone'] = '123456789012';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailureEmailMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['email']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testCreateFailureEmailEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['email'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['creditCardNumber']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberLessThanMinimumDigits(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '11112222333';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberMoreThanMaximumDigits(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '11112222333344445555';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureExpirationMonthMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['expirationMonth']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationMonth'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthInvalidCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationMonth'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthInvalidMonth(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationMonth'] = '13';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['expirationYear']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationYear'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearInvalidCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationYear'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearInvalidYear(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationYear'] = '1';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthAndYearValidatedBeforeCvv(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['expirationMonth'] = '';
        $params['expirationYear'] = '';
        $params['cvv'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureCvvMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['cvv']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testCreateFailureCvvEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['cvv'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testCreateFailureCvvContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['cvv'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testCreateFailureBillingAddress1Missing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['billingAddress1']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a billing address.', $response['message']);
    }

    public function testCreateFailureBillingAddress1Empty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingAddress1'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a billing address.', $response['message']);
    }

    public function testCreateFailureBillingCityMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['billingCity']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a city.', $response['message']);
    }

    public function testCreateFailureBillingCityEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingCity'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a city.', $response['message']);
    }

    public function testCreateFailureBillingCountryMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['billingCountry']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please select a country.', $response['message']);
    }

    public function testCreateFailureBillingCountryEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingCountry'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please select a country.', $response['message']);
    }

    public function testCreateFailureBillingStateMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['billingState']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingStateEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingState'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingStateInvalidUsState(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingCountry'] = 'us';
        $params['billingState'] = 'ab';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingStateInvalidCanadianState(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingCountry'] = 'ca';
        $params['billingState'] = 'al';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingZipMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['billingZip']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureBillingZipEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingZip'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureBillingZipInvalidUsZip(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingCountry'] = 'us';
        $params['billingZip'] = 'K1W 0B1';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureBillingZipInvalidCanadianZip(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingCountry'] = 'ca';
        $params['billingState'] = 'ab';
        $params['billingZip'] = '12345';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureProductIdMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['productId']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductIdEmptyString(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['productId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductIdEmptyArray(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['productId'] = [];

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['productId'][0] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductQtyMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['productQty']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureProductQtyEmptyString(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['productQty'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureProductQtyEmptyArray(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['productQty'] = [];

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureProductQtyContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['productQty'][0] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureOfferIdMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['offerId']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testCreateFailureOfferIdEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['offerId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testCreateFailureOfferIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['offerId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testCreateFailureBillingModelIdMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['billingModelId']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testCreateFailureBillingModelIdEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingModelId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testCreateFailureBillingModelIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['billingModelId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testCreateFailureShippingIdMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['shippingId']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testCreateFailureShippingIdEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['shippingId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testCreateFailureShippingIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['shippingId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testCreateFailureCampaignIdMissing(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        unset($params['campaignId']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testCreateFailureCampaignIdEmpty(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['campaignId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testCreateFailureCampaignIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params['campaignId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testCreateFailureFlowIsInvalid(): void
    {
        $params = OrdersEndpointMother::getCreateParamsWithInvalidFlow();

        $response = $this->request('POST', '/v1/limelight/subscription', $params);

        $this->assertEquals('Invalid flow mode.', $response['message']);
    }

    /**
     * @dataProvider dataProviderInvalidUtmValues
     */
    public function testCreateFailureInvalidUtmValues($utmName, $utmValue): void
    {
        $params = OrdersEndpointMother::getCreateParams();
        $params[$utmName] = $utmValue;

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('Invalid UTM character', $response['message']);
    }

    public function testCreateOrderServerError(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 1234567890,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willThrow(new ServerException('Invalid shipping id of (1234567890) found - order canceled.', 10322, null, 'Invalid shipping id of (1234567890) found - order canceled.'));

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParamsWithInvalidProductId());

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Invalid shipping id of (1234567890) found - order canceled.', $response['message']);
        $this->assertEquals('10322', $response['code']);
    }

    public function testCreateOrderAddress2NotSent(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16371',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => '3ffcf92aa20fdc5a9f6badabd3305a46',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParamsAddress2NotSent());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCreateOrderDeclined(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'declinetest3@authorify.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233330000',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willThrow(new ServerException('This transaction has been declined', 10800, null, 'This transaction has been declined'));

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParamsDeclined());

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('This transaction has been declined', $response['data']['declineReason']);
    }

    public function testCreateOrderFailureProductIdIsNotAnArray(): void
    {
        $params = OrdersEndpointMother::getCreateParamsWithScalarProductId();

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateOrderFailureProductQtIsNotAnArray(): void
    {
        $params = OrdersEndpointMother::getCreateParamsWithScalarProductQty();

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateOrderSuccessSingleProduct(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16374',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'd27f3cb1b447579b3dce86929fb63352',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCreateOrderSuccessMultipleProducts(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                            14 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16375',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '240.42',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '15.40',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'db77443b2f49dd01f38eebe001b6d2ba',
                            14 => '29afe9c518957d28312f2fa99b2d61cc',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParamsMultipleProductIds());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCreateOrderSuccessProductIdAndQtyAreNotArrays(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16376',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'aa13bd99946f25ffd644c122511be13a',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getCreateParamsWithScalarProductIdAndQty();

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCreateOrderSuccessNoAffiliateId(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16377',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => '3bf9b47809b8c5a4d43ab1e9ac063e92',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getCreateParamsWithScalarProductIdAndQty();
        unset($params['affiliateId']);

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCreateOrderSuccessWithUtmParameters(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                    'AFFID' => 'source',
                    'C1' => 'campaign',
                    'C2' => 'medium',
                    'C3' => 'term',
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16377',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => '3bf9b47809b8c5a4d43ab1e9ac063e92',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getCreateParamsWithUtmParameters();

        $response = $this->request('POST', '/v1/limelight/orders', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCreateOrderSuccessSingleBumpProduct(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            44 =>
                                [
                                    'offer_id' => 37,
                                    'billing_model_id' => 8,
                                    'quantity' => 1,
                                    'trial' =>
                                        [
                                            'product_id' => 47,
                                        ],
                                ],
                            20 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16374',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'd27f3cb1b447579b3dce86929fb63352',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParamsWithProductBump());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testCalculateTotalSuccess(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_total/calculate',
                [
                    'campaign_id' => '4',
                    'shipping_id' => '4',
                    'use_tax_provider' => '1',
                    'offers' =>
                        [
                            0 =>
                                [
                                    'id' => '14',
                                    'product_id' => '18',
                                    'billing_model_id' => '2',
                                    'quantity' => 1,
                                ],
                        ],
                    'location' =>
                        [
                            'state' => 'FL',
                            'country' => 'US',
                            'postal_code' => '32102',
                        ],
                ],
                '2'
            )
            ->willReturn(
                [
                    'status' => 'SUCCESS',
                    'data' =>
                        [
                            'total' => '5.54',
                            'subtotal' => '0.50',
                            'line_items' =>
                                [
                                    0 =>
                                        [
                                            'id' => 18,
                                            'name' => 'Sandbox - Book Credits - 10',
                                            'qty' => 1,
                                            'base_price' => '0.50',
                                            'unit_price' => '0.50',
                                            'total' => '0.50',
                                            'is_taxable' => true,
                                            'is_shippable' => false,
                                            'is_prepaid' => false,
                                        ],
                                ],
                            'shipping' => '5.00',
                            'tax' =>
                                [
                                    'pct' => '7.00',
                                    'total' => '0.04',
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = CalculateTotalEndpointMother::getWithFloridaLocationLowerCaseStateAndCountry();

        $response = $this->request('GET', '/v1/limelight/orders/total/calculate', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('5.54', $response['data']['data']['total']);
        $this->assertEquals(['pct' => '7.00', 'total' => '0.04'], $response['data']['data']['tax']);
    }

    public function testFindOrderFailureCampaignIdMissing(): void
    {
        $params = OrdersEndpointMother::getFindOrderParams();
        unset($params['campaignId']);

        $response = $this->request('GET', '/v1/limelight/orders/find', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testFindOrderFailureCampaignIdEmpty(): void
    {
        $params = OrdersEndpointMother::getFindOrderParams();
        $params['campaignId'] = '';

        $response = $this->request('GET', '/v1/limelight/orders/find', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testFindOrderFailureCampaignIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getFindOrderParams();
        $params['campaignId'] = 'x';

        $response = $this->request('GET', '/v1/limelight/orders/find', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testFindOrderFailureEmailMissing(): void
    {
        $params = OrdersEndpointMother::getFindOrderParams();
        unset($params['email']);

        $response = $this->request('GET', '/v1/limelight/orders/find', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testFindOrderFailureEmailEmpty(): void
    {
        $this->markTestSkipped();

        $params = OrdersEndpointMother::getFindOrderParams();
        $params['email'] = '';

        $response = $this->request('GET', '/v1/limelight/orders/find', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testFindOrderFailureNoOrdersFound(): void
    {
        $this->markTestSkipped();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'recurring' => '1',
                            'email' => 'non-existent@email.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '333',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getFindOrderParams();
        $params['email'] = 'non-existent@email.com';

        $response = $this->request('GET', '/v1/limelight/orders/find', $params);

        $this->assertEquals('The email address you entered does not match our records. Please try a different email address or contact support at 904-695-9933.', $response['message']);
    }

    public function testFindOrderSuccessSingleProduct(): void
    {
        $this->markTestSkipped();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'recurring' => '1',
                            'email' => 'findtest@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '1',
                    'order_id' => '16257',
                    'data' =>
                        [
                            16257 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16257',
                                    'acquisition_date' => '2019-04-10 15:45:06',
                                    'ancestor_id' => '13772',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'City 1',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '22',
                                    'billing_first_name' => 'Find',
                                    'billing_last_name' => 'Test',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => 'Street 1',
                                    'billing_street_address2' => 'Apartment',
                                    'campaign_id' => '4',
                                    'cc_expires' => '0120',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '50',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'findtest@authorify.com',
                                    'first_name' => 'Find',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Test',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15740',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-29',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'Find',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Test',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-28 00:00:57',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-29',
                                                    'subscription_id' => 'd25246616756a092a844541c4488a8e4',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 28, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/29/2019.',
                                            1 => 'June 28, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                        ],
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('GET', '/v1/limelight/orders/find', OrdersEndpointMother::getFindOrderParams());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('100', $response['data']['response_code']);
    }

    public function testUpdateOrderOrderUnrecognizedParameter(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['warpFactor'] = '7';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('The parameter warpFactor is not recognized.', $response['message']);
    }

    public function testUpdateOrderFailureFirstNameMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['firstName']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testUpdateOrderFailureFirstNameEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['firstName'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testUpdateOrderFailureFirstNameGreaterThanMaxCharacters(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['firstName'] = str_pad('bart', 51, 'x');

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testUpdateOrderFailureLastNameMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['lastName']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testUpdateOrderFailureLastNameEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['lastName'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testUpdateOrderFailureLastNameGreaterThanMaxCharacters(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['lastName'] = str_pad('simpson', 51, 'x');

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testUpdateOrderFailurePhoneMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['phone']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testUpdateOrderFailurePhoneEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['phone'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testUpdateOrderFailurePhoneLessThanMinCharacters(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['phone'] = '123456789';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testUpdateOrderFailurePhoneGreaterThanMaxCharacters(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['phone'] = '123456789012';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testUpdateOrderFailureEmailMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['email']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testUpdateOrderFailureEmailEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['email'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testUpdateOrderFailureCreditCardNumberMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['creditCardNumber']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testUpdateOrderFailureCreditCardNumberEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['creditCardNumber'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testUpdateOrderFailureCreditCardNumberContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['creditCardNumber'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testUpdateOrderFailureCreditCardNumberLessThanMinimumDigits(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['creditCardNumber'] = '11112222333';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testUpdateOrderFailureCreditCardNumberMoreThanMaximumDigits(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['creditCardNumber'] = '11112222333344445555';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationMonthMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['expirationMonth']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationMonthEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationMonth'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationMonthInvalidCharacter(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationMonth'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationMonthInvalidMonth(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationMonth'] = '13';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationYearMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['expirationYear']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationYearEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationYear'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationYearInvalidCharacter(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationYear'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationYearInvalidYear(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationYear'] = '1';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureExpirationMonthAndYearValidatedBeforeCvv(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['expirationMonth'] = '';
        $params['expirationYear'] = '';
        $params['cvv'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testUpdateOrderFailureCvvMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['cvv']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testUpdateOrderFailureCvvEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['cvv'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testUpdateOrderFailureCvvContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['cvv'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testUpdateOrderFailureBillingAddress1Missing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['billingAddress1']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a billing address.', $response['message']);
    }

    public function testUpdateOrderFailureBillingAddress1Empty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingAddress1'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a billing address.', $response['message']);
    }

    public function testUpdateOrderFailureBillingCityMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['billingCity']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a city.', $response['message']);
    }

    public function testUpdateOrderFailureBillingCityEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingCity'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a city.', $response['message']);
    }

    public function testUpdateOrderFailureBillingCountryMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['billingCountry']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please select a country.', $response['message']);
    }

    public function testUpdateOrderFailureBillingCountryEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingCountry'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please select a country.', $response['message']);
    }

    public function testUpdateOrderFailureBillingStateMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['billingState']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testUpdateOrderFailureBillingStateEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingState'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testUpdateOrderFailureBillingStateInvalidUsState(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingCountry'] = 'us';
        $params['billingState'] = 'ab';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testUpdateOrderFailureBillingStateInvalidCanadianState(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingCountry'] = 'ca';
        $params['billingState'] = 'al';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testUpdateOrderFailureBillingZipMissing(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['billingZip']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testUpdateOrderFailureBillingZipEmpty(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingZip'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testUpdateOrderFailureBillingZipInvalidUsZip(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingCountry'] = 'us';
        $params['billingZip'] = 'K1W 0B1';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testUpdateOrderFailureBillingZipInvalidCanadianZip(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingCountry'] = 'ca';
        $params['billingState'] = 'ab';
        $params['billingZip'] = '12345';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testUpdateOrderFailureCustomerNotFound(): void
    {
        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['email'] = 'becca.pramheda@authorify.com';

        $expectedResponse = [
            'status' => 'failure',
            'message' => 'The email address you entered does not match our records. Please try a different email address or contact support at 904-695-9933.',
            'code' => '0',
            'data' => null,
        ];

        $actualResponse = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals($expectedResponse, $actualResponse);

    }

    /**
     * Requires the HubSpot contact noordertest1@authorify.com to be present.
     */
    public function testUpdateOrderFailureNoOrdersFound(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'email' => 'noordertest1@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                  'response_code' => '333',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['email'] = 'noordertest1@authorify.com';

        $actualResponse = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('failure', $actualResponse['status']);
        $this->assertStringContainsString('An internal error has occurred. Please contact support at 904-695-9933.', $actualResponse['message']);
        $this->assertRegExp('/Error ID: \d{14}-\d{10}/', $actualResponse['message']);
    }

    public function testUpdateOrderSuccessBillingAddress2Missing(): void
    {
        $this->markTestSkipped();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                  'start_date' => '01/01/1971',
                  'end_date' => '12/31/2099',
                  'campaign_id' => 'all',
                  'criteria' =>
                  [
                    'recurring' => '1',
                    'email' => 'updatetest@authorify.com',
                  ],
                  'search_type' => 'all',
                  'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '2',
                    'order_id' =>
                        [
                            0 => '16010',
                            1 => '16117',
                        ],
                    'data' =>
                        [
                            16010 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16010',
                                    'acquisition_date' => '2019-04-11 07:52:48',
                                    'ancestor_id' => '13816',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => '0',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '138',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15985',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-11',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'Atlantic Beach',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => '51 Pine St',
                                    'shipping_street_address2' => 'some value',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-10 10:10:39',
                                    'tracking_number' => '',
                                    'transaction_id' => '0',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-11',
                                                    'subscription_id' => 'f7b11caed063283858e1adbfee08216e',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 10, 2019 10:10 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/11/2019.',
                                            1 => 'June 10, 2019 10:10 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed First Name from Jasper to James by API User limelighttest',
                                            3 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Last Name from Ferrer to Kirk by API User limelighttest',
                                            4 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                            16117 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16117',
                                    'acquisition_date' => '2019-04-11 07:51:38',
                                    'ancestor_id' => '13814',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '2',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => '',
                                    'next_subscription_product_id' => '',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '14892',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-13',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment 1',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-12 00:00:43',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-13',
                                                    'subscription_id' => 'ee5f354e51a52881bb7af037cf8f1164',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 12, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/13/2019.',
                                            1 => 'June 12, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            3 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            4 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            7 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            8 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            9 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            10 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            11 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address2 from some value to Apartment 1 by API User limelighttest',
                                            12 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            13 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            14 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            15 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            16 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            17 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            18 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            19 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            20 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            21 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            22 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            23 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            24 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            25 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            26 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            27 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            28 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            29 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            30 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            31 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                        ],
                ]
            );

        $client
            ->request(
                'authorize_payment',
                [
                    'billingFirstName' => 'James',
                    'billingLastName' => 'Kirk',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'phone' => '1234567890',
                    'email' => 'updatetest@authorify.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '1299',
                    'CVV' => '123',
                    'ipAddress' => '127.0.0.1',
                    'productId' => 22,
                    'campaignId' => 8,
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'error_found' => '0',
                    'error_message' => 'This transaction has been approved',
                    'transactionID' => 'Not Available',
                    'authId' => 'Not Available',
                    'gateway_id' => '7',
                    'prepaid' => '',
                    'prepaid_match' => 'No',
                ]
            );

        $client
            ->request(
                'order_update',
                [
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' => 'James',
                                    'last_name' => 'Kirk',
                                    'phone' => '1234567890',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_address1' => '51 Pine St',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_state' => 'FL',
                                    'billing_zip' => '32233',
                                    'billing_country' => 'US',
                                    'cc_number' => '4111222233334444',
                                    'cc_expiration_date' => '1299',
                                    'cc_payment_type' => 'visa',
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'response_code' => '911',
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'phone' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address1' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_city' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_state' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_zip' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_country' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_number' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_expiration_date' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_payment_type' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParams();
        unset($params['billingAddress2']);

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('success', $response['status']);
    }

    public function testUpdateOrderSuccessBillingAddress2Empty(): void
    {
        $this->markTestSkipped();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'recurring' => '1',
                            'email' => 'updatetest@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '2',
                    'order_id' =>
                        [
                            0 => '16010',
                            1 => '16117',
                        ],
                    'data' =>
                        [
                            16010 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16010',
                                    'acquisition_date' => '2019-04-11 07:52:48',
                                    'ancestor_id' => '13816',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => '0',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '138',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15985',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-11',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'Atlantic Beach',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => '51 Pine St',
                                    'shipping_street_address2' => 'some value',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-10 10:10:39',
                                    'tracking_number' => '',
                                    'transaction_id' => '0',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-11',
                                                    'subscription_id' => 'f7b11caed063283858e1adbfee08216e',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 10, 2019 10:10 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/11/2019.',
                                            1 => 'June 10, 2019 10:10 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed First Name from Jasper to James by API User limelighttest',
                                            3 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Last Name from Ferrer to Kirk by API User limelighttest',
                                            4 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                            16117 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16117',
                                    'acquisition_date' => '2019-04-11 07:51:38',
                                    'ancestor_id' => '13814',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '2',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => '',
                                    'next_subscription_product_id' => '',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '14892',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-13',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment 1',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-12 00:00:43',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-13',
                                                    'subscription_id' => 'ee5f354e51a52881bb7af037cf8f1164',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 12, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/13/2019.',
                                            1 => 'June 12, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            3 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            4 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            7 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            8 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            9 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            10 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            11 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address2 from some value to Apartment 1 by API User limelighttest',
                                            12 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            13 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            14 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            15 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            16 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            17 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            18 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            19 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            20 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            21 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            22 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            23 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            24 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            25 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            26 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            27 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            28 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            29 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            30 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            31 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                        ],
                ]
            );

        $client
            ->request(
                'authorize_payment',
                [
                    'billingFirstName' => 'James',
                    'billingLastName' => 'Kirk',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'phone' => '1234567890',
                    'email' => 'updatetest@authorify.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '1299',
                    'CVV' => '123',
                    'ipAddress' => '127.0.0.1',
                    'productId' => 22,
                    'campaignId' => 8,
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'error_found' => '0',
                    'error_message' => 'This transaction has been approved',
                    'transactionID' => 'Not Available',
                    'authId' => 'Not Available',
                    'gateway_id' => '7',
                    'prepaid' => '',
                    'prepaid_match' => 'No',
                ]
            );

        $client
            ->request(
                'order_update',
                [
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' => 'James',
                                    'last_name' => 'Kirk',
                                    'phone' => '1234567890',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_address1' => '51 Pine St',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_state' => 'FL',
                                    'billing_zip' => '32233',
                                    'billing_country' => 'US',
                                    'cc_number' => '4111222233334444',
                                    'cc_expiration_date' => '1299',
                                    'cc_payment_type' => 'visa',
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'response_code' => '911',
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'phone' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address1' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_city' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_state' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_zip' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_country' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_number' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_expiration_date' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_payment_type' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['billingAddress2'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('success', $response['status']);
    }

    public function testUpdateOrderSuccessEmailIsUrlEncoded(): void
    {
        $this->markTestSkipped();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'email' => 'updatetest@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '2',
                    'order_id' =>
                        [
                            0 => '16010',
                            1 => '16117',
                        ],
                    'data' =>
                        [
                            16010 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16010',
                                    'acquisition_date' => '2019-04-11 07:52:48',
                                    'ancestor_id' => '13816',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => '0',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '138',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15985',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-11',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'Atlantic Beach',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => '51 Pine St',
                                    'shipping_street_address2' => 'some value',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-10 10:10:39',
                                    'tracking_number' => '',
                                    'transaction_id' => '0',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-11',
                                                    'subscription_id' => 'f7b11caed063283858e1adbfee08216e',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 10, 2019 10:10 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/11/2019.',
                                            1 => 'June 10, 2019 10:10 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed First Name from Jasper to James by API User limelighttest',
                                            3 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Last Name from Ferrer to Kirk by API User limelighttest',
                                            4 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                            16117 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16117',
                                    'acquisition_date' => '2019-04-11 07:51:38',
                                    'ancestor_id' => '13814',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '2',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => '',
                                    'next_subscription_product_id' => '',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '14892',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-13',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment 1',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-12 00:00:43',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-13',
                                                    'subscription_id' => 'ee5f354e51a52881bb7af037cf8f1164',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 12, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/13/2019.',
                                            1 => 'June 12, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            3 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            4 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            7 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            8 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            9 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            10 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            11 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address2 from some value to Apartment 1 by API User limelighttest',
                                            12 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            13 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            14 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            15 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            16 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            17 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            18 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            19 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            20 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            21 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            22 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            23 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            24 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            25 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            26 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            27 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            28 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            29 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            30 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            31 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                        ],
                ]
            );

        $client
            ->request(
                'authorize_payment',
                ["billingFirstName" => "James", "billingLastName" => "Kirk", "billingAddress1" => "51 Pine St", "billingCity" => "Atlantic Beach", "billingState" => "FL", "billingZip" => "32233", "billingCountry" => "US", "phone" => "1234567890", "email" => "updatetest@authorify.com", "creditCardType" => "visa", "creditCardNumber" => "4111222233334444", "expirationDate" => "1299", "CVV" => "123", "ipAddress" => "127.0.0.1", "productId" => 22, "campaignId" => 8, "billingAddress2" => "some value"]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'error_found' => '0',
                    'error_message' => 'This transaction has been approved',
                    'transactionID' => 'Not Available',
                    'authId' => 'Not Available',
                    'gateway_id' => '7',
                    'prepaid' => '',
                    'prepaid_match' => 'No',
                ]
            );

        $client
            ->request(
                'order_update',
                ["order_id" => [16117 => ["first_name" => "James", "last_name" => "Kirk", "phone" => "1234567890", "billing_first_name" => "James", "billing_last_name" => "Kirk", "billing_address1" => "51 Pine St", "billing_address2" => "some value", "billing_city" => "Atlantic Beach", "billing_state" => "FL", "billing_zip" => "32233", "billing_country" => "US", "cc_number" => "4111222233334444", "cc_expiration_date" => "1299", "cc_payment_type" => "visa"]]]
            )
            ->willReturn(
                [
                    'response_code' => '911',
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'phone' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address1' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_city' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_state' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_zip' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_country' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_number' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_expiration_date' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_payment_type' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParamsEmailIsUrlEncoded();

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);
        $this->assertEquals('success', $response['status']);
    }

    /**
     * Authorize credit card.
     */
    public function testUpdateOrderSuccessNotDeclined(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'email' => 'updatetest@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '2',
                    'order_id' =>
                        [
                            0 => '16010',
                            1 => '16117',
                        ],
                    'data' =>
                        [
                            16010 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16010',
                                    'acquisition_date' => '2019-04-11 07:52:48',
                                    'ancestor_id' => '13816',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => '0',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '138',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15985',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-11',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'Atlantic Beach',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => '51 Pine St',
                                    'shipping_street_address2' => 'some value',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-10 10:10:39',
                                    'tracking_number' => '',
                                    'transaction_id' => '0',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-11',
                                                    'subscription_id' => 'f7b11caed063283858e1adbfee08216e',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 10, 2019 10:10 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/11/2019.',
                                            1 => 'June 10, 2019 10:10 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed First Name from Jasper to James by API User limelighttest',
                                            3 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Last Name from Ferrer to Kirk by API User limelighttest',
                                            4 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                            16117 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16117',
                                    'acquisition_date' => '2019-04-11 07:51:38',
                                    'ancestor_id' => '16010',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '2',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => '',
                                    'next_subscription_product_id' => '',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '14892',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-13',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment 1',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-12 00:00:43',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-13',
                                                    'subscription_id' => 'ee5f354e51a52881bb7af037cf8f1164',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 12, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/13/2019.',
                                            1 => 'June 12, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            3 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            4 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            7 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            8 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            9 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            10 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            11 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address2 from some value to Apartment 1 by API User limelighttest',
                                            12 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            13 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            14 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            15 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            16 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            17 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            18 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            19 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            20 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            21 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            22 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            23 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            24 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            25 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            26 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            27 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            28 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            29 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            30 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            31 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                        ],
                ]
            );

        $client
            ->request(
                'authorize_payment',
                [
                    'billingFirstName' => 'James',
                    'billingLastName' => 'Kirk',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'phone' => '1234567890',
                    'email' => 'updatetest@authorify.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '1299',
                    'CVV' => '123',
                    'ipAddress' => '127.0.0.1',
                    'productId' => 22,
                    'campaignId' => 8,
                    'billingAddress2' => 'some value',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'error_found' => '0',
                    'error_message' => 'This transaction has been approved',
                    'transactionID' => 'Not Available',
                    'authId' => 'Not Available',
                    'gateway_id' => '7',
                    'prepaid' => '',
                    'prepaid_match' => 'No',
                ]
            );

        $client
            ->request(
                'order_update',
                [
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' => 'James',
                                    'last_name' => 'Kirk',
                                    'phone' => '1234567890',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_address1' => '51 Pine St',
                                    'billing_address2' => 'some value',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_state' => 'FL',
                                    'billing_zip' => '32233',
                                    'billing_country' => 'US',
                                    'cc_number' => '4111222233334444',
                                    'cc_expiration_date' => '1299',
                                    'cc_payment_type' => 'visa',
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'response_code' => '911',
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'phone' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address1' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address2' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_city' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_state' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_zip' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_country' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_number' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_expiration_date' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_payment_type' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['email'] = 'updatetest@authorify.com';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Credit card details updated.', $response['message']);
    }

    /**
     * Authorize credit card.
     */
    public function testUpdateOrderSuccessDeclinedAndAncestorNotOnHold(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'email' => 'updatetest@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '2',
                    'order_id' =>
                        [
                            0 => '16010',
                            1 => '16117',
                        ],
                    'data' =>
                        [
                            16010 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16010',
                                    'acquisition_date' => '2019-04-11 07:52:48',
                                    'ancestor_id' => '13816',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => '0',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '138',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15985',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-11',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'Atlantic Beach',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => '51 Pine St',
                                    'shipping_street_address2' => 'some value',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-10 10:10:39',
                                    'tracking_number' => '',
                                    'transaction_id' => '0',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-11',
                                                    'subscription_id' => 'f7b11caed063283858e1adbfee08216e',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 10, 2019 10:10 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/11/2019.',
                                            1 => 'June 10, 2019 10:10 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed First Name from Jasper to James by API User limelighttest',
                                            3 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Last Name from Ferrer to Kirk by API User limelighttest',
                                            4 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                            16117 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16117',
                                    'acquisition_date' => '2019-04-11 07:51:38',
                                    'ancestor_id' => '16010',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '2',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => '',
                                    'next_subscription_product_id' => '',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '7',
                                    'order_total' => '0.99',
                                    'parent_id' => '14892',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-13',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment 1',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-12 00:00:43',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-13',
                                                    'subscription_id' => 'ee5f354e51a52881bb7af037cf8f1164',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 12, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/13/2019.',
                                            1 => 'June 12, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            3 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            4 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            7 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            8 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            9 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            10 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            11 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address2 from some value to Apartment 1 by API User limelighttest',
                                            12 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            13 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            14 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            15 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            16 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            17 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            18 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            19 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            20 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            21 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            22 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            23 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            24 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            25 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            26 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            27 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            28 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            29 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            30 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            31 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                        ],
                ]
            );

        $client
            ->request(
                'authorize_payment',
                [
                    'billingFirstName' => 'James',
                    'billingLastName' => 'Kirk',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'phone' => '1234567890',
                    'email' => 'updatetest@authorify.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '1299',
                    'CVV' => '123',
                    'ipAddress' => '127.0.0.1',
                    'productId' => 22,
                    'campaignId' => 8,
                    'billingAddress2' => 'some value',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'error_found' => '0',
                    'error_message' => 'This transaction has been approved',
                    'transactionID' => 'Not Available',
                    'authId' => 'Not Available',
                    'gateway_id' => '7',
                    'prepaid' => '',
                    'prepaid_match' => 'No',
                ]
            );

        $client
            ->request(
                'order_update',
                [
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' => 'James',
                                    'last_name' => 'Kirk',
                                    'phone' => '1234567890',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_address1' => '51 Pine St',
                                    'billing_address2' => 'some value',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_state' => 'FL',
                                    'billing_zip' => '32233',
                                    'billing_country' => 'US',
                                    'cc_number' => '4111222233334444',
                                    'cc_expiration_date' => '1299',
                                    'cc_payment_type' => 'visa',
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'response_code' => '911',
                    'order_id' =>
                        [
                            16117 =>
                                [
                                    'first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'phone' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_first_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_last_name' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address1' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_address2' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_city' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_state' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_zip' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'billing_country' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_number' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_expiration_date' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                    'cc_payment_type' =>
                                        [
                                            'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                        ],
                                ],
                        ],
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['email'] = 'updatetest@authorify.com';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Credit card details updated.', $response['message']);
    }

    /**
     * Authorize credit card.
     */
    public function testUpdateOrderSuccessDeclinedAndAncestorOnHold(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'order_find',
                [
                    'start_date' => '01/01/1971',
                    'end_date' => '12/31/2099',
                    'campaign_id' => 'all',
                    'criteria' =>
                        [
                            'email' => 'updatetest@authorify.com',
                        ],
                    'search_type' => 'all',
                    'return_type' => 'order_view',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'total_orders' => '2',
                    'order_id' =>
                        [
                            0 => '16010',
                            1 => '16117',
                        ],
                    'data' =>
                        [
                            16010 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16010',
                                    'acquisition_date' => '2019-04-11 07:52:48',
                                    'ancestor_id' => '13816',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => '0',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '138',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                    'next_subscription_product_id' => '7',
                                    'on_hold' => '1',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '2',
                                    'order_total' => '0.99',
                                    'parent_id' => '15985',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-11',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'Atlantic Beach',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => '51 Pine St',
                                    'shipping_street_address2' => 'some value',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-10 10:10:39',
                                    'tracking_number' => '',
                                    'transaction_id' => '0',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '1',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-11',
                                                    'subscription_id' => 'f7b11caed063283858e1adbfee08216e',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 10, 2019 10:10 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/11/2019.',
                                            1 => 'June 10, 2019 10:10 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed First Name from Jasper to James by API User limelighttest',
                                            3 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Last Name from Ferrer to Kirk by API User limelighttest',
                                            4 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 10, 2019 10:28 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                            16117 =>
                                [
                                    'response_code' => '100',
                                    'order_id' => '16117',
                                    'acquisition_date' => '2019-04-11 07:51:38',
                                    'ancestor_id' => '16010',
                                    'affiliate' => '',
                                    'afid' => '',
                                    'sid' => '',
                                    'affid' => '',
                                    'c1' => '',
                                    'c2' => '',
                                    'c3' => '',
                                    'aid' => '',
                                    'opt' => '',
                                    'amount_refunded_to_date' => '0.00',
                                    'auth_id' => 'Not Available',
                                    'billing_city' => 'Atlantic Beach',
                                    'billing_country' => 'US',
                                    'billing_cycle' => '2',
                                    'billing_first_name' => 'James',
                                    'billing_last_name' => 'Kirk',
                                    'billing_postcode' => '32233',
                                    'billing_state' => 'FL',
                                    'billing_state_id' => 'FL',
                                    'billing_street_address' => '51 Pine St',
                                    'billing_street_address2' => 'some value',
                                    'campaign_id' => '4',
                                    'cc_expires' => '1299',
                                    'cc_first_6' => '411122',
                                    'cc_last_4' => '4444',
                                    'cc_number' => '4444',
                                    'credit_card_number' => '411122******4444',
                                    'cc_orig_first_6' => '411122',
                                    'cc_orig_last_4' => '4444',
                                    'cc_type' => 'visa',
                                    'chargeback_date' => '',
                                    'check_account_last_4' => '',
                                    'check_routing_last_4' => '',
                                    'check_ssn_last_4' => '',
                                    'check_transitnum' => '',
                                    'child_id' => '',
                                    'click_id' => '',
                                    'created_by_user_name' => '',
                                    'created_by_employee_name' => '',
                                    'coupon_discount_amount' => '0.00',
                                    'coupon_id' => '',
                                    'credit_applied' => '0.00',
                                    'promo_code' => '',
                                    'customer_id' => '51',
                                    'customers_telephone' => '1234567890',
                                    'decline_salvage_discount_percent' => '0',
                                    'decline_reason' => '',
                                    'email_address' => 'updatetest@authorify.com',
                                    'first_name' => 'James',
                                    'gateway_id' => '7',
                                    'gateway_descriptor' => 'Sandbox - Authorify',
                                    'hold_date' => '',
                                    'ip_address' => '103.252.34.94',
                                    'is_blacklisted' => '0',
                                    'is_chargeback' => '0',
                                    'is_fraud' => '0',
                                    'is_recurring' => '1',
                                    'is_refund' => 'no',
                                    'is_rma' => '0',
                                    'is_test_cc' => '1',
                                    'is_void' => 'no',
                                    'last_name' => 'Kirk',
                                    'main_product_id' => '7',
                                    'main_product_quantity' => '1',
                                    'next_subscription_product' => '',
                                    'next_subscription_product_id' => '',
                                    'on_hold' => '0',
                                    'on_hold_by' => '',
                                    'order_confirmed' => 'NO_STATUS',
                                    'order_confirmed_date' => '',
                                    'order_sales_tax' => '0.00',
                                    'order_sales_tax_amount' => '0.00',
                                    'order_status' => '7',
                                    'order_total' => '0.99',
                                    'parent_id' => '14892',
                                    'prepaid_match' => 'No',
                                    'preserve_gateway' => '0',
                                    'processor_id' => '',
                                    'rebill_discount_percent' => '0',
                                    'current_rebill_discount_percent' => '0',
                                    'recurring_date' => '2019-07-13',
                                    'refund_amount' => '0.00',
                                    'refund_date' => '',
                                    'retry_date' => '',
                                    'rma_number' => '',
                                    'rma_reason' => '',
                                    'shipping_city' => 'City 1',
                                    'shipping_country' => 'US',
                                    'shipping_date' => 'Not Shipped',
                                    'shipping_first_name' => 'James',
                                    'shipping_id' => '',
                                    'shipping_last_name' => 'Kirk',
                                    'shipping_method_name' => 'NA',
                                    'shipping_postcode' => '32233',
                                    'shipping_state' => 'FL',
                                    'shipping_state_id' => 'FL',
                                    'shipping_street_address' => 'Street 1',
                                    'shipping_street_address2' => 'Apartment 1',
                                    'sub_affiliate' => '',
                                    'time_stamp' => '2019-06-12 00:00:43',
                                    'tracking_number' => '',
                                    'transaction_id' => 'Not Available',
                                    'upsell_product_id' => '',
                                    'upsell_product_quantity' => '',
                                    'void_amount' => '0.00',
                                    'void_date' => '',
                                    'shippable' => '0',
                                    'website_received' => '',
                                    'website_sent' => '',
                                    'products' =>
                                        [
                                            0 =>
                                                [
                                                    'product_id' => '7',
                                                    'sku' => 'sandbox-intel-i7-8700k',
                                                    'price' => '0.99',
                                                    'product_qty' => '1',
                                                    'name' => 'Sandbox - Intel i7-8700k',
                                                    'on_hold' => '0',
                                                    'is_recurring' => '1',
                                                    'recurring_date' => '2019-07-13',
                                                    'subscription_id' => 'ee5f354e51a52881bb7af037cf8f1164',
                                                    'next_subscription_product' => 'Sandbox - Intel i7-8700k',
                                                    'next_subscription_product_id' => '7',
                                                    'next_subscription_product_price' => '0.9900',
                                                    'next_subscription_qty' => '1',
                                                    'is_add_on' => '0',
                                                    'is_in_trial' => '0',
                                                    'step_number' => '',
                                                    'is_shippable' => '0',
                                                    'is_full_refund' => '0',
                                                    'refund_amount' => '0.00',
                                                    'subscription_type' => 'Bill by cycle',
                                                    'subscription_desc' => 'Bills every 31 days',
                                                    'billing_model' =>
                                                        [
                                                            'id' => '8',
                                                            'name' => '31 Days Billing Cycle',
                                                            'description' => 'Bills every 31 days',
                                                        ],
                                                    'offer' =>
                                                        [
                                                            'id' => '14',
                                                            'name' => 'Sandbox - Online',
                                                        ],
                                                ],
                                        ],
                                    'systemNotes' =>
                                        [
                                            0 => 'June 12, 2019 12:00 AM - Lime Light CRM API - Product <a href=products/products.php?product_id=7>7</a> was purchased using offer <a href=billing_models/template.php?id=14>14</a>, bill by cycle. It will recur to product <a href=products/products.php?product_id=7>7</a> on 7/13/2019.',
                                            1 => 'June 12, 2019 12:00 AM - Lime Light CRM API -                   Product <a href=products/products.php?product_id=7>7</a>  was given a 0.00 discount  while recurring on                     offer <a href=billing_models/template.php?id=14>14</a>',
                                            2 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            3 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            4 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            5 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            6 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            7 => 'June 12, 2019 11:48 AM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            8 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            9 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            10 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            11 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing Address2 from some value to Apartment 1 by API User limelighttest',
                                            12 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            13 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            14 => 'June 12, 2019 01:03 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            15 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            16 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            17 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            18 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            19 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            20 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            21 => 'June 12, 2019 02:06 PM - Lime Light CRM API - Changed Billing Address2 from Apartment 1 to some value by API User limelighttest',
                                            22 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed First Name from James to Update by API User limelighttest',
                                            23 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Last Name from Kirk to Test by API User limelighttest',
                                            24 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing Address1 from 51 Pine St to Street 1 by API User limelighttest',
                                            25 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed Billing City from Atlantic Beach to City 1 by API User limelighttest',
                                            26 => 'June 12, 2019 02:25 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                            27 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed First Name from Update to James by API User limelighttest',
                                            28 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Last Name from Test to Kirk by API User limelighttest',
                                            29 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing Address1 from Street 1 to 51 Pine St by API User limelighttest',
                                            30 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed Billing City from City 1 to Atlantic Beach by API User limelighttest',
                                            31 => 'June 13, 2019 02:16 PM - Lime Light CRM API - Changed credit card information by API User limelighttest',
                                        ],
                                ],
                        ],
                ]
            );

        $client
            ->request(
                'authorize_payment',
                [
                    'billingFirstName' => 'James',
                    'billingLastName' => 'Kirk',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'phone' => '1234567890',
                    'email' => 'updatetest@authorify.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '1299',
                    'CVV' => '123',
                    'ipAddress' => '127.0.0.1',
                    'productId' => 22,
                    'campaignId' => 8,
                    'billingAddress2' => 'some value',
                ]
            )
            ->willReturn(
                [
                    'response_code' => '100',
                    'error_found' => '0',
                    'error_message' => 'This transaction has been approved',
                    'transactionID' => 'Not Available',
                    'authId' => 'Not Available',
                    'gateway_id' => '7',
                    'prepaid' => '',
                    'prepaid_match' => 'No',
                ]
            );

        $client
        ->request(
            'order_update',
            [
                'order_id' =>
                    [
                        16010 =>
                            [
                                'first_name' => 'James',
                                'last_name' => 'Kirk',
                                'phone' => '1234567890',
                                'billing_first_name' => 'James',
                                'billing_last_name' => 'Kirk',
                                'billing_address1' => '51 Pine St',
                                'billing_address2' => 'some value',
                                'billing_city' => 'Atlantic Beach',
                                'billing_state' => 'FL',
                                'billing_zip' => '32233',
                                'billing_country' => 'US',
                                'cc_number' => '4111222233334444',
                                'cc_expiration_date' => '1299',
                                'cc_payment_type' => 'visa',
                            ],
                    ],
            ]
        )
        ->willReturn(
            [
                'response_code' => '911',
                'order_id' =>
                    [
                        16010 =>
                            [
                                'first_name' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'last_name' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'phone' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_first_name' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_last_name' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_address1' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_address2' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_city' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_state' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_zip' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'billing_country' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'cc_number' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'cc_expiration_date' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                                'cc_payment_type' =>
                                    [
                                        'response_code' => '343,343,343,343,343,343,343,343,343,343,343,343,343,343',
                                    ],
                            ],
                    ],
            ]
        );

        $client
            ->request(
                'subscription_order_update',
                [
                    'order_id' => 16010,
                    'product_id' => 7,
                    'status' => 'reset'
                ]
            )
            ->shouldBeCalled()
            ->willReturn(
                [
                    'response_code' => '100',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = OrdersEndpointMother::getUpdateOrderParams();
        $params['email'] = 'updatetest@authorify.com';

        $response = $this->request('POST', '/v1/limelight/orders/update', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Credit card details updated.', $response['message']);
    }

    public function testSubscribeSuccessSingleProduct(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 3,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate subscribe',
                    'products' =>
                        [
                            35 =>
                                [
                                    'offer_id' => 20,
                                    'billing_model_id' => 8,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16374',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'd27f3cb1b447579b3dce86929fb63352',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/subscription', OrdersEndpointMother::getSubscribeParams());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function testSubscribeSuccessTrialProductIdEmpty(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 3,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate subscribe',
                    'products' =>
                        [
                            35 =>
                                [
                                    'offer_id' => 20,
                                    'billing_model_id' => 8,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16374',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'd27f3cb1b447579b3dce86929fb63352',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/limelight/subscription', OrdersEndpointMother::getSubscribeParamsEmptyTrialProductId());

        $this->assertEquals('success', $response['status']);
        $this->assertEquals('Approved', $response['data']['resp_msg']);
    }

    public function dataProviderInvalidUtmValues(): array
    {
        return [
            ['utm_source', '?'],
            ['utm_source', '&'],
            ['utm_source', '='],
            ['utm_source', '#'],
            ['utm_source', '%'],

            ['utm_campaign', '?'],
            ['utm_campaign', '&'],
            ['utm_campaign', '='],
            ['utm_campaign', '#'],
            ['utm_campaign', '%'],

            ['utm_medium', '?'],
            ['utm_medium', '&'],
            ['utm_medium', '='],
            ['utm_medium', '#'],
            ['utm_medium', '%'],

            ['utm_term', '?'],
            ['utm_term', '&'],
            ['utm_term', '='],
            ['utm_term', '#'],
            ['utm_term', '%'],
        ];
    }
}