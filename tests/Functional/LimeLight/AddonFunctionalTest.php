<?php

namespace Tests\Authorify\Functional\LimeLight;

use Authorify\Provider\LimeLight\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Authorify\Functional\WebTestCaseTrait;
use Tests\Authorify\Mother\Provider\LimeLight\OrdersEndpointMother;

class AddonFunctionalTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testAddonFailureEmailMissing(): void
    {
        $params = OrdersEndpointMother::getAddonParams();
        unset($params['email']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testAddonFailureEmailEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonParams();
        $params['email'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testAddonFailureEmailInvalid(): void
    {
        $params = OrdersEndpointMother::getAddonParams();
        $params['email'] = 'bart.simpsons[at]smartagents.com';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid email address.', $response['message']);
    }

    public function testAddonFailurePreviousOrderIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        unset($params['previousOrderId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/ ', $params);

        $this->assertEquals('Please enter a valid previous order ID.', $response['message']);
    }

    public function testAddonFailurePreviousOrderIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();

        $response = $this->request('POST', '/v1/limelight/orders/addon/x', $params);

        $this->assertEquals('Please enter a valid previous order ID.', $response['message']);
    }


    public function testAddonFailureProductIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        unset($params['productId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testAddonFailureProductIdEmptyString(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['productId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testAddonFailureProductIdEmptyArray(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['productId'] = [];

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testAddonFailureProductIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['productId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testAddonFailureProductQtyMissing(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        unset($params['productQty']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonFailureProductQtyEmptyString(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['productQty'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonFailureProductQtyEmptyArray(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['productQty'] = [];

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonFailureProductQtyContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['productQty'][0] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testAddonFailureOfferIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        unset($params['offerId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testAddonFailureOfferIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['offerId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testAddonFailureOfferIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['offerId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testAddonFailureBillingModelIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        unset($params['billingModelId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testAddonFailureBillingModelIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['billingModelId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testAddonFailureBillingModelIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['billingModelId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testAddonFailureShippingIdMissing(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        unset($params['shippingId']);

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testAddonFailureShippingIdEmpty(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['shippingId'] = '';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testAddonFailureShippingIdContainsNonDigitCharacter(): void
    {
        $params = OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId();
        $params['shippingId'] = 'x';

        $response = $this->request('POST', '/v1/limelight/orders/addon/1', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testAddonOrderFailureProductIdIsNotAnArray(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16347',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'b748ad1df4d09a737cbc760283f1ed84',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon/' . $orderId, OrdersEndpointMother::getAddonParamsWithScalarProductId());

        $this->assertEquals('failure', $addonResponse['status']);
        $this->assertEquals('Please enter a valid product ID.', $addonResponse['message']);
    }

    public function testAddonOrderFailureProductQtyIsNotAnArray(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16352',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => '2b550ea929db003551dd960b77dfd61d',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon/' . $orderId, OrdersEndpointMother::getAddonParamsWithScalarProductQty());

        $this->assertEquals('failure', $addonResponse['status']);
        $this->assertEquals('Please enter a valid product quantity.', $addonResponse['message']);
    }

    public function testAddonOrderSuccessSingleProduct(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16353',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'f10a651240a202c6e25128bb072a8018',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $this->client = self::createClient();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order_card_on_file',
                [
                    'campaignId' => 4,
                    'shippingId' => 4,
                    'previousOrderId' => 16353,
                    'products' =>
                        [
                            7 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                    'initializeNewSubscription' => 1,
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16354',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.99',
                    'orderSalesTaxPercent' => '0.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            7 => '787eb3e25d9759b104626dfc78d98339',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);
        
        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon/' . $orderId, OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId());

        $this->assertEquals('success', $addonResponse['status']);
        $this->assertEquals('Approved', $addonResponse['data']['resp_msg']);
    }

    public function testAddonOrderSuccessMultipleProducts(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16355',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'd36eb83d9079092cac70e3efc9b66008',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $this->client = self::createClient();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order_card_on_file',
                [
                    'campaignId' => 4,
                    'shippingId' => 4,
                    'previousOrderId' => 16355,
                    'products' =>
                        [
                            7 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                    'initializeNewSubscription' => 1,
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16356',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.99',
                    'orderSalesTaxPercent' => '0.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            7 => '05fc231e219501029579f4ad9f9d0a1a',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon/' . $orderId, OrdersEndpointMother::getAddonParamsWithNoPreviousOrderId());

        $this->assertEquals('success', $addonResponse['status']);
        $this->assertEquals('Approved', $addonResponse['data']['resp_msg']);
    }

    public function testAddonOrderSuccessProductIdAndQtyAreNotArrays(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'AFID' => 'affiliate one',
                    'products' =>
                        [
                            10 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16357',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.02',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            10 => 'a36e41416bddabc0a66453fdf26733db',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $orderResponse = $this->request('POST', '/v1/limelight/orders', OrdersEndpointMother::getCreateParams());
        $orderId = $orderResponse['data']['order_id'];

        $this->client = self::createClient();

        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order_card_on_file',
                [
                    'campaignId' => 4,
                    'shippingId' => 4,
                    'previousOrderId' => 16357,
                    'products' =>
                        [
                            7 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                    'initializeNewSubscription' => 1,
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16358',
                    'transactionID' => 'Not Available',
                    'customerId' => '10',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.99',
                    'orderSalesTaxPercent' => '0.00',
                    'orderSalesTaxAmount' => '0.00',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            7 => 'd59a2c18402c714ca11db3c11caa04c5',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $addonResponse = $this->request('POST', '/v1/limelight/orders/addon/' . $orderId, OrdersEndpointMother::getAddonParamsWithScalarProductIdAndQty());

        $this->assertEquals('success', $addonResponse['status']);
        $this->assertEquals('Approved', $addonResponse['data']['resp_msg']);
    }
}
