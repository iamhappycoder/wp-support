<?php

namespace Tests\Authorify\Functional;

trait WebTestCaseTrait
{

    public function setUp(): void
    {
        parent::setUp();

        $this->client = self::createClient();
    }

    protected function request(string $method, string $path, array $params = []): array
    {
        $this->client->request($method, $path, $params);

        return json_decode($this->client->getResponse()->getContent(), true);
    }
}