<?php

namespace Tests\Authorify\Functional\Credits;

use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Exception\ServerException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Authorify\Functional\WebTestCaseTrait;
use Tests\Authorify\Mother\Endpoint\Credits\TopUpEndpointMother;

class TopUpFunctionalTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testCreateOrderUnrecognizedParameter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['warpFactor'] = '7';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('The parameter warpFactor is not recognized.', $response['message']);
    }

    public function testCreateFailureFirstNameMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['firstName']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureFirstNameEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['firstName'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureFirstNameGreaterThanMaxCharacters(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['firstName'] = str_pad('bart', 51, 'x');

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a first name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureLastNameMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['lastName']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureLastNameEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['lastName'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailureLastNameGreaterThanMaxCharacters(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['lastName'] = str_pad('simpson', 51, 'x');

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a last name that is no more than 50 characters long.', $response['message']);
    }

    public function testCreateFailurePhoneMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['phone']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailurePhoneEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['phone'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailurePhoneLessThanMinCharacters(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['phone'] = '123456789';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

    public function testCreateFailurePhoneGreaterThanMaxCharacters(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['phone'] = '123456789012';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a 10-digit phone number.', $response['message']);
    }

//    public function testCreateFailureEmailMissing(): void
//    {
//        $params = TopUpEndpointMother::getCreateParams();
//        unset($params['email']);
//
//        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);
//
//        $this->assertEquals('Please enter a valid email address.', $response['message']);
//    }
//
//    public function testCreateFailureEmailEmpty(): void
//    {
//        $params = TopUpEndpointMother::getCreateParams();
//        $params['email'] = '';
//
//        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);
//
//        $this->assertEquals('Please enter a valid email address.', $response['message']);
//    }

    public function testCreateFailureCreditCardNumberMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['creditCardNumber']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberLessThanMinimumDigits(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '11112222333';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureCreditCardNumberMoreThanMaximumDigits(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['creditCardNumber'] = '11112222333344445555';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card number.', $response['message']);
    }

    public function testCreateFailureExpirationMonthMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['expirationMonth']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationMonth'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthInvalidCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationMonth'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthInvalidMonth(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationMonth'] = '13';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['expirationYear']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationYear'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearInvalidCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationYear'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationYearInvalidYear(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationYear'] = '1';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureExpirationMonthAndYearValidatedBeforeCvv(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['expirationMonth'] = '';
        $params['expirationYear'] = '';
        $params['cvv'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid credit card expiration date.', $response['message']);
    }

    public function testCreateFailureCvvMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['cvv']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testCreateFailureCvvEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['cvv'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testCreateFailureCvvContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['cvv'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid CVV number.', $response['message']);
    }

    public function testCreateFailureBillingAddress1Missing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['billingAddress1']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a billing address.', $response['message']);
    }

    public function testCreateFailureBillingAddress1Empty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingAddress1'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a billing address.', $response['message']);
    }

    public function testCreateFailureBillingCityMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['billingCity']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a city.', $response['message']);
    }

    public function testCreateFailureBillingCityEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingCity'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a city.', $response['message']);
    }

    public function testCreateFailureBillingCountryMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['billingCountry']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please select a country.', $response['message']);
    }

    public function testCreateFailureBillingCountryEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingCountry'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please select a country.', $response['message']);
    }

    public function testCreateFailureBillingStateMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['billingState']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingStateEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingState'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingStateInvalidUsState(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingCountry'] = 'us';
        $params['billingState'] = 'ab';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingStateInvalidCanadianState(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingCountry'] = 'ca';
        $params['billingState'] = 'al';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please select a state.', $response['message']);
    }

    public function testCreateFailureBillingZipMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['billingZip']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureBillingZipEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingZip'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureBillingZipInvalidUsZip(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingCountry'] = 'us';
        $params['billingZip'] = 'K1W 0B1';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureBillingZipInvalidCanadianZip(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingCountry'] = 'ca';
        $params['billingState'] = 'ab';
        $params['billingZip'] = '12345';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid zip code.', $response['message']);
    }

    public function testCreateFailureProductIdMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['productId']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductIdEmptyString(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['productId'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductIdEmptyArray(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['productId'] = [];

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductIdContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['productId'][0] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateFailureProductQtyMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['productQty']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureProductQtyEmptyString(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['productQty'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureProductQtyEmptyArray(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['productQty'] = [];

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureProductQtyContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['productQty'][0] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateFailureOfferIdMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['offerId']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testCreateFailureOfferIdEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['offerId'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testCreateFailureOfferIdContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['offerId'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid offer ID.', $response['message']);
    }

    public function testCreateFailureBillingModelIdMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['billingModelId']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testCreateFailureBillingModelIdEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingModelId'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testCreateFailureBillingModelIdContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['billingModelId'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid billing model ID.', $response['message']);
    }

    public function testCreateFailureShippingIdMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['shippingId']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testCreateFailureShippingIdEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['shippingId'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testCreateFailureShippingIdContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['shippingId'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid shipping ID.', $response['message']);
    }

    public function testCreateFailureCampaignIdMissing(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        unset($params['campaignId']);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testCreateFailureCampaignIdEmpty(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['campaignId'] = '';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testCreateFailureCampaignIdContainsNonDigitCharacter(): void
    {
        $params = TopUpEndpointMother::getCreateParams();
        $params['campaignId'] = 'x';

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('Please enter a valid campaign ID.', $response['message']);
    }

    public function testCreateOrderServerError(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                ['firstName' => 'Bart', 'lastName' => 'Simpson', 'phone' => '1234567890', 'email' => 'bart.simpsons@smartagents.com', 'creditCardType' => 'visa', 'creditCardNumber' => '4111222233334444', 'expirationDate' => '0123', 'CVV' => '123', 'billingFirstName' => 'Bart', 'billingLastName' => 'Simpson', 'billingAddress1' => '51 Pine St', 'billingAddress2' => 'some value', 'billingCity' => 'Atlantic Beach', 'billingState' => 'FL', 'billingZip' => '32233', 'billingCountry' => 'US', 'shippingFirstName' => 'Bart', 'shippingLastName' => 'Simpson', 'shippingAddress1' => '51 Pine St', 'shippingAddress2' => 'some value', 'shippingCity' => 'Atlantic Beach', 'shippingState' => 'FL', 'shippingZip' => '32233', 'shippingCountry' => 'US', 'shippingId' => 1234567890, 'tranType' => 'Sale', 'ipAddress' => '127.0.0.1', 'campaignId' => 4, 'billingSameAsShipping' => 'YES', 'products' => [18 => ['offer_id' => 14, 'billing_model_id' => 2, 'quantity' => 1]]]
            )
            ->willThrow(new ServerException('Invalid shipping id of (1234567890) found - order canceled.', 10322, null, 'Invalid shipping id of (1234567890) found - order canceled.'));

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', TopUpEndpointMother::getCreateParamsWithInvalidProductId());

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Invalid shipping id of (1234567890) found - order canceled.', $response['message']);
        $this->assertEquals('10322', $response['code']);
    }

    public function testCreateOrderAddress2NotSent(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart.simpsons@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            18 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16309',
                    'transactionID' => 'Not Available',
                    'customerId' => '30',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.54',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.04',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            18 => 'd2482421af1f30a30c95573c005a0076',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', TopUpEndpointMother::getCreateParamsAddress2NotSent());

        $this->assertEquals('success', $response['status']);
        $this->assertIsInt($response['data']['newBookCredits']);
    }

    public function testCreateOrderDeclined(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart.simpsons@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233330000',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            18 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willThrow(new ServerException('This transaction has been declined', 10800, null, 'This transaction has been declined'));

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', TopUpEndpointMother::getCreateParamsDeclined());

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('This transaction has been declined', $response['data']['declineReason']);
    }

    public function testCreateOrderFailureProductIdIsNotAnArray(): void
    {
        $params = TopUpEndpointMother::getCreateParamsWithScalarProductId();

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid product ID.', $response['message']);
    }

    public function testCreateOrderFailureProductQtyIsNotAnArray(): void
    {
        $params = TopUpEndpointMother::getCreateParamsWithScalarProductQty();

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid product quantity.', $response['message']);
    }

    public function testCreateOrderSuccessSingleProduct(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart.simpsons@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            18 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16309',
                    'transactionID' => 'Not Available',
                    'customerId' => '30',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.54',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.04',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            18 => 'd2482421af1f30a30c95573c005a0076',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', TopUpEndpointMother::getCreateParams());

        $this->assertEquals('success', $response['status']);
        $this->assertIsInt($response['data']['newBookCredits']);
    }

    public function testCreateOrderSuccessMultipleProducts(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart.simpsons@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            18 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                            14 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16321',
                    'transactionID' => 'Not Available',
                    'customerId' => '30',
                    'authId' => 'Not Available',
                    'orderTotal' => '240.94',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '15.44',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            18 => 'd5ce2254342b101a70ad9923232fa434',
                            14 => '68a14af01f93836be0b3d26aada0f35d',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', TopUpEndpointMother::getCreateParamsMultipleProductIds());

        $this->assertEquals('success', $response['status']);
        $this->assertIsInt($response['data']['newBookCredits']);
    }

    public function testCreateOrderSuccessProductIdAndQtyAreNotArrays(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart.simpsons@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233334444',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            18 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willReturn(
                [
                    'gateway_id' => '7',
                    'response_code' => '100',
                    'error_found' => '0',
                    'order_id' => '16322',
                    'transactionID' => 'Not Available',
                    'customerId' => '30',
                    'authId' => 'Not Available',
                    'orderTotal' => '5.54',
                    'orderSalesTaxPercent' => '7.00',
                    'orderSalesTaxAmount' => '0.04',
                    'test' => '1',
                    'prepaid_match' => '0',
                    'gatewayCustomerService' => '12345',
                    'gatewayDescriptor' => 'Sandbox - Authorify',
                    'subscription_id' =>
                        [
                            18 => 'b2f0c656bade24b4752738cd9c619d7b',
                        ],
                    'resp_msg' => 'Approved',
                ]
            );

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = TopUpEndpointMother::getCreateParamsWithScalarProductIdAndQty();

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('success', $response['status']);
        $this->assertIsInt($response['data']['newBookCredits']);
    }

    public function testCreateOrderSuccessContactDoesNotExist(): void
    {
        $client = $this->prophesize(Client::class);
        $client
            ->request(
                'new_order',
                [
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '1234567890',
                    'email' => 'bart.simpsons@smartagents.com',
                    'creditCardType' => 'visa',
                    'creditCardNumber' => '4111222233330000',
                    'expirationDate' => '0123',
                    'CVV' => '123',
                    'billingFirstName' => 'Bart',
                    'billingLastName' => 'Simpson',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'billingCountry' => 'US',
                    'shippingFirstName' => 'Bart',
                    'shippingLastName' => 'Simpson',
                    'shippingAddress1' => '51 Pine St',
                    'shippingAddress2' => 'some value',
                    'shippingCity' => 'Atlantic Beach',
                    'shippingState' => 'FL',
                    'shippingZip' => '32233',
                    'shippingCountry' => 'US',
                    'shippingId' => 4,
                    'tranType' => 'Sale',
                    'ipAddress' => '127.0.0.1',
                    'campaignId' => 4,
                    'billingSameAsShipping' => 'YES',
                    'products' =>
                        [
                            18 =>
                                [
                                    'offer_id' => 14,
                                    'billing_model_id' => 2,
                                    'quantity' => 1,
                                ],
                        ],
                ]
            )
            ->willThrow(new ServerException('This transaction has been declined', 10800, null, 'This transaction has been declined'));

        $clientMock = $client->reveal();
        static::$container->set('Test\Authorify\Provider\LimeLight\Client', $clientMock);

        $params = TopUpEndpointMother::getCreateParamsDeclined();

        $response = $this->request('POST', '/v1/credits/bart.simpsons@smartagents.com/topup', $params);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('This transaction has been declined', $response['message']);
        $this->assertEquals('This transaction has been declined', $response['data']['declineReason']);
    }
}
