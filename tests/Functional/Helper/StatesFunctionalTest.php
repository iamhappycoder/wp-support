<?php

namespace Tests\Authorify\Functional\Helper;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Authorify\Functional\WebTestCaseTrait;

class StatesFunctionalTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testListFailureCountryCodeEmpty(): void
    {
        $response = $this->request('GET', '/v1/helper/states/ ');

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid country code.', $response['message']);
    }

    public function testListFailureCountryCodeInvalid(): void
    {
        $response = $this->request('GET', '/v1/helper/states/x');

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Please enter a valid country code.', $response['message']);
    }

    public function testListCountryCodeValidUs(): void
    {
        $response = $this->request('GET', '/v1/helper/states/us');

        $this->assertEquals('success', $response['status']);

        $this->assertCount(51, $response['data']);
        $this->assertEquals('Alabama', $response['data']['AL']);
        $this->assertEquals('Wyoming', $response['data']['WY']);
    }

    public function testListCountryCodeValidCanada(): void
    {
        $response = $this->request('GET', '/v1/helper/states/ca');

        $this->assertEquals('success', $response['status']);

        $this->assertCount(10, $response['data']);
        $this->assertEquals('Alberta', $response['data']['AB']);
        $this->assertEquals('Saskatchewan', $response['data']['SK']);
    }
}
