<?php

namespace Tests\Authorify\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiFunctionalTest extends WebTestCase
{
    public function testRequestFailureMethodNotAllowed(): void
    {
        $client = self::createClient();

        $client->request('PUT', '/v1/limelight/orders');

        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('HTTP method not allowed.', $response['message']);
    }

    public function testRequestFailureResourceNotFound(): void
    {
        $client = self::createClient();

        $client->request('POST', '/v1/limelight/orders/addon/');

        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals('failure', $response['status']);
        $this->assertEquals('Invalid endpoint.', $response['message']);
    }
}