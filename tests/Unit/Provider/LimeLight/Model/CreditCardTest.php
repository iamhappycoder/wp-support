<?php

namespace Tests\Authorify\Unit\Provider\LimeLight\Model;

use Authorify\Provider\LimeLight\Model\CreditCard;
use PHPUnit\Framework\TestCase;

class CreditCardTest extends TestCase
{
    public function testDefaults(): void
    {
        $creditCard = new CreditCard();

        $this->assertNull($creditCard->getType());
        $this->assertNull($creditCard->getNumber());
        $this->assertNull($creditCard->getExpiration());
        $this->assertNull($creditCard->getCvv());
    }

    public function testAccessors(): void
    {
        $creditCard = new CreditCard(
            null,
            '4111222233334444',
            new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC')),
            'CVV'
        );

        $this->assertEquals('visa', $creditCard->getType());
        $this->assertEquals('4111222233334444', $creditCard->getNumber());
        $this->assertEquals(new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC')), $creditCard->getExpiration());
        $this->assertEquals('CVV', $creditCard->getCvv());
    }

    public function testMutators(): void
    {
        $creditCard = new CreditCard(
            null,
            '4111222233334444',
            new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC')),
            'CVV'
        );

        $this->assertInstanceOf(CreditCard::class, $creditCard->setType('master'));
        $this->assertInstanceOf(CreditCard::class, $creditCard->setNumber('2111222233334444'));
        $this->assertInstanceOf(CreditCard::class, $creditCard->setExpiration(new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC'))));
        $this->assertInstanceOf(CreditCard::class, $creditCard->setCvv('123'));

        $this->assertEquals(
            $creditCard = new CreditCard(
                'master',
                '2111222233334444',
                new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC')),
                '123'
            ),
            $creditCard
        );
    }

    public function testGetNumberIsMasked(): void
    {
        $creditCard = new CreditCard(
            null,
            '4111222233335678',
            new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC')),
            '123'
        );

        $this->assertEquals('XXXXXXXXXXXX5678', $creditCard->getNumber(true));
    }

    public function testGetCvvIsMasked(): void
    {
        $creditCard = new CreditCard(
            null,
            '4111222233335678',
            new \DateTimeImmutable('2018-10-02 02:16:00', new \DateTimeZone('UTC')),
            '123'
        );

        $this->assertEquals('XXX', $creditCard->getCvv(true));
    }
}
