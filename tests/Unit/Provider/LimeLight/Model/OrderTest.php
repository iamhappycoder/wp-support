<?php

namespace Tests\Authorify\Unit\Provider\LimeLight\Model;

use Authorify\Provider\LimeLight\Model\Order;
use Authorify\Provider\LimeLight\Model\Product;
use PHPUnit\Framework\TestCase;
use Tests\Authorify\Mother\Provider\LimeLight\CustomerMother;
use Tests\Authorify\Mother\Provider\LimeLight\ProductMother;

class OrderTest extends TestCase
{
    public function testDefaults(): void
    {
        $order = new Order();

        $this->assertNull($order->getShippingId());
        $this->assertEquals('Sale', $order->getTransactionType());
        $this->assertNull($order->getIpAddress());
        $this->assertNull($order->getCampaignId());
        $this->assertNull($order->isBillingSameAsShipping());
        $this->assertNull($order->getCustomer());
        $this->assertEmpty($order->getProducts());
        $this->assertNull($order->getAffiliateId());
        $this->assertNull($order->getProductBump());
    }

    public function testAccessors(): void
    {
        $order = new Order(1, 'two', '3.4.5.6', 7, true, CustomerMother::getByKey(0), ProductMother::getByRange(0, 1));
        $order->setAffiliateId('af1');
        $order->setProductBump(new Product(1, 2, 3, 4));

        $this->assertOrderProperties(
            $order,
            [
                'shippingId' => 1,
                'transactionType' => 'two',
                'ipAddress' => '3.4.5.6',
                'campaignId' => 7,
                'isBillingSameAsShipping' => true,
                'customer' => CustomerMother::getByKey(0),
                'products' => ProductMother::getByRange(0, 1),
                'affiliateId' => 'af1',
                'productBump' => ProductMother::getByKey(0),
            ]
        );
    }

    public function testMutators(): void
    {
        $order = new Order(1, 'two', '3.4.5.6', 7, true, CustomerMother::getByKey(0), ProductMother::getByRange(0, 1));
        $order->setAffiliateId('af1');
        $order->setProductBump(new Product(5, 6, 6, 8));

        $this->assertInstanceOf(Order::class, $order->setShippingId(5));
        $this->assertInstanceOf(Order::class, $order->setTransactionType(6));
        $this->assertInstanceOf(Order::class, $order->setIpAddress(7));
        $this->assertInstanceOf(Order::class, $order->setCampaignId(8));
        $this->assertInstanceOf(Order::class, $order->setIsBillingSameAsShipping(false));
        $this->assertInstanceOf(Order::class, $order->setCustomer(CustomerMother::getByKey(1)));
        $this->assertInstanceOf(Order::class, $order->setProducts(ProductMother::getByRange(2, 2)));
        $this->assertInstanceOf(Order::class, $order->setAffiliateId('af2'));
        $this->assertInstanceOf(Order::class, $order->setProductBump(ProductMother::getByKey(0)));

        $this->assertOrderProperties(
            $order,
            [
                'shippingId' => 5,
                'transactionType' => 6,
                'ipAddress' => 7,
                'campaignId' => 8,
                'isBillingSameAsShipping' => false,
                'customer' => CustomerMother::getByKey(1),
                'products' => ProductMother::getByRange(2, 2),
                'affiliateId' => 'af2',
                'productBump' => ProductMother::getByKey(0),
            ]
        );
    }

    private function assertOrderProperties(Order $order, array $properties): void
    {
        $this->assertEquals($properties['shippingId'], $order->getShippingId());
        $this->assertEquals($properties['transactionType'], $order->getTransactionType());
        $this->assertEquals($properties['ipAddress'], $order->getIpAddress());
        $this->assertEquals($properties['campaignId'], $order->getCampaignId());
        $this->assertEquals($properties['isBillingSameAsShipping'], $order->isBillingSameAsShipping());
        $this->assertEquals($properties['customer'], $order->getCustomer());
        $this->assertEquals($properties['products'], $order->getProducts());
        $this->assertEquals($properties['affiliateId'], $order->getAffiliateId());
    }
}
