<?php

namespace Tests\Authorify\Unit\Provider\LimeLight\Model;

use Authorify\Provider\LimeLight\Model\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testDefaults(): void
    {
        $product = new Product();

        $this->assertNull($product->getId());
        $this->assertNull($product->getOfferId());
        $this->assertNull($product->getBillingModelId());
        $this->assertNull($product->getQuantity());
        $this->assertNull($product->getTrialProductId());
    }

    public function testAccessors(): void
    {
        $product = new Product(1, 2, 3, 4, 5);

        $this->assertEquals(1, $product->getId());
        $this->assertEquals(2, $product->getOfferId());
        $this->assertEquals(3, $product->getBillingModelId());
        $this->assertEquals(4, $product->getQuantity());
        $this->assertEquals(5, $product->getTrialProductId());
    }

    public function testMutators(): void
    {
        $product = new Product(1, 2, 3, 4, 9);

        $this->assertInstanceOf(Product::class, $product->setId(5));
        $this->assertInstanceOf(Product::class, $product->setOfferId(6));
        $this->assertInstanceOf(Product::class, $product->setBillingModelId(7));
        $this->assertInstanceOf(Product::class, $product->setQuantity(8));
        $this->assertInstanceOf(Product::class, $product->setTrialProductId(9));
        $this->assertEquals(
            new Product(5, 6, 7, 8, 9),
            $product
        );
    }
}