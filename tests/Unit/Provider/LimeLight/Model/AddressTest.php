<?php

namespace Tests\Authorify\Unit\Provider\LimeLight\Model;

use Authorify\Provider\LimeLight\Model\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    public function testDefaults(): void
    {
        $address = new Address();

        $this->assertNull($address->getFirstName());
        $this->assertNull($address->getLastName());
        $this->assertNull($address->getAddress1());
        $this->assertNull($address->getAddress2());
        $this->assertNull($address->getCity());
        $this->assertNull($address->getState());
        $this->assertNull($address->getZip());
        $this->assertNull($address->getCountry());
        $this->assertNull($address->getType());
    }

    public function testAccessors(): void
    {
        $address = new Address(
            'First',
            'Last',
            'Address 1',
            'Address 2',
            'City',
            'State',
            'Zip',
            'Country',
            'billing'
        );

        $this->assertEquals('First', $address->getFirstName());
        $this->assertEquals('Last', $address->getLastName());
        $this->assertEquals('Address 1', $address->getAddress1());
        $this->assertEquals('Address 2', $address->getAddress2());
        $this->assertEquals('City', $address->getCity());
        $this->assertEquals('State', $address->getState());
        $this->assertEquals('Zip', $address->getZip());
        $this->assertEquals('Country', $address->getCountry());
        $this->assertEquals('billing', $address->getType());
    }

    public function testMutators(): void
    {
        $address = new Address(
            'First',
            'Last',
            'Address 1',
            'Address 2',
            'City',
            'State',
            'Zip',
            'Country',
            'billing'
        );

        $this->assertInstanceOf(Address::class, $address->setFirstName('David'));
        $this->assertInstanceOf(Address::class, $address->setLastName('Budd'));
        $this->assertInstanceOf(Address::class, $address->setAddress1('New Address 1'));
        $this->assertInstanceOf(Address::class, $address->setAddress2('New Address 2'));
        $this->assertInstanceOf(Address::class, $address->setCity('New City'));
        $this->assertInstanceOf(Address::class, $address->setState('New State'));
        $this->assertInstanceOf(Address::class, $address->setZip('New Zip'));
        $this->assertInstanceOf(Address::class, $address->setCountry('New Country'));
        $this->assertInstanceOf(Address::class, $address->setType('shipping'));

        $this->assertEquals(
            new Address(
                'David',
                'Budd',
                'New Address 1',
                'New Address 2',
                'New City',
                'New State',
                'New Zip',
                'New Country',
                'shipping'
            ),
            $address
        );
    }
}
