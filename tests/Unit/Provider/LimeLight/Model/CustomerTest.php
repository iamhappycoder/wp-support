<?php

namespace Tests\Authorify\Unit\Provider\LimeLight\Model;

use Authorify\Provider\LimeLight\Model\Customer;
use PHPUnit\Framework\TestCase;
use Tests\Authorify\Mother\Provider\LimeLight\AddressMother;
use Tests\Authorify\Mother\Provider\LimeLight\CreditCardMother;

class CustomerTest extends TestCase
{
    public function testDefaults(): void
    {
        $customer = new Customer();

        $this->assertTrue($customer->getEmail() === null);
        $this->assertTrue($customer->getFirstName() === null);
        $this->assertTrue($customer->getLastName() === null);
        $this->assertTrue($customer->getPhone() === null);
        $this->assertTrue($customer->getCreditCards() === []);
        $this->assertTrue($customer->getAddresses() === []);
    }

    public function testAccessors(): void
    {
        $customer = new Customer(
            'Email',
            'Firstname',
            'Lastname',
            'Phone',
            CreditCardMother::getByRange(0, 1),
            AddressMother::getByRange(0, 1)
        );

        $this->assertEquals('Email', $customer->getEmail());
        $this->assertEquals('Firstname', $customer->getFirstName());
        $this->assertEquals('Lastname', $customer->getLastName());
        $this->assertEquals('Phone', $customer->getPhone());
        $this->assertEquals(CreditCardMother::getByRange(0, 1), $customer->getCreditCards());
        $this->assertEquals(AddressMother::getByRange(0, 1), $customer->getAddresses());
    }

    public function testMutators(): void
    {
        $creditCards = [];
        $addresses = [];
        $customer = new Customer(
            'Email',
            'Firstname',
            'Lastname',
            'Phone',
            $creditCards,
            $addresses
        );

        $this->assertInstanceOf(Customer::class, $customer->setEmail('philcoulson@shield.com'));
        $this->assertInstanceOf(Customer::class, $customer->setFirstName('Phil'));
        $this->assertInstanceOf(Customer::class, $customer->setLastName('Coulson'));
        $this->assertInstanceOf(Customer::class, $customer->setPhone('8016722'));
        $this->assertInstanceOf(Customer::class, $customer->setCreditCards($creditCards));
        $this->assertInstanceOf(Customer::class, $customer->setAddresses(AddressMother::getByRange(0, 1)));

        $this->assertEquals(
            $customer = new Customer(
                'Email',
                'Firstname',
                'Lastname',
                'Phone',
                $creditCards,
                AddressMother::getByRange(0, 1)
            ),
            $customer
        );
    }
}
