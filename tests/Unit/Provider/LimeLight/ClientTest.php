<?php

namespace Tests\Authorify\Unit\Provider\LimeLight\Model;

use Authorify\Provider\LimeLight\Client;
use Authorify\Transport\TransportInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ClientTest extends TestCase
{
    /**
     * When response_code > 100, request parameters and response are logged.
     */
    public function testRequestLimeLightLoggerIsUsed(): void
    {
        $transport = $this->prophesize(TransportInterface::class);
        $transport
            ->request(
                'POST',
                'https://authorify.limelightcrm.com/api/v1/some_method',
                ['creditCardNumber' => '4111222233334444', 'cvv' => '123'],
                ['Content-Type: application/json'],
                'username',
                'password'
            )
            ->willReturn('{"response_code":"101"}');
        $transportMock = $transport->reveal();

        $logger = $this->prophesize(LoggerInterface::class);
//        $logger
//            ->debug('{"url":"https:\/\/authorify.limelightcrm.com\/api\/v1\/some_method","params":{"creditCardNumber":"4111222233334444","cvv":"123"},"response":{"response_code":"101"}}')
//            ->shouldBeCalled();
        $loggerMock = $logger->reveal();

        $client = new Client($transportMock, $loggerMock, 'username', 'password');
        $client->request('some_method', ['creditCardNumber' => '4111222233334444', 'cvv' => '123']);
    }
}