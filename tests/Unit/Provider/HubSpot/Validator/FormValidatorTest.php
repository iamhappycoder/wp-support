<?php

namespace Tests\Authorify\Unit\Provider\HubSpot\Validator;

use Authorify\Provider\HubSpot\Validator\FormValidator;
use PHPUnit\Framework\TestCase;

class FormValidatorTest extends TestCase
{
    public function testValidateMissingRequiredField()
    {
        $fields = [
            'firstname' => ['required' => true]
        ];

        $data = [];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => ['firstname'],
            'invalid' => [],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateMissingOptionalField()
    {
        $fields = [
            'firstname' => ['required' => false]
        ];

        $data = [];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateMissingOptionalByDefaultField()
    {
        $fields = [
            'firstname' => []
        ];

        $data = [];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateRequiredField()
    {
        $fields = [
            'firstname' => ['required' => true]
        ];

        $data = [
            'firstname' => 'bart'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => ['firstname' => 'bart']
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderInvalidZipCodes
     */
    public function testValidateInvalidZipCodeUsingDefaultValidationMessage($zip)
    {
        $fields = [
            'zip' => ['required' => true, 'validation' => 'zipCode']
        ];

        $data = [
            'zip' => $zip
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['zip' => 'Please include a 5-digit zip code for the zip field.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderInvalidZipCodes
     */
    public function testValidateInvalidZipCodeUsingCustomValidationMessage($zip)
    {
        $fields = [
            'zip' => ['required' => true, 'validation' => 'zipCode', 'validationMessage' => 'Please use a valid zip code.']
        ];

        $data = [
            'zip' => $zip
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['zip' => 'Please use a valid zip code.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateValidZipCode()
    {
        $fields = [
            'zip' => ['required' => true, 'validation' => 'zipCode']
        ];

        $data = [
            'zip' => '12345'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => ['zip' => '12345']
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateInvalidEmailUsingDefaultValidationMessage()
    {
        $fields = [
            'email' => ['required' => true, 'validation' => 'email']
        ];

        $data = [
            'email' => 'x'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['email' => 'Please include a properly formatted email address for the email field.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateInvalidEmailUsingCustomValidationMessage()
    {
        $fields = [
            'email' => ['required' => true, 'validation' => 'email', 'validationMessage' => 'Please use a valid email.']
        ];

        $data = [
            'email' => 'x'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['email' => 'Please use a valid email.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateValidEmail()
    {
        $fields = [
            'email' => ['required' => true, 'validation' => 'email']
        ];

        $data = [
            'email' => 'bart.simpson@smartagents.com'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => ['email' => 'bart.simpson@smartagents.com']
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderInvalidPhoneNumbers
     */
    public function testValidateInvalidPhoneNumberUsingDefaultValidationMessage($phone)
    {
        $fields = [
            'phone' => ['required' => true, 'validation' => 'phoneNumber']
        ];

        $data = [
            'phone' => $phone
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['phone' => 'Please include an 11-digit phone number for the phone field.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderInvalidPhoneNumbers
     */
    public function testValidateInvalidPhoneNumberUsingCustomValidationMessage($phone)
    {
        $fields = [
            'phone' => ['required' => true, 'validation' => 'phoneNumber', 'validationMessage' => 'Please use a valid phone number.']
        ];

        $data = [
            'phone' => $phone
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['phone' => 'Please use a valid phone number.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderValidPhoneNumbers
     */
    public function testValidateValidPhoneNumber($phoneNumber)
    {
        $fields = [
            'phone' => ['required' => true, 'validation' => 'phoneNumber']
        ];

        $data = [
            'phone' => $phoneNumber
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => ['phone' => '+11234567890']
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testValidateSelectionInvalid()
    {
        $fields = [
            'best_meet_time' => [
                'required' => true,
                'validation' => 'best_meet_time',
                'values' => ['berlin', 'tokyo', 'rio']
            ]
        ];

        $data = [
            'best_meet_time' => 'professor'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => ['best_meet_time' => 'Please select best time to meet.'],
            'valid' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderBestMeetTime
     */
    public function testValidateSelection($bestMeetTime)
    {
        $fields = [
            'best_meet_time' => [
                'required' => true,
                'validation' => 'best_meet_time',
                'values' => ['As Soon As Possible', 'Today', 'Tomorrow']
            ]
        ];

        $data = [
            'best_meet_time' => $bestMeetTime
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => ['best_meet_time' => $bestMeetTime]
        ];

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider dataProviderTestValidateNoValidatorField
     */
    public function testValidateNoValidatorField($config)
    {
        $fields = [
            'lastname' => $config
        ];

        $data = [
            'lastname' => 'simpson'
        ];

        $formValidator = new FormValidator();

        $result = $formValidator->validate($fields, $data);
        $expectedResult = [
            'missing' => [],
            'invalid' => [],
            'valid' => ['lastname' => 'simpson']
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function dataProviderInvalidZipCodes()
    {
        return [
            ['abcde'],
            ['abc12'],
            ['1234'],
            ['123456']
        ];
    }

    public function dataProviderTestValidateNoValidatorField()
    {
        return [
            [[]],
            [['validator' => 'none']]
        ];
    }

    public function dataProviderInvalidPhoneNumbers()
    {
        return [
            ['123456789'],
            ['123456789123']
        ];
    }

    public function dataProviderValidPhoneNumbers()
    {
        return [
            ['1234567890'],
            ['(123) 456-7890'],
            ['123-456-7890'],
            ['123.456.7890'],

            ['+11234567890'],
            ['+1 (123) 456-7890'],
            ['+1 123-456-7890'],
            ['+1-123-456-7890'],
            ['+1 123.456.7890'],
            ['+1.123.456.7890'],
            ['+1.1234567890'],
            ['+1.1234567890'],
        ];
    }

    public function dataProviderBestMeetTime()
    {
        return [
            ['As Soon As Possible'],
            ['Today'],
            ['Tomorrow']
        ];
    }
}