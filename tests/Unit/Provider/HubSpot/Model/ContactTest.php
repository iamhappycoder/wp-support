<?php

namespace Tests\Authorify\Unit\Provider\HubSpot\Model;

use Authorify\Provider\HubSpot\Model\Contact;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    public function testGetEmailNull(): void
    {
        $contact = new Contact([]);

        $this->assertNull($contact->getEmail());
    }

    public function testGetFirstNameNull(): void
    {
        $contact = new Contact([]);

        $this->assertNull($contact->getFirstName());
    }

    public function testGetLastNameNull(): void
    {
        $contact = new Contact([]);

        $this->assertNull($contact->getLastName());
    }

    public function testGetPhoneNull(): void
    {
        $contact = new Contact([]);

        $this->assertNull($contact->getPhone());
    }

    /**
     * @dataProvider providerProperties
     */
    public function testGetEmail($properties): void
    {
        $contact = new Contact($properties);

        $this->assertEquals('bart@smartagents.com', $contact->getEmail());
    }

    /**
     * @dataProvider providerProperties
     */
    public function testGetFirstName($properties): void
    {
        $contact = new Contact($properties);

        $this->assertEquals('Bart', $contact->getFirstName());
    }

    /**
     * @dataProvider providerProperties
     */
    public function testGetLastName($properties): void
    {
        $contact = new Contact($properties);

        $this->assertEquals('Simpson', $contact->getLastName());
    }

    /**
     * @dataProvider providerProperties
     */
    public function testGetPhone($properties): void
    {
        $contact = new Contact($properties);

        $this->assertEquals('1234567890', $contact->getPhone());
    }

    public function providerProperties(): array
    {
        $properties =
            [
                'email' =>
                    [
                        'value' => 'bart@smartagents.com',
                        'versions' =>
                            [
                                0 =>
                                    [
                                        'value' => 'bart@smartagents.com',
                                        'source-type' => 'CONTACTS',
                                        'source-id' => 'CRM_UI',
                                        'source-label' => NULL,
                                        'timestamp' => 1567657426434,
                                        'selected' => false,
                                    ],
                            ],
                    ],
                'firstname' =>
                    [
                        'value' => 'Bart',
                        'versions' =>
                            [
                                0 =>
                                    [
                                        'value' => 'Bart',
                                        'source-type' => 'API',
                                        'source-id' => NULL,
                                        'source-label' => NULL,
                                        'timestamp' => 1567661904355,
                                        'selected' => false,
                                    ],
                            ],
                    ],
                'lastname' =>
                    [
                        'value' => 'Simpson',
                        'versions' =>
                            [
                                0 =>
                                    [
                                        'value' => 'Simpson',
                                        'source-type' => 'API',
                                        'source-id' => NULL,
                                        'source-label' => NULL,
                                        'timestamp' => 1567661904355,
                                        'selected' => false,
                                    ],
                            ],
                    ],
                'phone' =>
                    [
                        'value' => '1234567890',
                        'versions' =>
                            [
                                0 =>
                                    [
                                        'value' => '1234567890',
                                        'source-type' => 'API',
                                        'source-id' => NULL,
                                        'source-label' => NULL,
                                        'timestamp' => 1567661904355,
                                        'selected' => false,
                                    ],
                            ],
                    ],
            ];

        return [
            [$properties],
        ];
    }
}