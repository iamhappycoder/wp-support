<?php

namespace Tests\Authorify\Unit\Provider\HubSpot;

use Authorify\Provider\HubSpot\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testAppendApiKeyNotAnApiEndpoint(): void
    {
        $client = new Client('dummy-api-key');

        $endpoint = 'https://www.domain.com';
        $client->appendApiKey($endpoint);

        $this->assertEquals('https://www.domain.com', $endpoint);
    }

    public function testAppendApiKeyApiEndpointWithoutQuery(): void
    {
        $client = new Client('dummy-api-key');

        $endpoint = 'https://api.hubapi.com';
        $client->appendApiKey($endpoint);

        $this->assertEquals('https://api.hubapi.com?hapikey=dummy-api-key', $endpoint);
    }

    public function testAppendApiKeyApiEndpointWithQuery(): void
    {
        $client = new Client('dummy-api-key');

        $endpoint = 'https://api.hubapi.com?foo=bar';
        $client->appendApiKey($endpoint);

        $this->assertEquals('https://api.hubapi.com?foo=bar&hapikey=dummy-api-key', $endpoint);
    }
}
