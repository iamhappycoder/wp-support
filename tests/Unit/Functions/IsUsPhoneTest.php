<?php

namespace Tests\Authorify\Unit\Functions;

use PHPUnit\Framework\TestCase;

class IsUsPhoneTest extends TestCase
{
    /**
     * @dataProvider dataProviderInvalid
     */
    public function testInvalid(string $phoneNumber): void
    {
        $this->assertFalse(is_usphone($phoneNumber));
    }

    /**
     * @dataProvider dataProviderValid
     */
    public function testValid(string $phoneNumber): void
    {
        $this->assertTrue(is_usphone($phoneNumber));
    }

    public function dataProviderInvalid(): array
    {
        return [
            ['123456789'],
            ['123456789012'],
            ['+1123456789'],
            ['+1123456789012']
        ];
    }

    public function dataProviderValid(): array
    {
        return [
            ['1234567890'],
            ['+11234567890']
        ];
    }
}
