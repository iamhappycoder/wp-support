<?php

namespace Tests\Authorify\Unit\Functions;

use PHPUnit\Framework\TestCase;

class IsCaZipTest extends TestCase
{
    /**
     * @dataProvider dataProviderInvalid
     */
    public function testInvalid(string $zip): void
    {
        $this->assertFalse(is_cazip($zip));
    }

    /**
     * @dataProvider dataProviderValid
     */
    public function testValid(string $zip): void
    {
        $this->assertTrue(is_cazip($zip));
    }

    public function dataProviderInvalid(): array
    {
        return [
            // Excluded characters D, F, I, O, Q or U cannot be in any position.
            ['D1A 0B1'],
            ['F1A 0B1'],
            ['I1A 0B1'],
            ['O1A 0B1'],
            ['Q1A 0B1'],
            ['U1A 0B1'],
            ['K1D 0B1'],
            ['K1F 0B1'],
            ['K1I 0B1'],
            ['K1O 0B1'],
            ['K1Q 0B1'],
            ['K1U 0B1'],
            ['K1A 0D1'],
            ['K1A 0F1'],
            ['K1A 0I1'],
            ['K1A 0O1'],
            ['K1A 0Q1'],
            ['K1A 0U1'],

            // First character should not be W or Z.
            ['W1A 0B1'],
            ['Z1A 0B1'],

            // Cannot start with number.
            ['13A 0B1'],

            // Need space between first three and last three characters.
            ['K1A0B1'],

            // Not in ANA NAN format.
            ['1A2 0B1'],
            ['K1A A1B'],
        ];
    }

    public function dataProviderValid(): array
    {
        return [
            // W or Z can be any position except first.
            ['K1W 0B1'],
            ['K1Z 0B1'],
            ['K1A 0W1'],
            ['K1A 0Z1'],

            ['K1A 0B1'],
        ];
    }
}
