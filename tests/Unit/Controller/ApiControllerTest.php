<?php

namespace Tests\Authorify\Unit\Controller;

use Authorify\Controller\ApiController;
use Authorify\Exception\InternalErrorException;
use Authorify\Generator\ErrorIdGenerator;
use Authorify\Parameter\Validator\AbstractParameterValidator;
use Authorify\Service\ServiceConfig;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiControllerTest extends TestCase
{
    public function testHandleRequestV1InternalErrorLogged(): void
    {
        ($routeCollection = new RouteCollection())
            ->add('ll_orders_create', new Route(...['/limelight/orders', [], [], [], '', [], ['POST'], '']));

        $serviceConfigProphecy = $this->prophesize(ServiceConfig::class);
        $serviceConfigProphecy
            ->getRouteCollection()
            ->willReturn($routeCollection);

        $parameterValidatorProphecy = $this->prophesize(AbstractParameterValidator::class);
        $parameterValidatorProphecy
            ->validate(
                array (
                    'productId' =>
                        array (
                            0 => '10',
                        ),
                    'productQty' =>
                        array (
                            0 => '1',
                        ),
                    'offerId' => '14',
                    'billingModelId' => '2',
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '123-456-7890',
                    'email' => 'bart@smartagents.com',
                    'creditCardNumber' => '4111222233334444',
                    'expirationMonth' => '01',
                    'expirationYear' => '23',
                    'cvv' => '123',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingCountry' => 'US',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'shippingId' => '4',
                    'campaignId' => '4',
                    'affiliateId' => 'affiliate one',
                )
            );

        $serviceConfigProphecy
            ->getParameterValidator('ll_orders_create')
            ->willReturn($parameterValidatorProphecy);

        $utcDateTime = new \DateTimeImmutable('2019-07-05 20:38:23', new \DateTimeZone('UTC'));
        $serviceConfigProphecy
            ->callService(
                'll_orders_create',
                array (
                    'productId' =>
                        array (
                            '10',
                        ),
                    'productQty' =>
                        array (
                            '1',
                        ),
                    'offerId' => '14',
                    'billingModelId' => '2',
                    'firstName' => 'Bart',
                    'lastName' => 'Simpson',
                    'phone' => '123-456-7890',
                    'email' => 'bart@smartagents.com',
                    'creditCardNumber' => '4111222233334444',
                    'expirationMonth' => '01',
                    'expirationYear' => '23',
                    'cvv' => '123',
                    'billingAddress1' => '51 Pine St',
                    'billingAddress2' => 'some value',
                    'billingCity' => 'Atlantic Beach',
                    'billingCountry' => 'US',
                    'billingState' => 'FL',
                    'billingZip' => '32233',
                    'shippingId' => '4',
                    'campaignId' => '4',
                    'affiliateId' => 'affiliate one',
                    'ipAddress' => '127.0.0.1',
                    'utcDateTime' => $utcDateTime
                )
            )
            ->willThrow(new InternalErrorException('message id'));

        $errorIdGeneratorProphecy = $this->prophesize(ErrorIdGenerator::class);
        $errorIdGeneratorProphecy
            ->generate($utcDateTime)
            ->willReturn('error-id');

        $translatorProphecy = $this->prophesize(TranslatorInterface::class);
        $translatorProphecy
            ->trans('message id', [], 'messages')
            ->willReturn('error message');

        $loggerProphecy = $this->prophesize(LoggerInterface::class);
        $loggerProphecy
            ->critical(Argument::containingString('[error-id] error message'))->shouldBeCalled();

        $translatorProphecy
            ->trans('internal.error', ['%1%' => 'error-id'], 'messages')
            ->willReturn('internal error message');

        $apiController = new ApiController(
            $translatorProphecy->reveal(),
            $loggerProphecy->reveal(),
            $serviceConfigProphecy->reveal(),
            $errorIdGeneratorProphecy->reveal()
        );

        $params = [
            'productId' =>
                [
                    '10',
                ],
            'productQty' =>
                [
                    '1',
                ],
            'offerId' => '14',
            'billingModelId' => '2',
            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',
            'email' => 'bart@smartagents.com',
            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',
            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',
            'shippingId' => '4',
            'campaignId' => '4',
            'affiliateId' => 'affiliate one',
            'utcDateTime' => '2019-07-05 20:38:23',
        ];

        $attributes = [
            '_route' => 'api_v1',
            '_controller' => 'Authorify\Controller\ApiController::handleRequestV1',
            'uri' => 'limelight/orders',
            '_route_params' =>
                [
                    'uri' => 'limelight/orders',
                ],
        ];

        $request = Request::create(
            'http://localhost/v1/limelight/orders',
            'POST',
            $params,
            [],
            [],
            [
                'HTTP_USER_AGENT' => 'Symfony BrowserKit',
                'HTTP_HOST' => 'localhost',
                'HTTPS' => false,
            ]
        );
        $request->attributes->replace($attributes);

        $response = $apiController->handleRequestV1($request);

        $this->assertEquals('Symfony\Component\HttpFoundation\JsonResponse', get_class($response));
        $this->assertEquals(
            '{"status":"failure","message":"internal error message","code":null,"data":null}',
            $response->getContent()
        );
    }
}