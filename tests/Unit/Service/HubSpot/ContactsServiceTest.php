<?php

namespace Tests\Authorify\Unit\Service\HubSpot;

use Authorify\Provider\HubSpot\Manager\ContactsManager;
use Authorify\Service\HubSpot\ContactsService;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Tests\Authorify\Mother\Model\ServiceResultMother;

class ContactsServiceTest extends TestCase
{
    public function testGetContactByEmail(): void
    {
        $contactsManagerMock = $this->prophesize(ContactsManager::class);
        $contactsManagerMock
            ->getContactByEmail('bart.simpson@smartagents.com')
            ->shouldBeCalled()
            ->willReturn(['response data']);

        $contactService = new ContactsService($contactsManagerMock->reveal());
        $result = $contactService->getContactByEmail('bart.simpson@smartagents.com');

        $expected = ServiceResultMother::getByKey(0);

        $this->assertEquals($expected, $result);
    }

    public function testUpdateContactByEmail(): void
    {
        $contactsManagerMock = $this->prophesize(ContactsManager::class);
        $contactsManagerMock
            ->updateContactByEmail('bart.simpson@smartagents.com', ['foo' => 'bar'])
            ->shouldBeCalled()
            ->willReturn(['response data']);

        $contactService = new ContactsService($contactsManagerMock->reveal());
        $result = $contactService->updateContactByEmail('bart.simpson@smartagents.com', ['foo' => 'bar']);

        $expected = ServiceResultMother::getByKey(0);

        $this->assertEquals($expected, $result);
    }
}
