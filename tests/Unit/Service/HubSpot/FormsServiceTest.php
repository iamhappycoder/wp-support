<?php

namespace Tests\Authorify\Unit\Service\HubSpot;

use Authorify\Service\HubSpot\ConfigService;
use PHPUnit\Framework\TestCase;
use Authorify\Service\HubSpot\FormsService;
use Authorify\Provider\HubSpot\Validator\FormValidator;
use Authorify\Provider\HubSpot\Client;

class HubSpotFormsServiceTest extends TestCase
{
    const PORTAL_ID = 3424767;
    const FORMS = [
        'CheckAvailability' => [
            'formGuid' => '3e833204-9913-4856-bcd0-f1a8abf35267',
            'redirectUrl' => null,
            'successMessage' => 'Thank you!',
            'fields' => [
                'firstname' => ['required' => true],
                'zip' => ['required' => true, 'validation' => 'zipCode'],
                'email' => ['required' => true, 'validation' => 'email'],
                'phone' => ['required' => true, 'validation' => 'phoneNumber'],
            ]
        ],
        'WestWorld' => [
            'formGuid' => '3e833204-9913-4856-bcd0-f1a8abf35267',
            'redirectUrl' => 'http://www.westworld.com',
            'successMessage' => 'Thank you for your visit to WestWorld!',
            'fields' => [
                'goal' => ['required' => true]
            ]
        ]
    ];

    private $configServiceMock;
    private $clientMock;
    private $validatorMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->configServiceMock = $this->createMock(ConfigService::class);
        $this->clientMock = $this->createMock(Client::class);
        $this->validatorMock = $this->createMock(FormValidator::class);
    }

    public function testV1UnknownForm()
    {
        $data = [
            'firstname' => 'Bart',
            'zip' => '32233',
            'email' => 'bart.simpson@smartagents.com',
            'phone' => '+1.3867526534',
            '_id' => 'x',
            '_pageUrl' => 'http://www.authorify.com/alt-integration-test',
            '_pageName' => 'Authorify (alt-integration-test)'
        ];

        $hubSpotFormsService = new FormsService($this->configServiceMock, $this->clientMock, $this->validatorMock);
        $response = $hubSpotFormsService->v1($data, 'utk', '1.2.3.4');

        $expectedResponse = [
            'status' => 'fail',
            'message' => 'unknown form.',
            'data' => null
        ];

        $this->assertEquals($expectedResponse, $response);
    }

    public function testV1ValidationFailed()
    {
        $data = [
            'x' => 'Bart',
            'zip' => '32233',
            'email' => 'bart.simpson@smartagents.com',
            'phone' => '+1.3867526534',
            '_id' => 'CheckAvailability',
            '_pageUrl' => 'http://www.authorify.com/alt-integration-test',
            '_pageName' => 'Authorify (alt-integration-test)'
        ];

        $this->configServiceMock
            ->expects($this->once())
            ->method('getPortalId')
            ->with()
            ->willReturn(self::PORTAL_ID);

        $this->configServiceMock
            ->expects($this->once())
            ->method('getForms')
            ->with()
            ->willReturn(self::FORMS);

        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->with(self::FORMS['CheckAvailability']['fields'], $data)
            ->willReturn(['missing' => [], 'invalid' => ['firstname' => 'firstname validation message'], 'valid' => []]);

        $hubSpotFormsService = new FormsService($this->configServiceMock, $this->clientMock, $this->validatorMock);
        $response = $hubSpotFormsService->v1($data, 'utk', '1.2.3.4');

        $expectedResponse = [
            'status' => 'fail',
            'message' => 'validation failed.',
            'data' => ['missing' => [], 'invalid' => ['firstname' => 'firstname validation message'], 'valid' => []]
        ];

        $this->assertEquals($expectedResponse, $response);
    }

    public function testV1SuccessWithoutRedirect()
    {
        $hsContext = [
            'hutk' => 'utk',
            'ipAddress' => '1.2.3.4',
            'pageUrl' => 'http://www.authorify.com/alt-integration-test',
            'pageName' => 'Authorify (alt-integration-test)',
        ];

        $payload = [
            'firstname' => 'Bart',
            'zip' => '32233',
            'email' => 'bart.simpson@smartagents.com',
            'phone' => '+1.3867526534',
            'hs_context' => json_encode($hsContext)

        ];

        $data = [
            'firstname' => 'Bart',
            'zip' => '32233',
            'email' => 'bart.simpson@smartagents.com',
            'phone' => '+1.3867526534',
            '_id' => 'CheckAvailability',
            '_pageUrl' => 'http://www.authorify.com/alt-integration-test',
            '_pageName' => 'Authorify (alt-integration-test)'
        ];

        $this->configServiceMock
            ->expects($this->once())
            ->method('getPortalId')
            ->with()
            ->willReturn(self::PORTAL_ID);

        $this->configServiceMock
            ->expects($this->once())
            ->method('getForms')
            ->with()
            ->willReturn(self::FORMS);

        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->with(self::FORMS['CheckAvailability']['fields'], $data)
            ->willReturn(['missing' => [], 'invalid' => [], 'valid' => ['firstname' => 'Bart', 'zip' => '32233', 'email' => 'bart.simpson@smartagents.com', 'phone' => '+1.3867526534']]);

        $this->clientMock
            ->expects($this->once())
            ->method('request')
            ->with(
                'https://forms.hubspot.com/uploads/form/v2/3424767/3e833204-9913-4856-bcd0-f1a8abf35267',
                http_build_query($payload),
                ['Content-Type: application/x-www-form-urlencoded']
            )
            ->willReturn(['result' => 'abc', 'statusCode' => '123']);

        $hubSpotFormsService = new FormsService($this->configServiceMock, $this->clientMock, $this->validatorMock);

        $response = $hubSpotFormsService->v1($data, 'utk', '1.2.3.4');
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Thank you!',
            'data' => ['result' => 'abc', 'statusCode' => '123']
        ];

        $this->assertEquals($expectedResponse, $response);
    }

    public function testV1SuccessWithRedirect()
    {
        $hsContext = [
            'hutk' => 'utk',
            'ipAddress' => '1.2.3.4',
            'pageUrl' => 'http://www.authorify.com/alt-integration-test',
            'pageName' => 'Authorify (alt-integration-test)',
        ];

        $payload = [
            'goal' => 'unlock the maze',
            'hs_context' => json_encode($hsContext)
        ];

        $data = [
            'goal' => 'unlock the maze',
            '_id' => 'WestWorld',
            '_pageUrl' => 'http://www.authorify.com/alt-integration-test',
            '_pageName' => 'Authorify (alt-integration-test)'
        ];

        $forms = self::FORMS;
        $forms['WestWorld']['successMessage'] = null;

        $this->configServiceMock
            ->expects($this->once())
            ->method('getPortalId')
            ->with()
            ->willReturn(self::PORTAL_ID);

        $this->configServiceMock
            ->expects($this->once())
            ->method('getForms')
            ->with()
            ->willReturn($forms);

        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->with(self::FORMS['WestWorld']['fields'], $data)
            ->willReturn(['missing' => [], 'invalid' => [], 'valid' => ['goal' => 'unlock the maze']]);

        $this->clientMock
            ->expects($this->once())
            ->method('request')
            ->with(
                'https://forms.hubspot.com/uploads/form/v2/3424767/3e833204-9913-4856-bcd0-f1a8abf35267',
                http_build_query($payload),
                ['Content-Type: application/x-www-form-urlencoded']
            )
            ->willReturn(['result' => 'abc', 'statusCode' => '123']);

        $hubSpotFormsService = new FormsService($this->configServiceMock, $this->clientMock, $this->validatorMock);

        $response = $hubSpotFormsService->v1($data, 'utk', '1.2.3.4');
        $expectedResponse = [
            'status' => 'success',
            'message' => null,
            'data' => ['result' => 'abc', 'statusCode' => '123', 'redirectUrl' => 'http://www.westworld.com']
        ];

        $this->assertEquals($expectedResponse, $response);
    }

    public function testV1SuccessWithSuccessMessageInRedirect()
    {
        $hsContext = [
            'hutk' => 'utk',
            'ipAddress' => '1.2.3.4',
            'pageUrl' => 'http://www.authorify.com/alt-integration-test',
            'pageName' => 'Authorify (alt-integration-test)',
        ];

        $payload = [
            'goal' => 'unlock the maze',
            'hs_context' => json_encode($hsContext)
        ];

        $data = [
            'goal' => 'unlock the maze',
            '_id' => 'WestWorld',
            '_pageUrl' => 'http://www.authorify.com/alt-integration-test',
            '_pageName' => 'Authorify (alt-integration-test)'
        ];

        $this->configServiceMock
            ->expects($this->once())
            ->method('getPortalId')
            ->with()
            ->willReturn(self::PORTAL_ID);

        $this->configServiceMock
            ->expects($this->once())
            ->method('getForms')
            ->with()
            ->willReturn(self::FORMS);

        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->with(self::FORMS['WestWorld']['fields'], $data)
            ->willReturn(['missing' => [], 'invalid' => [], 'valid' => ['goal' => 'unlock the maze']]);

        $this->clientMock
            ->expects($this->once())
            ->method('request')
            ->with(
                'https://forms.hubspot.com/uploads/form/v2/3424767/3e833204-9913-4856-bcd0-f1a8abf35267',
                http_build_query($payload),
                ['Content-Type: application/x-www-form-urlencoded']
            )
            ->willReturn(['result' => 'abc', 'statusCode' => '123']);

        $hubSpotFormsService = new FormsService($this->configServiceMock, $this->clientMock, $this->validatorMock);

        $response = $hubSpotFormsService->v1($data, 'utk', '1.2.3.4');
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Thank you for your visit to WestWorld!',
            'data' => ['result' => 'abc', 'statusCode' => '123', 'redirectUrl' => 'http://www.westworld.com?msg=Thank+you+for+your+visit+to+WestWorld%21']
        ];

        $this->assertEquals($expectedResponse, $response);
    }
}
