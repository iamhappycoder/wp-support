<?php

namespace Tests\Authorify\Unit\Generator;

use Authorify\Generator\ErrorIdGenerator;
use PHPUnit\Framework\TestCase;

class ErrorIdGeneratorTest extends TestCase
{
    public function testGenerate(): void
    {
        $utcDateTime = new \DateTimeImmutable('2019-07-10 05:09:13', new \DateTimeZone('UTC'));
        $generator = new ErrorIdGenerator();

        $errorId = $generator->generate($utcDateTime);

        $this->assertRegExp('/^20190710050913-\d+$/', $errorId);
    }
}