<?php

namespace Tests\Authorify\Unit\Model\Bus;

use Authorify\Model\Bus\BusItemFactory;
use PHPUnit\Framework\TestCase;

class BusItemFactoryTest extends TestCase
{
    public function testCreateBusItemForAutoLogin(): void
    {
        $busItem = BusItemFactory::createBusItemForAutoLogin('bart.simpson@smartagents.com', 'Do-The-BartMan!', new \DateTimeImmutable('2020-03-15 03:13:13'));

        $this->assertEquals('bart.simpson@smartagents.com', $busItem->getEmail());
        $this->assertEquals('Do-The-BartMan!', $busItem->getPassword());
        $this->assertRegExp('/^\$2y\$/', $busItem->getEncryptedPassword());
        $this->assertRegExp('/^[a-fA-F0-9]{32}$/', $busItem->getToken());
        $this->assertEquals('2020-03-15 03:13:13', $busItem->getCreatedAt()->format('Y-m-d H:i:s'));
    }

    public function testCreateBusItemForAutoLoginfromArray(): void
    {
        $busItem = BusItemFactory::createBusItemForAutoLoginfromArray([
            'email' => 'homer.simpson@smartagents.com',
            'password' => 'Doh',
            'bcrypt' => 'the-encrypted-password',
            'token' => 'the-token',
            'created_at' => '2020-03-15 03:13:14',
        ]);

        $this->assertEquals('homer.simpson@smartagents.com', $busItem->getEmail());
        $this->assertEquals('Doh', $busItem->getPassword());
        $this->assertEquals('the-encrypted-password', $busItem->getEncryptedPassword());
        $this->assertEquals('the-token', $busItem->getToken());
        $this->assertEquals('2020-03-15 03:13:14', $busItem->getCreatedAt()->format('Y-m-d H:i:s'));
    }
}