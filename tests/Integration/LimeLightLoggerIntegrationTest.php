<?php

namespace Tests\Authorify\Integration;

use Monolog\Logger;

class LimeLightLoggerIntegrationTest extends AbstractIntegrationTestCase
{
    /**
     * @var Logger
     */
    private $limeLightLogger;

    private $logFile;

    public function setUp(): void
    {
        parent::setUp();

        $this->logFile = self::$container->get('kernel')->getLogDir() . '/limelight-test.log';
        if (file_exists($this->logFile)) {
            unlink($this->logFile);
        }

        $this->limeLightLogger = self::$container->get('monolog.logger.limelight');
    }

    public function testDebug()
    {
        $message = json_encode([
            'params' => [
                'creditCardNumber' => '4111222233335678',
                'CVV' => '123',
            ],
        ]);

        $this->limeLightLogger->debug($message);

        $messages = file($this->logFile);
        $lastMessage = $messages[count($messages) - 1];

        $this->assertStringContainsString('{"params":{"creditCardNumber":"XXXXXXXXXXXX5678","CVV":"XXX"}}', $lastMessage);
    }
}