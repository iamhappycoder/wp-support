<?php

namespace Tests\Authorify\Integration;

use phpDocumentor\Reflection\Types\Mixed_;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class AbstractIntegrationTestCase extends KernelTestCase
{
    protected const USE_MOCK = true;

    public function setUp(): void
    {
        self::bootKernel();
    }

    public function getParameter(string $name)
    {
        return self::$container->get('parameter_bag')->get($name);
    }
}