<?php

namespace Tests\Authorify\Integration\Trasport;

use Authorify\Transport\HttpTransport;
use PHPUnit\Framework\TestCase;

class HttpTransportIntegrationTest extends TestCase
{
    /** @var HttpTransport */
    protected $transport;

    public function setUp(): void
    {
        parent::setUp();

        $this->transport = new HttpTransport();
    }

    public function testRequestGet(): void
    {
        $response = $this->transport->request(
            HttpTransport::METHOD_GET,
            'https://postman-echo.com/get?param1=value1&param2=value2'
        );

        $response = json_decode($response, true);

        $this->assertEquals(['param1' => 'value1', 'param2' => 'value2'], $response['args']);
    }

    public function testRequestPost(): void
    {
        $response = $this->transport->request(
            HttpTransport::METHOD_POST,
            'https://postman-echo.com/post?param1=value1&param2=value2',
                ['param3' => 'value3', 'param4' => 'value4']
        );

        $response = json_decode($response, true);

        $this->assertEquals(['param1' => 'value1', 'param2' => 'value2'], $response['args']);
        $this->assertEquals(['{"param3":"value3","param4":"value4"}' => ''], $response['form']);
    }

    public function testRequestWithHeader(): void
    {
        $response = $this->transport->request(
            HttpTransport::METHOD_GET,
            'https://postman-echo.com/get?param1=value1&param2=value2',
            null,
            ['Content-Type: application/json']);

        $response = json_decode($response, true);

        $this->assertArrayHasKey('content-type', $response['headers']);
        $this->assertEquals('application/json', $response['headers']['content-type']);
    }
}