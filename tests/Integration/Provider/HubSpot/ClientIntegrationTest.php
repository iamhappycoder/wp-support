<?php

namespace Tests\Authorify\Integration\Provider\HubSpot;

use Authorify\Provider\HubSpot\Client as HubSpotClient;
use PHPUnit\Framework\TestCase;

class ClientIntegrationTest extends TestCase
{
    public function testRequest()
    {
        $utcDateTime = new \DateTime('now', new \DateTimeZone('UTC'));

        $hsContext = [
            'hutk' => 'test',
            'ipAddress' => '182.18.236.116',
            'pageUrl' => 'http://www.authorify.com/alt-test',
            'pageName' => 'Authorify (alt-test)',
        ];

        $endpoint = 'https://forms.hubspot.com/uploads/form/v2/5297547/cd3b04f7-6755-4c5e-bdd4-e60a81f8b169';
        $payload = [
            'firstname' => 'Bart-' . $utcDateTime->format('Y-m-d H:i:s'),
            'zip' => '32233',
            'email' => 'bart.simpson@smartagents.com',
            'phone' => '+1.3867526534',
            'best_meet_time' => 'As Soon As Possible',
            'hs_context' => json_encode($hsContext)
        ];
        $headers = ['Content-Type: application/x-www-form-urlencoded'];

        $hubSpotClient = new HubSpotClient('dummy-api-key');
        $response = $hubSpotClient->request(
            $endpoint,
            http_build_query($payload),
            $headers
        );

        $this->assertEquals(
            ['result' => '', 'statusCode' => 204],
            $response
        );
    }
}
