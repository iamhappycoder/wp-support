<?php

namespace Tests\Authorify\Integration\Provider\HubSpot\Manager;

use Authorify\Exception\ServerException;
use Authorify\Provider\HubSpot\Client;
use Authorify\Provider\HubSpot\Manager\ContactsManager;
use Tests\Authorify\Integration\AbstractIntegrationTestCase;

class ContactsManagerIntegrationTest extends AbstractIntegrationTestCase
{
    private $contactsManager;

    public function setUp(): void
    {
        parent::setUp();

        $apiKey = $this->getParameter('api.hubspot.key');
        
        $this->contactsManager = new ContactsManager(
            new Client($apiKey)
        );
    }

    public function testGetContactByEmailFailureInvalidEmail(): void
    {
        $this->expectExceptionObject(new ServerException('contact does not exist', 404));

        $this->contactsManager->getContactByEmail('bh_at_hubspot.com');
    }

    public function testGetContactByEmailFailureEmailNotFound(): void
    {
        $this->expectExceptionObject(new ServerException('contact does not exist', 404));

        $this->contactsManager->getContactByEmail('bhx@hubspot.com');
    }

    public function testGetContactByEmailSuccess(): void
    {
        $response = $this->contactsManager->getContactByEmail('bh@hubspot.com');

        $expected = [
            'vid' => '7101',
            'code' => '200',
        ];

        $result = $response['result'];
        $actual = [
            'vid' => $result['vid'],
            'code' => $response['statusCode'],
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testUpdateContactByEmailFailureInvalidEmail(): void
    {
        $this->expectExceptionObject(new ServerException('resource not found', 404));

        $this->contactsManager->updateContactByEmail('bh_at_hubspot.com', ['afy_book_credits' => '1']);
    }

    public function testUpdateContactByEmailFailureEmailNotFound(): void
    {
        $this->expectExceptionObject(new ServerException('resource not found', 404));

        $this->contactsManager->updateContactByEmail('bhx@hubspot.com', ['afy_book_credits' => '1']);
    }

    public function testUpdateContactByEmailFailureInvalidProperty(): void
    {
        $this->expectExceptionObject(new ServerException('Property values were not valid', 400));

        $this->contactsManager->updateContactByEmail('bh@hubspot.com', ['afy_book_creditsx' => '1']);
    }

    public function testUpdateContactByEmailFailureInvalidValue(): void
    {
        $this->expectExceptionObject(new ServerException('Property values were not valid', 400));

        $this->contactsManager->updateContactByEmail('bh@hubspot.com', ['afy_book_credits' => 'x']);
    }

    public function testUpdateContactByEmailSuccess(): void
    {
        $response = $this->contactsManager->updateContactByEmail('bh@hubspot.com', ['afy_book_credits' => '13']);

        $expected = [
            'code' => '204',
        ];

        $actual = [
            'code' => $response['statusCode'],
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testCreateContactFailContactAlreadyExists(): void
    {
        $this->expectExceptionObject(new ServerException('Contact already exists', 409));

        $this->contactsManager->createContact('bh@hubspot.com', ['afy_book_credits' => '0']);
    }

    public function testCreateContactPass(): void
    {
        $email = 'createtest' . time() . '@authorify.com';
        $response = $this->contactsManager->createContact($email, ['afy_book_credits' => '0']);

        $this->assertEquals(200, $response['statusCode']);
        $this->assertEquals($email, $response['result']['properties']['email']['value']);
        $this->assertEquals(0, $response['result']['properties']['afy_book_credits']['value']);
    }

    public function testCreateContactPropertyPass(): void
    {
        $this->markTestSkipped('Only for creating properties.');

        $property = [
            'name' => 'afy_last_login_date',
            'label' => 'AFY Last Login Date',
            'description' => 'Date of Last Login',
            'groupName' => 'authorify_app',
            'type' => 'datetime',
            'fieldType' => 'date',
            'formField' => true,
        ];

        $response = $this->contactsManager->createContactProperty($property);
    }
}
