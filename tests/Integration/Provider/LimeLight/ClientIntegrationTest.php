<?php

namespace Tests\Authorify\Integration\Provider;

use Authorify\Provider\LimeLight\Client;
use Authorify\Transport\TransportInterface;
use Psr\Log\LoggerInterface;
use Tests\Authorify\Integration\AbstractIntegrationTestCase;

class ClientIntegrationTest extends AbstractIntegrationTestCase
{
    /**
     * @var TransportInterface
     */
    private $transport;

    /**
     * @var LoggerInterface
     */
    private $limeLightLogger;

    public function setUp(): void
    {
        parent::setUp();

        $this->transport = self::$container->get(TransportInterface::class);
        $this->limeLightLogger = self::$container->get('monolog.logger.limelight');
    }

    public function testRequestSuccess()
    {
        $username = 'limelighttest';
        $password = 'CBaDfmPqHZc9zy';

        $params = [
            // Customer Info
            'email' => 'director@shield.com',
            'firstName' => 'Phil',
            'lastName' => 'Coulson',
            'phone' => '8135551212',

            // Card Info
            'creditCardType' => 'visa',
            'creditCardNumber' => '1444444444444440',
            'expirationDate' => '0628',
            'CVV' => '123',

            // Order Info
            'shippingId' => '4',
            'tranType' => 'Sale',
            'ipAddress' => '198.4.3.2',
            'campaignId' => '4',
            'products' => [
                '10' => [
                    'offer_id' => '14',
                    'billing_model_id' => '2',
                    'quantity' => '1'
                ]
            ],

            // Billing Info
            'billingFirstName' => 'Phil',
            'billingLastName' => 'Son of Coul',
            'billingAddress1' => '56 Escobar St',
            'billingAddress2' => 'FL 7',
            'billingCity' => 'Houston',
            'billingState' => 'TX',
            'billingZip' => '33655',
            'billingCountry' => 'US',
            'billingSameAsShipping' => 'YES',

            // Shipping Info
            'shippingFirstName' => 'Phil',
            'shippingLastName' => 'Son of Coul',
            'shippingAddress1' => '123 Medellin St',
            'shippingAddress2' => 'APT 7',
            'shippingCity' => 'Santo Alto',
            'shippingState' => 'TX',
            'shippingZip' => '33544',
            'shippingCountry' => 'US',

            'AFFID' => 'affiliate1',
        ];

        if (parent::USE_MOCK) {
            $transportMock = $this->prophesize(TransportInterface::class);
            $transportMock
                ->request(
                    'POST',
                    'https://authorify.limelightcrm.com/api/v1/new_order',
                    ["email" => "director@shield.com", "firstName" => "Phil", "lastName" => "Coulson", "phone" => "8135551212", "creditCardType" => "visa", "creditCardNumber" => "1444444444444440", "expirationDate" => "0628", "CVV" => "123", "shippingId" => "4", "tranType" => "Sale", "ipAddress" => "198.4.3.2", "campaignId" => "4", "products" => [10 => ["offer_id" => "14", "billing_model_id" => "2", "quantity" => "1"]], "billingFirstName" => "Phil", "billingLastName" => "Son of Coul", "billingAddress1" => "56 Escobar St", "billingAddress2" => "FL 7", "billingCity" => "Houston", "billingState" => "TX", "billingZip" => "33655", "billingCountry" => "US", "billingSameAsShipping" => "YES", "shippingFirstName" => "Phil", "shippingLastName" => "Son of Coul", "shippingAddress1" => "123 Medellin St", "shippingAddress2" => "APT 7", "shippingCity" => "Santo Alto", "shippingState" => "TX", "shippingZip" => "33544", "shippingCountry" => "US", "AFFID" => "affiliate1"],
                    ["Content-Type: application/json"],
                    'limelighttest',
                    'CBaDfmPqHZc9zy'
                )->willReturn('{"gateway_id":"7","response_code":"100","error_found":"0","order_id":"16290","transactionID":"Not Available","customerId":"7","authId":"Not Available","orderTotal":"5.02","orderSalesTaxPercent":"0.00","orderSalesTaxAmount":"0.00","test":"1","prepaid_match":"0","gatewayCustomerService":"12345","gatewayDescriptor":"Sandbox - Authorify","subscription_id":{"10":"2b1a3e52998fe6341d60f08a0cdf0a62"},"resp_msg":"Approved"}');
            $this->transport = $transportMock->reveal();
        }

        $client = new Client($this->transport, $this->limeLightLogger, $username, $password);
        $actual = $client->request('new_order', $params);

        $this->assertEquals('Approved', $actual['resp_msg']);
    }
}