<?php

namespace Tests\Authorify\Integration\Provider\LimeLight\Manager;

use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Manager\CustomerManager;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Authorify\Integration\AbstractIntegrationTestCase;

class CustomerManagerIntegrationTest extends AbstractIntegrationTestCase
{
    /**
     * @var CustomerManager
     */
    private $customerManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->customerManager = self::$container->get(CustomerManager::class);
    }

    public function testFindFailNotFound(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'customer_find',
                    [
                        'campaign_id' => 'all',
                        'start_date' => '01/01/1971',
                        'end_date' => '12/31/2099',
                        'criteria' => ['email' => 'notfound@smartagents.com'],
                        'search_type' => 'all',
                        'return_type' => 'customer_view',
                    ]
                )->willReturn(['response_code' => '604']);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->customerManager = new CustomerManager($clientMock, $translator);
        }

        $expected = ['response_code' => '604',];
        $actual = $this->customerManager->find('notfound@smartagents.com');

        $this->assertEquals($expected, $actual);
    }

    public function testFindPass(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'customer_find',
                    [
                        'campaign_id' => 'all',
                        'start_date' => '01/01/1971',
                        'end_date' => '12/31/2099',
                        'criteria' => ['email' => 'jamestkirk@starfleet.com'],
                        'search_type' => 'all',
                        'return_type' => 'customer_view',
                    ]
                )->willReturn([
                    'response_code' => '100',
                    'total_customers' => '1',
                    'customer_ids' => '17',
                    'data' => [
                        17 => [
                            'response_code' => '100',
                            'first_name' => 'James',
                            'last_name' => 'Kirk',
                            'email' => 'jamestkirk@starfleet.com',
                            'phone' => ' 11234567890',
                            'date_created' => '2018-11-15 15:02:04',
                            'order_count' => '2',
                            'order_list' => '10538,10539',
                        ]
                    ],
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->customerManager = new CustomerManager($clientMock, $translator);
        }

        $expected = [
            'response_code' => '100',
            'total_customers' => '1',
            'customer_ids' => '17',
            'data' => [
                17 => [
                'response_code' => '100',
                'first_name' => 'James',
                'last_name' => 'Kirk',
                'email' => 'jamestkirk@starfleet.com',
                'phone' => ' 11234567890',
                'date_created' => '2018-11-15 15:02:04',
                'order_count' => '2',
                'order_list' => '10538,10539',
                ]
            ],
        ];
        $actual = $this->customerManager->find('jamestkirk@starfleet.com');

        $this->assertEquals($expected, $actual);
    }
}