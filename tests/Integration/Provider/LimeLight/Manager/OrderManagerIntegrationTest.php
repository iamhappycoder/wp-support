<?php

namespace Tests\Authorify\Integration\Provider\LimeLight\Manager;

use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Manager\OrderManager;

use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Authorify\Integration\AbstractIntegrationTestCase;
use Tests\Authorify\Mother\Endpoint\LimeLight\CalculateTotalEndpointMother;
use Tests\Authorify\Mother\Endpoint\LimeLight\FindOrderEndpointMother;
use Tests\Authorify\Mother\Endpoint\LimeLight\UpdateOrderEndpointMother;
use Tests\Authorify\Mother\Endpoint\LimeLight\ViewOrderEndpointMother;
use Tests\Authorify\Mother\Provider\LimeLight\Manager\Order\CalculateTotalResultMother;
use Tests\Authorify\Mother\Provider\LimeLight\OrderMother;

class OrderManagerIntegrationTest extends AbstractIntegrationTestCase
{
    /**
     * @var OrderManager
     */
    private $orderManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->orderManager = self::$container->get(OrderManager::class);
    }

    public function testRequest(): void
    {
        $this->markTestSkipped();

        $order = OrderMother::getByKey(0);

        $result = $this->orderManager->create($order);

        $this->assertEquals([], $result);
    }

    public function testCalculateTotal(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_total/calculate',
                    ['campaign_id' => '4', 'shipping_id' => '4', 'use_tax_provider' => '1', 'offers' => [['id' => '14', 'product_id' => '18', 'billing_model_id' => '2', 'quantity' => '1']], 'location' => ['state' => 'FL', 'country' => 'US', 'postal_code' => '32102']],
                    '2'
                )->willReturn([
                    'status' => 'SUCCESS',
                    'data' => [
                        'total' => '5.54',
                        'subtotal' => '0.50',
                        'line_items' => [
                            [
                                'id' => 18,
                                'name' => 'Sandbox - Book Credits - 10',
                                'qty' => 1,
                                'base_price' => '0.50',
                                'unit_price' => '0.50',
                                'total' => '0.50',
                                'is_taxable' => true,
                                'is_shippable' => false,
                                'is_prepaid' => false,
                            ],
                        ],
                        'shipping' => '5.00',
                        'tax' => [
                            'pct' => '7.00',
                            'total' => '0.04',
                        ]
                    ]
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        $params = CalculateTotalEndpointMother::getWithFloridaLocationUpperCaseStateAndCountry();

        $result = $this->orderManager->calculateTotal($params);

        $this->assertEquals(CalculateTotalResultMother::getSandboxBookCredits10(), $result);
    }

    public function testCalculateTotalWithProductQuantityOverride(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_total/calculate',
                    ['campaign_id' => '4', 'shipping_id' => '4', 'use_tax_provider' => '1', 'offers' => [['id' => '14', 'product_id' => '18', 'billing_model_id' => '2', 'quantity' => '1']], 'location' => ['state' => 'FL', 'country' => 'US', 'postal_code' => '32102']],
                    '2'
                )->willReturn([
                    'status' => 'SUCCESS',
                    'data' => [
                        'total' => '6.07',
                        'subtotal' => '1.00',
                        'line_items' => [
                            [
                                'id' => 18,
                                'name' => 'Sandbox - Book Credits - 10',
                                'qty' => 2,
                                'base_price' => '0.50',
                                'unit_price' => '0.50',
                                'total' => '1.00',
                                'is_taxable' => true,
                                'is_shippable' => false,
                                'is_prepaid' => false,
                            ],
                        ],
                        'shipping' => '5.00',
                        'tax' => [
                            'pct' => '7.00',
                            'total' => '0.07',
                        ]
                    ]
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        $params = CalculateTotalEndpointMother::getWithFloridaLocationUpperCaseStateAndCountry();

        $result = $this->orderManager->calculateTotal($params, 2);

        $this->assertEquals(CalculateTotalResultMother::getSandboxBookCredits10Quantity2(), $result);
    }

    public function testFindSuccess(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_find',
                    ["start_date" => "01/01/1971", "end_date" => "12/31/2099", "campaign_id" => "all", "criteria" => ["email" => "findtest@authorify.com"], "search_type" => "all", "return_type" => "order_view"]
                )->willReturn([
                    'response_code' => '100'
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        list ($email) = FindOrderEndpointMother::getParams();

        $results = $this->orderManager->find($email);

        $this->assertEquals('100', $results['response_code']);
    }

    public function testFindRecurringSuccess(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_find',
                    ["start_date" => "01/01/1971", "end_date" => "12/31/2099", "campaign_id" => "all", "criteria" => ["recurring" => "1", "email" => "findtest@authorify.com"], "search_type" => "all", "return_type" => "order_view"]
                )->willReturn([
                    'response_code' => '100'
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        list ($email) = FindOrderEndpointMother::getParams();

        $results = $this->orderManager->findRecurring($email);

        $this->assertEquals('100', $results['response_code']);
    }

    public function testFindOverdueSuccess(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_find',
                    ["start_date" => "01/01/1971", "end_date" => "12/31/2099", "campaign_id" => "all", "criteria" => ["declines" => "1", "email" => "findtest@authorify.com"], "search_type" => "all", "return_type" => "order_view"]
                )->willReturn([
                    'response_code' => '100'
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        list ($email) = FindOrderEndpointMother::getParams();

        $results = $this->orderManager->findOverdue($email);

        $this->assertEquals('100', $results['response_code']);
    }

    public function testUpdate(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_update',
                    ['order_id' => [12901 => ['first_name' => 'Update', 'last_name' => 'Test', 'phone' => '1234567890', 'billing_first_name' => 'Update', 'billing_last_name' => 'Test', 'billing_address1' => 'Address 1', 'billing_address2' => 'Address 2', 'billing_city' => 'City 1', 'billing_state' => 'FL', 'billing_zip' => '32233', 'billing_country' => 'US', 'cc_number' => '4111222233334444', 'cc_expiration_date' => '12/99', 'cc_payment_type' => 'visa']]]
                )->willReturn([
                    'response_code' => '100'
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        list ($orderId, $customerInfo, $billingInfo, $paymentInfo) = UpdateOrderEndpointMother::getParams();

        $results = $this->orderManager->update($orderId, $customerInfo, $billingInfo, $paymentInfo);

        $this->assertTrue(in_array($results['response_code'], ['100', '911']));
    }

    public function testView(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'order_view',
                    ['order_id' => 12901]
                )->willReturn([
                    'response_code' => '100',
                    'order_id' => '12901'
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        list($orderId) = ViewOrderEndpointMother::getParams();

        $results = $this->orderManager->view($orderId);

        $this->assertEquals('100', $results['response_code']);
        $this->assertEquals('12901', $results['order_id']);
    }

    public function testForceBill(): void
    {
        $this->markTestSkipped('Brittle test');

        list ($email) = FindOrderEndpointMother::getParams();

        $findResults = $this->orderManager->findRecurring($email);
        $orderId = $findResults['order_id'][0];

        $results = $this->orderManager->forceBill($orderId);

        $this->assertEquals('100', $results['response_code']);
        $this->assertRegExp(
            "/The order was billed successfully\. The original order #\d+ was archived and recurring was stopped\.  New order ID#\d+ was created\./",
            $results['response_message']
        );
    }

    public function testUpdateSubscription(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'subscription_order_update',
                    [
                        'order_id' => 1,
                        'product_id' => 2,
                        'status' => 'reset',
                    ]
                )->willReturn([
                    'response_message' => 'Successfully updated status of the subscription for Product 2 for Order 1',
                    'response_code' => '100',
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->orderManager = new OrderManager($clientMock, $translator);
        }

        $updateResult = $this->orderManager->updateSubscription("1", "2");
        $this->assertEquals('100', $updateResult['response_code']);
    }
}