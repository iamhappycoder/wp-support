<?php

namespace Tests\Authorify\Integration\Provider\LimeLight\Manager;

use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Exception\ServerException;
use Authorify\Provider\LimeLight\Manager\PaymentsManager;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Authorify\Integration\AbstractIntegrationTestCase;
use Tests\Authorify\Mother\Endpoint\LimeLight\AuthorizePaymentEndpointMother;

class PaymentsManagerIntegrationTest extends AbstractIntegrationTestCase
{
    /**
     * @var PaymentsManager
     */
    private $paymentsManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->paymentsManager = self::$container->get(PaymentsManager::class);
    }

    public function testAuthorizeFailure(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'authorize_payment',
                    ['billingAddress1' => 'Billing Address 1', 'billingCity' => 'Billing City 1', 'billingState' => 'FL', 'billingZip' => '12345', 'billingCountry' => 'US', 'phone' => '1234567890', 'email' => 'alec.sadler@smartagents.com', 'creditCardType' => 'master', 'creditCardNumber' => '4111222233330000', 'expirationDate' => '1299', 'CVV' => '123', 'ipAddress' => '1.2.3.4', 'productId' => '18', 'campaignId' => '4']
                )->willThrow(new ServerException('This transaction has been declined', 10800));
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->paymentsManager = new PaymentsManager($clientMock, $translator);
        }

        $this->expectExceptionObject(new ServerException('This transaction has been declined', 10800));

        $params = AuthorizePaymentEndpointMother::getParamsDeclined();

        $this->paymentsManager->authorize($params);
    }

    public function testAuthorizeSuccess(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'authorize_payment',
                    ['billingAddress1' => 'Billing Address 1', 'billingCity' => 'Billing City 1', 'billingState' => 'FL', 'billingZip' => '12345', 'billingCountry' => 'US', 'phone' => '1234567890', 'email' => 'alec.sadler@smartagents.com', 'creditCardType' => 'master', 'creditCardNumber' => '4111222233334444', 'expirationDate' => '1299', 'CVV' => '123', 'ipAddress' => '1.2.3.4', 'productId' => '18', 'campaignId' => '4']
                )->willReturn([
                    'response_code' => '100',
                    'error_found' => '0'
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->paymentsManager = new PaymentsManager($clientMock, $translator);
        }

        $params = AuthorizePaymentEndpointMother::getParams();

        $result = $this->paymentsManager->authorize($params);

        $this->assertEquals('100', $result['response_code']);
        $this->assertEquals('0', $result['error_found']);
    }
}