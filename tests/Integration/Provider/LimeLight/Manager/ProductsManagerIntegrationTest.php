<?php

namespace Tests\Authorify\Integration\Provider\LimeLight\Manager;


use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Manager\ProductsManager;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Authorify\Integration\AbstractIntegrationTestCase;

class ProductsManagerIntegrationTest extends AbstractIntegrationTestCase
{
    /**
     * @var ProductsManager
     */
    private $productManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->productManager = self::$container->get(ProductsManager::class);
    }

    public function testGetByIdFailure(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'product_index',
                    ['product_id' => [-7]]
                )->willReturn([
                    'response_code' => 600
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->productManager = new ProductsManager($clientMock, $translator);
        }

        $result = $this->productManager->getById(-7);

        $this->assertEquals(600, $result['response_code']);
    }

    public function testGetByIdSuccess(): void
    {
        if (parent::USE_MOCK) {
            $client = $this->prophesize(Client::class);
            $client
                ->request(
                    'product_index',
                    ['product_id' => [7]]
                )->willReturn([
                    'response_code' => 100
                ]);
            $clientMock = $client->reveal();
            $translator = self::$container->get(TranslatorInterface::class);
            $this->productManager = new ProductsManager($clientMock, $translator);
        }

        $result = $this->productManager->getById(7);

        $this->assertEquals(100, $result['response_code']);
    }
}