<?php

namespace Tests\Authorify\Mother\Provider\LimeLight;

use Authorify\Provider\LimeLight\Model\Address;
use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class AddressMother implements MotherInterface
{
    use MotherTrait;

    public static $data = [
        0 => ['Firstname 1', 'Lastname 1', 'Address1 1', 'Address2 1', 'City 1', 'State 1', 'Zip 1', 'Country 1'],
        1 => ['Firstname 2', 'Lastname 2', 'Address1 2', 'Address2 2', 'City 2', 'State 2', 'Zip 2', 'Country 2'],

        2 => ['Homer', 'Simpson', '56 Escobar St', 'FL 7', 'Houston', 'TX', '33655', 'US'],
        3 => ['Maggie', 'Simpson', '123 Medellin St', 'APT 7', 'Santo Alto', 'TX', '33544', 'US'],

        4 => ['Bart', 'Simpson', 'Some street', 'Some apartment', 'Some city', 'FL', '32102', 'US'],
    ];

    public static function createObject(array $params): Object
    {
        list ($firstName, $lastName, $address1, $address2, $city, $state, $zip, $country) = $params;

        return new Address(
            $firstName,
            $lastName,
            $address1,
            $address2,
            $city,
            $state,
            $zip,
            $country
        );
    }
}