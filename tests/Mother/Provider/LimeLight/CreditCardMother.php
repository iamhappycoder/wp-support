<?php

namespace Tests\Authorify\Mother\Provider\LimeLight;

use Authorify\Provider\LimeLight\Model\CreditCard;
use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class CreditCardMother implements MotherInterface
{
    use MotherTrait;

    public static $data = [
        0 => ['type 1', '4umber 1', '2018-10-15 22:22:22', 'cvv 1'],
        1 => ['type 2', '4umber 2', '2018-10-16 22:22:22', 'cvv 2'],

        2 => ['visa', '4444444444444440', '0628', '123'],
    ];

    public static function createObject(array $params): Object
    {
        list ($type, $number, $expiration, $cvv) = $params;

        return new CreditCard(
            $type,
            $number,
            new \DateTimeImmutable($expiration, new \DateTimeZone('UTC')),
            $cvv
        );
    }
}