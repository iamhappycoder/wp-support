<?php

namespace Tests\Authorify\Mother\Provider\LimeLight;

use Authorify\Provider\LimeLight\Model\Product;
use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class ProductMother implements MotherInterface
{
    use MotherTrait;

    public static $data = [
        0 => [1, 2, 3, 4],
        1 => [5, 6, 7, 8],
        2 => [9, 10, 11, 12],

        3 => [10, 14, 2, 1],

        4 => [18, 14, 2, 1],
    ];

    public static function createObject(array $params): Object
    {
        list ($id, $offerId, $billingModelId, $quantity) = $params;

        return new Product(
            $id,
            $offerId,
            $billingModelId,
            $quantity
        );
    }
}