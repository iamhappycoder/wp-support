<?php

namespace Tests\Authorify\Mother\Provider\LimeLight;

class OrdersEndpointMother
{
    public static function getCreateParamsWithInvalidProductId(): array
    {
        $params = self::getCreateParams();
        $params['shippingId'] = '1234567890';
        $params['productQty']['10'] = '2';

        return $params;
    }

    public static function getCreateParams(): array
    {
        return [
            'productId' => ['10'],
            'productQty' => ['10' => '2'],
            'offerId' => '14',
            'billingModelId' => '2',

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',
            'email' => 'bart@smartagents.com',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',

            'shippingId' => '4',
            'campaignId' => '4',

            'affiliateId' => 'affiliate one'
        ];
    }

    public static function getCreateParamsMultipleProductIds(): array
    {
        $params = self::getCreateParams();
        $params['productId'][] = '14';
        $params['productQty']['14'] = '1';

        return $params;
    }

    public static function getCreateParamsAddress2NotSent(): array
    {
        return [
            'productId' => ['10'],
            'productQty' => ['10' => '2'],
            'offerId' => '14',
            'billingModelId' => '2',

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',
            'email' => 'bart@smartagents.com',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingCity' => 'Atlantic Beach',
            'billingState' => 'FL',
            'billingZip' => '32233',
            'billingCountry' => 'US',

            'shippingId' => '4',
            'campaignId' => '4'
        ];
    }

    public static function getCreateParamsDeclined(): array
    {
        $params = self::getCreateParams();
        $params['email'] = 'declinetest3@authorify.com';
        $params['creditCardNumber'] = '4111222233330000';

        return $params;
    }

    public static function getAddonParams(): array
    {
        return [
            'email' => 'bart.simpsons@smartagents.com',
            'previousOrderId' => '10134',
            'productId' => ['7'],
            'productQty' => ['7' => '1'],
            'trialProductId' => ['7' => ''],
            'offerId' => '14',
            'billingModelId' => '2',

            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getAddonMultipleParamsOneProduct(): array
    {
        return [
            'offers' => [
                7 => [
                    'offerId' => '14',
                    'billingModelId' => '2',
                    'productQty' => '1',
                ],
            ],
            'email' => 'bart.simpsons@smartagents.com',
            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getAddonMultipleParamsTwoProducts(): array
    {
        return [
            'offers' => [
                7 => [
                    'offerId' => '14',
                    'billingModelId' => '2',
                    'productQty' => '1',
                ],
                14 => [
                    'offerId' => '14',
                    'billingModelId' => '2',
                    'productQty' => '1',
                ],

            ],
            'email' => 'bart.simpsons@smartagents.com',
            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithNoPreviousOrderId(): array
    {
        return [
            'email' => 'bart.simpsons@smartagents.com',
            'productId' => ['7'],
            'productQty' => ['7' => '2'],
            'offerId' => '14',
            'billingModelId' => '2',

            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithNoPreviousOrderIdAndMultipleProductIds(): array
    {
        return [
            'productId' => ['7', '14'],
            'productQty' => ['7' => '1', '14' => '1'],
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }

    public static function getCreateParamsWithScalarProductIdAndQty(): array
    {
        $params = self::getCreateParams();

        $params['productId'] = $params['productId'][0];
        $params['productQty'] = $params['productQty'][$params['productId']];

        return $params;
    }

    public static function getCreateParamsWithScalarProductId(): array
    {
        $params = self::getCreateParams();

        $params['productId'] = $params['productId'][0];

        return $params;
    }

    public static function getCreateParamsWithScalarProductQty(): array
    {
        $params = self::getCreateParams();

        $params['productQty'] = $params['productQty'][10];

        return $params;
    }

    public static function getAddonParamsWithScalarProductId(): array
    {
        return [
            'email' => 'bart.simpsons@smartagents.com',
            'productId' => '7',
            'productQty' => ['7' => '2'],
            'offerId' => '14',
            'billingModelId' => '2',

            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithScalarProductQty(): array
    {
        return [
            'email' => 'bart.simpsons@smartagents.com',
            'productId' => ['7'],
            'productQty' => '2',
            'offerId' => '14',
            'billingModelId' => '2',

            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithScalarProductIdAndQty(): array
    {
        return [
            'email' => 'bart.simpsons@smartagents.com',
            'productId' => '7',
            'productQty' => '2',
            'offerId' => '14',
            'billingModelId' => '2',

            'campaignId' => '4',
            'shippingId' => '4',
        ];
    }

    public static function getFindOrderParams(): array
    {
        return [
            'campaignId' => '4',
            'email' => 'findtest@authorify.com',
        ];
    }

    public static function getUpdateOrderParamsUnknownEmail(): array
    {
        $params = self::getUpdateOrderParams();
        $params['firstName'] = 'Kiera';
        $params['lastName'] = 'Cameron';
        $params['email'] = 'kiera.cameron@smartagents.com';

        return $params;
    }

    public static function getUpdateOrderParams(): array
    {
        return [
            'firstName' => 'James',
            'lastName' => 'Kirk',
            'phone' => '123-456-7890',
            'email' => 'updatetest@authorify.com',
            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '12',
            'expirationYear' => '99',
            'cvv' => '123',
            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',
            'isCalculateOnly' => '0',
        ];
    }

    public static function getUpdateOrderParamsEmailIsUrlEncoded(): array
    {
        $params = self::getUpdateOrderParams();
        $params['email'] = 'updatetest%40authorify.com';

        return $params;
    }

    public static function getSubscribeParams(): array
    {
        return [
            'productId' => ['35'],
            'productQty' => ['35' => '2'],
            'offerId' => '20',
            'billingModelId' => '8',

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',
            'email' => 'bart@smartagents.com',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',

            'shippingId' => '3',
            'campaignId' => '4',

            'affiliateId' => 'affiliate subscribe'
        ];
    }

    public static function getSubscribeParamsEmptyTrialProductId(): array
    {
        return [
            'productId' => '35',
            'trialProductId' => '',
            'productQty' => '2',
            'offerId' => '20',
            'billingModelId' => '8',

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',
            'email' => 'bart@smartagents.com',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',

            'shippingId' => '3',
            'campaignId' => '4',

            'affiliateId' => 'affiliate subscribe'
        ];
    }

    public static function getCreateParamsWithProductBump(): array
    {
        return [
            'productId' => ['44'],
            'productQty' => ['44' => '2'],
            'offerId' => '37',
            'billingModelId' => '8',
            'trialProductId' => ['44' => '47'],

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',
            'email' => 'bart@smartagents.com',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',

            'shippingId' => '4',
            'campaignId' => '4',

            'affiliateId' => 'affiliate one',

            'productBumpId' => '20',
            'productBumpQty' => '2',
            'productBumpOfferId' => '14',
            'productBumpBillingModelId' => '2',
        ];
    }

    public static function getCreateParamsWithUtmParameters(): array
    {
        $params = self::getCreateParams();
        unset($params['affiliateId']);

        $params['utm_source'] = 'source';
        $params['utm_campaign'] = 'campaign';
        $params['utm_medium'] = 'medium';
        $params['utm_term'] = 'term';

        return $params;
    }

    public static function getCreateParamsWithInvalidFlow(): array
    {
        $params = self::getCreateParams();
        $params['flow'] = 'the-bart-man';

        return $params;
    }
}