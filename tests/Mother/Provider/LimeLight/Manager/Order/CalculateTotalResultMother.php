<?php

namespace Tests\Authorify\Mother\Provider\LimeLight\Manager\Order;


class CalculateTotalResultMother
{
    public static function getSandboxBookCredits10(): array
    {
        return [
          'status' => 'SUCCESS',
          'data' =>
          [
            'total' => '5.54',
            'subtotal' => '0.50',
            'line_items' =>
            [
              [
                'id' => 18,
                'name' => 'Sandbox - Book Credits - 10',
                'qty' => 1,
                'base_price' => '0.50',
                'unit_price' => '0.50',
                'total' => '0.50',
                'is_taxable' => true,
                'is_shippable' => false,
                'is_prepaid' => false,
              ],
            ],
            'shipping' => '5.00',
            'tax' =>
            [
              'pct' => '7.00',
              'total' => '0.04',
            ],
          ],
        ];
    }

    public static function getSandboxBookCredits10Quantity2(): array
    {
        return [
          'status' => 'SUCCESS',
          'data' =>
          [
            'total' => '6.07',
            'subtotal' => '1.00',
            'line_items' =>
            [
              [
                'id' => 18,
                'name' => 'Sandbox - Book Credits - 10',
                'qty' => 2,
                'base_price' => '0.50',
                'unit_price' => '0.50',
                'total' => '1.00',
                'is_taxable' => true,
                'is_shippable' => false,
                'is_prepaid' => false,
              ],
            ],
            'shipping' => '5.00',
            'tax' =>
            [
              'pct' => '7.00',
              'total' => '0.07',
            ],
          ],
        ];
    }
}