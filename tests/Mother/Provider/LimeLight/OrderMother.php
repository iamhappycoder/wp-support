<?php

namespace Tests\Authorify\Mother\Provider\LimeLight;

use Authorify\Provider\LimeLight\Model\Order;

use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class OrderMother implements MotherInterface
{
    use MotherTrait;

    public static $data = [
        0 => [114, 'Sale', '1.2.3.4', 4, 0, ['key' => 3]],
        1 => [4, 'Sale', '1.2.3.4', 4, 2, ['key' => 4]],
    ];

    public static function createObject(array $params): Object
    {
        list ($shippingId, $transactionType, $ipAddress, $campaignId, $customerMotherKey, $productMotherKeys) = $params;

        $customer = CustomerMother::getByKey($customerMotherKey);
        $products = self::getProducts($productMotherKeys);
        
        return new Order(
            $shippingId,
            $transactionType,
            $ipAddress,
            $campaignId,
            true,
            $customer,
            $products
        );
    }
    
    protected static function getProducts(array $keys): array
    {
        $products = [];
                
        if (isset($keys['key'])) {
            $products = [ProductMother::getByKey($keys['key'])];
        } else if (isset($keys['range'])) {
            list ($beginKey, $endKey) = $keys['range'];
            
            $products = ProductMother::getByRange($beginKey, $endKey);
        } else if (isset($keys['list'])) {
            $products = ProductMother::getByList(...$keys['list']);
        }
        
        return $products;
    }
}