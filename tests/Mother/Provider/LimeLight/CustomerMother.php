<?php

namespace Tests\Authorify\Mother\Provider\LimeLight;

use Authorify\Provider\LimeLight\Model\Customer;
use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class CustomerMother implements MotherInterface
{
    use MotherTrait;

    public static $data = [
        0 => ['bart@smartagents.com', 'Bart', 'Simpson', '1234567890', ['key' => 2], ['billing' => 2, 'shipping' => 3]],
        1 => ['homer@smartagents.com', 'Homer', 'Simpson', '1234567891', ['key' => 2], ['billing' => 2, 'shipping' => 3]],
        2 => ['bart@smartagents.com', 'Bart', 'Simpson', '1234567891', ['key' => 2], ['billing' => 4, 'shipping' => 3]],
    ];

    public static function createObject(array $params): Object
    {
        list ($email, $firstName, $lastName, $phone, $creditCardMotheryKeys, $addressMotherKeys) = $params;

        $creditCards = self::getCreditCards($creditCardMotheryKeys);
        $addresses = self::getAddresses($addressMotherKeys);
        
        return new Customer(
            $email,
            $firstName,
            $lastName,
            $phone,
            $creditCards,
            $addresses
        );
    }
    
    public static function getCreditCards(array $keys): array
    {
        $creditCards = [];
        
        if (isset($keys['key'])) {
            $creditCards = [CreditCardMother::getByKey($keys['key'])];
        } else if (isset($keys['range'])) {
            list ($beginKey, $endKey) = $keys['range'];
            
            $creditCards = CreditCardMother::getByRange($beginKey, $endKey);
        } else if (isset($keys['list'])) {
            $creditCards = CreditCardMother::getByList(...$keys['list']);
        }
        
        return $creditCards;
    }
    
    public static function getAddresses(array $keys): array
    {
        $addresses = [];

        foreach ($keys as $type => $key) {
            $addresses[$type] = AddressMother::getByKey($key);
        }
        
        return $addresses;
    }
}