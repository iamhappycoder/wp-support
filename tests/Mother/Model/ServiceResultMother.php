<?php

namespace Tests\Authorify\Mother\Model;

use Authorify\Model\ServiceResult;
use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class ServiceResultMother implements MotherInterface
{
    use MotherTrait;

    public static $data = [
        0 => ['success', null, null, ['response data']]
    ];

    public static function createObject(array $params): Object
    {
        list ($status, $errorMessage, $errorCode, $data) = $params;

        return (new ServiceResult($status, $errorMessage, $errorCode))
            ->setData($data);
    }

}