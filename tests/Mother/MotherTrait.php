<?php

namespace Tests\Authorify\Mother;

trait MotherTrait
{
    public static function getByKey(int $key): Object
    {
        return self::createObject(self::$data[$key]);
    }
    
    public static function getByRange(int $beginKey, int $endKey): array
    {
        $data = [];

        for ($i = $beginKey; $i <= $endKey; ++$i) {
            $data[] = self::createObject(self::$data[$i]);
        }

        return $data;
    }
    
    public static function getByList(array $keys): array
    {
        $data = [];

        foreach ($keys as $key) {
            $data[] = self::createObject(self::$data[$key]);
        }

        return $data;
    }
}