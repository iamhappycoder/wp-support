<?php

namespace Tests\Authorify\Mother;

interface MotherInterface
{
    public static function createObject(array $params): Object;
}