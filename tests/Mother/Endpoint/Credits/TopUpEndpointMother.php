<?php

namespace Tests\Authorify\Mother\Endpoint\Credits;


class TopUpEndpointMother
{
    public static function getCreateParamsWithInvalidProductId(): array
    {
        $params = self::getCreateParams();
        $params['shippingId'] = '1234567890';

        return $params;
    }

    public static function getCreateParams(): array
    {
        return [
            'productId' => ['18'],
            'productQty' => ['18' => '10'],
            'offerId' => '14',
            'billingModelId' => '2',

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingAddress2' => 'some value',
            'billingCity' => 'Atlantic Beach',
            'billingCountry' => 'US',
            'billingState' => 'FL',
            'billingZip' => '32233',

            'shippingId' => '4',
            'campaignId' => '4'
        ];
    }

    public static function getCreateParamsMultipleProductIds(): array
    {
        $params = self::getCreateParams();
        $params['productId'][] = '14';
        $params['productQty']['14'] = '11';

        return $params;
    }

    public static function getCreateParamsAddress2NotSent(): array
    {
        return [
            'productId' => ['18'],
            'productQty' => ['18' => '10'],
            'offerId' => '14',
            'billingModelId' => '2',

            'firstName' => 'Bart',
            'lastName' => 'Simpson',
            'phone' => '123-456-7890',

            'creditCardNumber' => '4111222233334444',
            'expirationMonth' => '01',
            'expirationYear' => '23',
            'cvv' => '123',

            'billingAddress1' => '51 Pine St',
            'billingCity' => 'Atlantic Beach',
            'billingState' => 'FL',
            'billingZip' => '32233',
            'billingCountry' => 'US',

            'shippingId' => '4',
            'campaignId' => '4'
        ];
    }

    public static function getCreateParamsDeclined(): array
    {
        $params = self::getCreateParams();
        $params['creditCardNumber'] = '4111222233330000';

        return $params;
    }

    public static function getAddonParams(): array
    {
        return [
            'previousOrderId' => '10134',
            'productId' => ['7'],
            'productQty' => ['7' => '1'],
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithNoPreviousOrderId(): array
    {
        return [
            'productId' => ['7'],
            'productQty' => ['7' => '1'],
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithNoPreviousOrderIdAndMultipleProductIds(): array
    {
        return [
            'productId' => ['7', '14'],
            'productQty' => ['7' => '1', '14' => '1'],
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }

    public static function getCreateParamsWithScalarProductIdAndQty(): array
    {
        $params = self::getCreateParams();

        $params['productId'] = $params['productId'][0];
        $params['productQty'] = $params['productQty'][$params['productId']];

        return $params;
    }

    public static function getCreateParamsWithScalarProductId(): array
    {
        $params = self::getCreateParams();

        $params['productId'] = $params['productId'][0];

        return $params;
    }

    public static function getCreateParamsWithScalarProductQty(): array
    {
        $params = self::getCreateParams();

        $params['productQty'] = $params['productQty'][18];

        return $params;
    }

    public static function getAddonParamsWithScalarProductId(): array
    {
        return [
            'productId' => '7',
            'productQty' => ['7' => '1'],
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithScalarProductQty(): array
    {
        return [
            'productId' => ['7'],
            'productQty' => '1',
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }

    public static function getAddonParamsWithScalarProductIdAndQty(): array
    {
        return [
            'productId' => '7',
            'productQty' => '1',
            'offerId' => '14',
            'billingModelId' => '2',

            'shippingId' => '4',
        ];
    }}