<?php

namespace Tests\Authorify\Mother\Endpoint\LimeLight;

class UpdateOrderEndpointMother
{
    public static function getParams(): array
    {
        return [
            '12901',
            [
                'first_name' => 'Update',
                'last_name' => 'Test',
                'phone' => '1234567890',
            ],
            [
                'billing_first_name' => 'Update',
                'billing_last_name' => 'Test',
                'billing_address1' => 'Address 1',
                'billing_address2' => 'Address 2',
                'billing_city' => 'City 1',
                'billing_state' => 'FL',
                'billing_zip' => '32233',
                'billing_country' => 'US',
            ],
            [
                'cc_number' => '4111222233334444',
                'cc_expiration_date' => '12/99',
                'cc_payment_type' => 'visa',
            ]
        ];
    }
}