<?php

namespace Tests\Authorify\Mother\Endpoint\LimeLight;

class AuthorizePaymentEndpointMother
{
    public static function getParams(): array
    {
        return [
            'billingAddress1' => 'Billing Address 1',
            'billingCity' => 'Billing City 1',
            'billingState' => 'FL',
            'billingZip' => '12345',
            'billingCountry' => 'US',
            'phone' => '1234567890',
            'email' => 'alec.sadler@smartagents.com',
            'creditCardType' => 'master',
            'creditCardNumber' => '4111222233334444',
            'expirationDate' => '1299',
            'CVV' => '123',
            'ipAddress' => '1.2.3.4',
            'productId' => '18',
            'campaignId' => '4',
        ];
    }

    public static function getParamsDeclined(): array
    {
        $params = self::getParams();
        $params['creditCardNumber'] = '4111222233330000';

        return $params;
    }
}