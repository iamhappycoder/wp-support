<?php

namespace Tests\Authorify\Mother\Endpoint\LimeLight;


use Tests\Authorify\Mother\MotherInterface;
use Tests\Authorify\Mother\MotherTrait;

class SubscriptionEndpointMother
{
    public static function getUpdateParams(): array
    {
        return [
            'orderId' => '10834',
            'productId' => '10',
            'status' => 'start',
        ];
    }
}