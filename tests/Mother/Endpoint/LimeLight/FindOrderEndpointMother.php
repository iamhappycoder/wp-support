<?php

namespace Tests\Authorify\Mother\Endpoint\LimeLight;

class FindOrderEndpointMother
{
    public static function getParams(): array
    {
        return [
            'findtest@authorify.com',
        ];
    }

    public static function getParamsUnknownEmail(): array
    {
        return [
            'kiera.cameron@smartagents.com',
        ];
    }
}