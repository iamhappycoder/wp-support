<?php

namespace Tests\Authorify\Mother\Endpoint\LimeLight;

class CalculateTotalEndpointMother
{
    public static function getWithFloridaLocationLowerCaseStateAndCountry(): array
    {
        return [
            'campaignId' => '4',
            'shippingId' => '4',
            'offers' => [
                [
                    'id' => '14',
                    'productId' => '18',
                    'billingModelId' => '2',
                    'productQty' => '2',
                ],
            ],
            'location' => [
                'state' => 'fl',
                'country' => 'us',
                'zip' => '32102',
            ]
        ];
    }

    public static function getWithFloridaLocationUpperCaseStateAndCountry(): array
    {
        return [
            'campaignId' => '4',
            'shippingId' => '4',
            'offers' => [
                [
                    'id' => '14',
                    'productId' => '18',
                    'billingModelId' => '2',
                    'productQty' => '2',
                ],
            ],
            'location' => [
                'state' => 'FL',
                'country' => 'US',
                'zip' => '32102',
            ]
        ];
    }
}