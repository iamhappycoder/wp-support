<?php

namespace Tests\Authorify\Mother\Endpoint\LimeLight;

class ViewOrderEndpointMother
{
    public static function getParams(): array
    {
        return [
            '12901',
        ];
    }
}