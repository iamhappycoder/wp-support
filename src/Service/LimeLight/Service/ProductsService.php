<?php

namespace Authorify\Service\LimeLight\Service;

use Authorify\Model\ServiceResult;
use Authorify\Provider\LimeLight\Manager\ProductsManager;

class ProductsService
{
    /** @var ProductsManager */
    protected $productsManager;

    public function __construct(ProductsManager $productsManager)
    {
        $this->productsManager = $productsManager;
    }

    public function getById(array $params): ServiceResult
    {
        $response = $this->productsManager->getById($params['id']);

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }
}