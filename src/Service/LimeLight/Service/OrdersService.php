<?php

namespace Authorify\Service\LimeLight\Service;

use Authorify\Exception\InternalErrorException;
use Authorify\Exception\ServerException;
use Authorify\Exception\ServiceException;
use Authorify\Model\ServiceResult;
use Authorify\Provider\HubSpot\Manager\ContactsManager;
use Authorify\Provider\HubSpot\Model\Contact;
use Authorify\Provider\LimeLight\Manager\CustomerManager;
use Authorify\Provider\LimeLight\Manager\OrderManager as OrdersManager;
use Authorify\Provider\LimeLight\Manager\PaymentsManager;
use Authorify\Provider\LimeLight\Model\Address;
use Authorify\Provider\LimeLight\Model\CreditCard;
use Authorify\Provider\LimeLight\Model\Customer;
use Authorify\Provider\LimeLight\Model\Order;
use Authorify\Provider\LimeLight\Model\Product;
use Authorify\Model\Bus\BusInterface;
use Authorify\Model\Bus\BusItemFactory;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrdersService
{
    /** @var OrdersManager */
    protected $ordersManager;

    /** @var PaymentsManager */
    protected $paymentsManager;

    /** @var CustomerManager */
    protected $customerManager;

    /** @var ContactsManager */
    protected $contactsManager;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var LoggerInterface */
    protected $logger;

    /** @var BusInterface */
    protected $busService;

    protected $limeLightAuthorizationCampaignId;
    protected $limeLightAuthorizationProductId;

    public function __construct(
        OrdersManager $ordersManager,
        PaymentsManager $paymentsManager,
        CustomerManager $customerManager,
        ContactsManager $contactsManager,
        TranslatorInterface $translator,
        LoggerInterface $logger,
        BusInterface $busService,
        int $limeLightAuthorizationCampaignId,
        int $limeLightAuthorizationProductId
    ) {
        $this->ordersManager = $ordersManager;
        $this->paymentsManager = $paymentsManager;
        $this->customerManager = $customerManager;
        $this->contactsManager = $contactsManager;
        $this->translator = $translator;
        $this->logger = $logger;
        $this->busService = $busService;

        $this->limeLightAuthorizationCampaignId = $limeLightAuthorizationCampaignId;
        $this->limeLightAuthorizationProductId = $limeLightAuthorizationProductId;
    }

    public function create(array $params): ServiceResult
    {
        return $this->newOrder($params);
    }

    public function addon(array $params): ServiceResult
    {
        $products = [];
        foreach ($params['productId'] as $productId) {
            $products[] = new Product(
                $productId,
                $params['offerId'],
                $params['billingModelId'],
                $params['productQty'][$productId]
            );
        }

        $order = new Order(
            $params['shippingId'],
            null,
            null,
            $params['campaignId'],
            null,
            null,
            $products
        );

        $order->setPreviousOrderId($params['previousOrderId']);

        $response = $this->ordersManager->addon($order);

        $credits = $this->addCredits($params['email'], $params['productQty'][$params['productId'][0]]);
        $response['afy_book_credits'] = $credits;

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }

    public function addonMultiple(array $params): ServiceResult
    {
        $totalProductQty = 0;
        $products = [];
        foreach ($params['offers'] as $offer) {
            $totalProductQty += $offer['productQty'];

            $products[] = new Product(
                $offer['productId'],
                $offer['offerId'],
                $offer['billingModelId'],
                $offer['productQty']
            );
        }

        $order = new Order(
            $params['shippingId'],
            null,
            null,
            $params['campaignId'],
            null,
            null,
            $products
        );

        $order->setPreviousOrderId($params['previousOrderId']);

        $response = $this->ordersManager->addon($order);

        $credits = $this->addCredits($params['email'], $totalProductQty);
        $response['afy_book_credits'] = $credits;

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }

    public function calculateTotal(array $params): ServiceResult
    {
        $response = $this->ordersManager->calculateTotal($params, 1);

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }

    public function find(array $params): ServiceResult
    {
        $response = $this->ordersManager->findRecurring($params['email']);
        if ($response['response_code'] > 100) {
            throw new ServerException($this->translator->trans('customer.not.found', [], 'messages'));
        }
        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }

    /**
     * @throws ServiceException
     */
    public function update(array $params): ServiceResult
    {
        $this->validateHubSpotContact($params['email']);

        $findResponse = $this->ordersManager->find($params['email']);
        if ($findResponse['response_code'] > 100) {
            //
            // No orders to update.
            //
            throw new InternalErrorException('no.orders.found');
        }

        list('orderId' => $orderId, 'order' => $order, 'isStartOrder' => $isStartOrder, 'productIds' => $productIds) = $this->parseFindOrderResponse($findResponse);

        $creditCard = new CreditCard(null, $params['creditCardNumber'], $params['expirationDate']);

        $this->updateAuthorize($params, $creditCard);

        $updateResponse = $this->ordersManager->update(
            $orderId,
            [
                'first_name' => $params['firstName'],
                'last_name' => $params['lastName'],
                'phone' => $params['phone'],
            ],
            [
                'billing_first_name' => $params['firstName'],
                'billing_last_name' => $params['lastName'],
                'billing_address1' => $params['billingAddress1'],
                'billing_address2' => $params['billingAddress2'],
                'billing_city' => $params['billingCity'],
                'billing_state' => $params['billingState'],
                'billing_zip' => $params['billingZip'],
                'billing_country' => $params['billingCountry'],
            ],
            [
                'cc_number' => $params['creditCardNumber'],
                'cc_expiration_date' => $params['expirationDate']->format('my'),
                'cc_payment_type' => $creditCard->getType(),
            ]
        );

        if ($isStartOrder) {
            foreach ($productIds as $productid) {
                $updateResponse = $this->ordersManager->updateSubscription($orderId, $productid);
            }
        }

        $creditCardLastUpdatedTimestamp = $params['utcDateTime']->setTime(0, 0, 0)->format('Uv');
        $this->contactsManager->updateContactByEmail($params['email'], ['ll_credit_card_last_updated' => $creditCardLastUpdatedTimestamp]);

        $status = $this->validateUpdateResponse($updateResponse);

        $message = ServiceResult::RESULT_FAILURE == $status ?
            $this->translator->trans('card.update.error', [], 'messages') :
            $this->translator->trans('card.update.success', [], 'messages');

        $serviceResult = (new ServiceResult($status, $message))
            ->setData($updateResponse);

        return $serviceResult;
    }

    public function subscribe(array $params): ServiceResult
    {
        $result = $this->newOrder($params);
        if ($result->getStatus() === ServiceResult::RESULT_SUCCESS) {
            $this->setupRegistrationWorkflow($params);
        }
        return $result;
    }

    private function setupRegistrationWorkflow(array $params)
    {
        if (!isset($params['flow'])) {
            return;
        }
        if ($params['flow'] === Contact::LOGIN_FLOW_AUTOLOGIN) {
            $item = BusItemFactory::createBusItemForAutoLogin($params['email'], $params['password']);
        } else {
            $item = BusItemFactory::createBusItemForBasic($params['email']);
        }

        $this->busService->queueItem($item);
    }

    public function validateUpdateResponse(array $updateResponse, array $validResponseCodes = [100, 343]): string
    {
        $status = ServiceResult::RESULT_SUCCESS;
        if ($updateResponse['response_code'] > 100) {
            //
            // order_update returns codes for each fields. We make sure they are either 100(success) or 343(no change).
            //
            foreach ($updateResponse['order_id'] as $orderId => $fields) {
                foreach ($fields as $key => $value) {
                    $responseCodes = explode(',', $value['response_code']);
                    foreach ($responseCodes as $responseCode) {
                        if (!in_array($responseCode, $validResponseCodes)) {
                            $status = ServiceResult::RESULT_FAILURE;
                            break 3;
                        }
                    }
                }
            }
        }

        return $status;
    }

    protected function buildCalculateOrderTotalParam(array $order): array
    {
        $product = $order['products'][0];

        return [
            'campaignId' => $order['campaign_id'],
            'shippingId' => !empty($order['shipping_id']) ? $order['shipping_id'] : 3,
            'offers' => [
                [
                    'id' => $product['offer']['id'],
                    'productId' => $product['product_id'],
                    'billingModelId' => $product['billing_model']['id'],
                    'productQty' => $product['product_qty'],
                ],
            ],
            'location' => [
                'state' => $order['billing_state'],
                'country' => $order['billing_country'],
                'zip' => $order['billing_postcode'],
            ]
        ];
    }

    protected function validateHubSpotContact(string $email): void
    {
        try {
            $internalError = false;
            $this->contactsManager->getContactByEmail($email);
        } catch (ServerException $se) {
            if ($se->getCode() == 404) {
                throw new ServerException($this->translator->trans('customer.not.found', [], 'messages'));
            }

            $internalError = true;
        } catch (\Exception $e) {
            $internalError = true;
        } finally {
            if ($internalError) {
                throw new InternalErrorException('get.contact.failure');
            }
        }
    }

    protected function parseFindOrderResponse(array $response): array
    {
        if (!is_array($response['order_id'])) {
            $response['order_id'] = [$response['order_id']];
        }

        $orderId = max($response['order_id']);
        $order = $response['data'][$orderId];
        $ancestorOrder = $response['data'][$order['ancestor_id']];

        $productIds = [];

        //
        // If order is declined, we may need to "start" a new order.
        //
        $isStartOrder = $order['order_status'] === Order::STATUS_DECLINED && $ancestorOrder['on_hold'] === '1';
        if ($isStartOrder) {
            $orderId = $ancestorOrder['order_id'];
            $order = $ancestorOrder;

            foreach ($order['products'] as $product) {
                if ($product['on_hold'] === '1') {
                    $productIds[] = $product['product_id'];
                }
            }
        }

        return ['orderId' => $orderId, 'order' => $order, 'isStartOrder' => $isStartOrder, 'productIds' => $productIds];
    }

    protected function updateAuthorize(array $params, CreditCard $creditCard): void
    {
        $authorizeParams = [
            'billingFirstName' => $params['firstName'],
            'billingLastName' => $params['lastName'],
            'billingAddress1' => $params['billingAddress1'],
            'billingCity' => $params['billingCity'],
            'billingState' => $params['billingState'],
            'billingZip' => $params['billingZip'],
            'billingCountry' => $params['billingCountry'],
            'phone' => $params['phone'],
            'email' => $params['email'],
            'creditCardType' => $creditCard->getType(),
            'creditCardNumber' => $params['creditCardNumber'],
            'expirationDate' => $params['expirationDate']->format('my'),
            'CVV' => $params['cvv'],
            'ipAddress' => $params['ipAddress'],
            'productId' => $this->limeLightAuthorizationProductId,
            'campaignId' => $this->limeLightAuthorizationCampaignId,
        ];
        if ($params['billingAddress2']) {
            $authorizeParams['billingAddress2'] = $params['billingAddress2'];
        }
        $this->paymentsManager->authorize($authorizeParams);
    }

    protected function addCredits(string $email, int $credits): int
    {
        ['result' => $getContactResult, 'statusCode' => $getContactStatusCode] = $this->contactsManager->getContactByEmail($email);
        $currentBookCredits = (int)($getContactResult['properties']['afy_book_credits']['value'] ??  0);

        // Update credits
        $newBookCredits = $currentBookCredits + $credits;
        ['result' => $updateContactResult, 'statusCode' => $updateContactStatusCode] = $this->contactsManager->updateContactByEmail($email, ['afy_book_credits' => $newBookCredits]);

        return $newBookCredits;
    }

    protected function newOrder(array $params): ServiceResult
    {
        $creditCards = [
            new CreditCard(
                null, // Type will be determined from the credit card number.
                $params['creditCardNumber'],
                $params['expirationDate'],
                $params['cvv']
            )
        ];

        $addresses = [
            new Address(
                $params['firstName'],
                $params['lastName'],
                $params['billingAddress1'],
                $params['billingAddress2'],
                $params['billingCity'],
                $params['billingState'],
                $params['billingZip'],
                $params['billingCountry'],
                Address::TYPE_BILLING
            ),
            new Address(
                $params['firstName'],
                $params['lastName'],
                $params['billingAddress1'],
                $params['billingAddress2'],
                $params['billingCity'],
                $params['billingState'],
                $params['billingZip'],
                $params['billingCountry'],
                Address::TYPE_SHIPPING
            )
        ];

        $customer = new Customer(
            $params['email'],
            $params['firstName'],
            $params['lastName'],
            $params['phone'],
            $creditCards,
            $addresses
        );

        $products = [];
        foreach ($params['productId'] as $productId) {
            $products[] = new Product(
                $productId,
                $params['offerId'],
                $params['billingModelId'],
                $params['productQty'][$productId],
                $params['trialProductId'][$productId] ?? null
            );
        }

        $order = new Order(
            $params['shippingId'],
            null, // Default to 'Sale'.
            $params['ipAddress'],
            $params['campaignId'],
            true,
            $customer,
            $products
        );

        if ($params['affiliateId']) {
            $order->setAffiliateId($params['affiliateId']);
        }

        foreach (['source', 'campaign', 'medium', 'term'] as $key) {
            $utmName = 'utm_' . $key;
            $utmValue = $params[$utmName] ?? null;
            if ($utmValue) {
                $order->addUtm($utmName, $params[$utmName]);
            }
        }

        $productBump = null;
        if ($params['productBumpId'] ?? null &&
            $params['productBumpOfferId'] ?? null &&
            $params['productBumpBillingModelId'] ?? null &&
            $params['productBumpQty'] ?? null) {
            $productBump = new Product(
                $params['productBumpId'],
                $params['productBumpOfferId'],
                $params['productBumpBillingModelId'],
                $params['productBumpQty']
            );

            $order->setProductBump($productBump);
        }

        $response = $this->ordersManager->create($order);

        $properties = [
            'firstname' => $customer->getFirstName(),
            'lastname' => $customer->getLastName(),
            'phone' => $customer->getPhone(),
        ];

        try {
            ['result' => $getContactResult, 'statusCode' => $getContactStatusCode] = $this->contactsManager->getContactByEmail($customer->getEmail());

            $contact = new Contact($getContactResult['properties']);
            if ($contact->getFirstName()) {
                unset($properties['firstname']);
            }

            if ($contact->getLastName()) {
                unset($properties['lastname']);
            }

            if ($contact->getPhone()) {
                unset($properties['phone']);
            }

            if ($properties) {
                $this->contactsManager->updateContactByEmail($customer->getEmail(), $properties);
            }
        } catch (ServerException $serverException) {
            if ($serverException->getCode() != 404) {
                throw $serverException;
            }

            $properties['afy_book_credits'] = 0;
            $this->contactsManager->createContact(
                $customer->getEmail(),
                $properties
            );
        }

        if ($productBump) {
            $credits = $this->addCredits($customer->getEmail(), $productBump->getQuantity());
            $response['afy_book_credits'] = $credits;
        }

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }
}
