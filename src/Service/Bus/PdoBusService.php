<?php
namespace Authorify\Service\Bus;

use Authorify\Model\Bus\BusAbstract;
use Authorify\Model\Bus\BusItemInterface;
use Authorify\Model\Bus\BusItemFactory;
use DateTimeInterface;

final class PdoBusService extends BusAbstract
{
    private \PDO $conn;

    public function __construct(\PDO $pdo)
    {
        $this->conn = $pdo;
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->conn->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $this->schema($pdo);
    }

    private function schema(\PDO $pdo)
    {
        $sql = <<< HEREDOC
drop table if exists bus_autologin;
HEREDOC;
        $pdo->exec($sql);

        $sql = <<< HEREDOC
create table if not exists registration_bus (
    id uuid primary key,
    email text,
    flow text,
    status text,
    data jsonb,
    created_at timestamp
);
HEREDOC;
        $pdo->exec($sql);

        $sql = <<< HEREDOC
create index if not exists email_idx on registration_bus (email);
HEREDOC;
        $pdo->exec($sql);
    }

    public function queueItem(BusItemInterface $item)
    {
        $item->setStatus(BusItemInterface::CREATED);
        $sql = <<< HEREDOC
insert into registration_bus
    (id, email, data, status, flow, created_at)
values
    (:id, :email, :data, :status, :flow, :created_at)
HEREDOC;
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':id', $item->getId());
        $stmt->bindValue(':email', $item->getEmail());
        $stmt->bindValue(':data', json_encode($item->getData()));
        $stmt->bindValue(':status', $item->getStatus());
        $stmt->bindValue(':flow', $item->getFlow());
        $stmt->bindValue(':created_at', $item->getCreatedAt()->format(DateTimeInterface::ATOM));
        $stmt->execute();
    }

    public function readItem(string $email, string $flow): ?BusItemInterface
    {
        // @TODO: This ended up being wrong, it should be read by primary key or orderId, and needs to be corrected on the next iteration
        $sql = <<< HEREDOC
select id from registration_bus where email = :email and flow = :flow order by created_at desc limit 1
HEREDOC;
        $pdo = $this->conn;
        $pdo->beginTransaction();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':flow', $flow);
        $stmt->execute();
        $key = $stmt->fetch(\PDO::FETCH_COLUMN);
        $item = $this->getValue($key);
        $pdo->commit();
        return $item;
    }

    public function exec(BusItemInterface $busItem)
    {
        $this->setValue($busItem, BusItemInterface::PROCESSING);
    }

    public function getItemForProcessing(string $id): ?BusItemInterface
    {
        return $this->getValue($id, BusItemInterface::PROCESSING);
    }

    public function delete(string $id)
    {
        $sql = <<< HEREDOC
delete from registration_bus where id = :id
HEREDOC;
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':email', $id);
        $stmt->execute();
    }

    public function finish(string $id)
    {
        $sql = <<< HEREDOC
        update registration_bus set status = :status where id = :id
HEREDOC;
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':status', BusItemInterface::READY);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }

    /**
     * @inheritDoc
     */
    public function getQueue(): array
    {
        $sql = <<< HEREDOC
        update registration_bus set status = :statusProcessing where status = :statusCreated returning id
HEREDOC;
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':statusProcessing', BusItemInterface::PROCESSING);
        $stmt->bindValue(':statusCreated', BusItemInterface::CREATED);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function markForRetrial(string $id)
    {
        $sql = <<< HEREDOC
        update registration_bus set status = :status where email = :email
HEREDOC;
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':status', BusItemInterface::CREATED);
        $stmt->bindValue(':email', $id);
        $stmt->execute();
    }

    private function setValue(BusItemInterface $busItem, string $newStatus)
    {
        $sql = <<< HEREDOC
        update registration_bus set status = :status where email = :email
HEREDOC;
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':status', $newStatus);
        $stmt->bindValue(':email', $busItem->getEmail());
        $stmt->execute();
    }

    private function getValue(string $id, $status = false): ?BusItemInterface
    {
        if ($status) {
            $sql = <<< HEREDOC
select * from registration_bus where id = :id and status = :status
HEREDOC;
        } else {
            $sql = <<< HEREDOC
select * from registration_bus where id = :id
HEREDOC;
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':id', $id);
        if ($status) {
            $stmt->bindValue(':status', $status);
        }
        $stmt->execute();
        $item = $stmt->fetch();
        if ($item) {
            return BusItemFactory::fromArray($item);
        }
        return null;
    }

    public function markExpiredAsFailed($maxAge = '5 minutes')
    {
        $stmt = $this->conn->prepare("update registration_bus set status = :failed where status != :ready and (created_at + interval '{$maxAge}') < now()");
        $stmt->bindValue(':failed', BusItemInterface::FAILED);
        $stmt->bindValue(':ready', BusItemInterface::READY);
        $stmt->execute();
    }

    public function garbageCollect($maxAge = '2 hours')
    {
        $stmt = $this->conn->prepare("delete from registration_bus where status = :ready and (created_at + interval '{$maxAge}') < now()");
        $stmt->bindValue(':ready', BusItemInterface::READY);
        $stmt->execute();
    }

    public function beginTransaction()
    {
        return $this->conn->beginTransaction();
    }

    public function rollback()
    {
        return $this->conn->rollBack();
    }

    public function commit()
    {
        return $this->conn->commit();
    }
}
