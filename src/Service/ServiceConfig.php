<?php

namespace Authorify\Service;

use Authorify\Model\ServiceResult;
use Authorify\Parameter\Validator\AbstractParameterValidator;
use Authorify\Parameter\Validator\Helper\ListStatesParameterValidator;
use Authorify\Parameter\Validator\LimeLight\AddonOrderParameterValidator;
use Authorify\Parameter\Validator\LimeLight\AddonMultipleOrderParameterValidator;
use Authorify\Parameter\Validator\LimeLight\CalculateOrderTotalParameterValidator;
use Authorify\Parameter\Validator\LimeLight\CreateOrderParameterValidator;
use Authorify\Parameter\Validator\LimeLight\CreateOrderWithRegistrationParameterValidator;
use Authorify\Parameter\Validator\LimeLight\FindOrderParameterValidator;
use Authorify\Parameter\Validator\LimeLight\GetProductByIdParameterValidator;
use Authorify\Parameter\Validator\LimeLight\UpdateOrderParameterValidator;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Authorify\Service\Helper\StatesService as HelperStatesService;
use Authorify\Service\LimeLight\Service\OrdersService as LimeLightOrdersService;
use Authorify\Service\LimeLight\Service\ProductsService as LimeLightProductsService;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ServiceConfig
{
    public const HELPER_STATES_LIST = 'helper_states_list';

    public const LIMELIGHT_ORDERS_CREATE = 'll_orders_create';
    public const LIMELIGHT_ORDERS_ADDON = 'll_orders_addon';
    public const LIMELIGHT_ORDERS_ADDON_MULTIPLE = 'll_orders_addon_multiple';
    public const LIMELIGHT_ORDERS_CALCULATE_TOTAL = 'll_orders_calculate_total';
    public const LIMELIGHT_ORDERS_FIND = 'll_orders_find';
    public const LIMELIGHT_ORDERS_UPDATE = 'll_orders_update';
    public const LIMELIGHT_PRODUCTS_GET_BY_ID = 'll_products_get_by_id';
    public const LIMELIGHT_SUBSCRIPTION = 'll_subscription';

    public const CREDITS_TOP_UP = 'credits_top_up';

    protected $routes = [
        self::HELPER_STATES_LIST => ['/helper/states/{countryCode}', [], [], [], '', [], ['GET'], ''],

        self::LIMELIGHT_ORDERS_CREATE => ['/limelight/orders', [], [], [], '', [], ['POST'], ''],
        self::LIMELIGHT_ORDERS_ADDON => ['/limelight/orders/addon/{previousOrderId}', [], [], [], '', [], ['POST'], ''],
        self::LIMELIGHT_ORDERS_ADDON_MULTIPLE => ['/limelight/orders/addon-multiple/{previousOrderId}', [], [], [], '', [], ['POST'], ''],
        self::LIMELIGHT_ORDERS_CALCULATE_TOTAL => ['/limelight/orders/total/calculate', [], [], [], '', [], ['GET'], ''],
        self::LIMELIGHT_ORDERS_FIND => ['/limelight/orders/find', [], [], [], '', [], ['GET'], ''],
        self::LIMELIGHT_ORDERS_UPDATE => ['/limelight/orders/update', [], [], [], '', [], ['POST'], ''],
        self::LIMELIGHT_PRODUCTS_GET_BY_ID => ['/limelight/products/{id}', [], [], [], '', [], ['GET'], ''],
        self::LIMELIGHT_SUBSCRIPTION => ['/limelight/subscription', [], [], [], '', [], ['POST'], ''],

        self::CREDITS_TOP_UP => ['/credits/{email}/topup', [], [], [], '', [], ['POST'], ''],
    ];

    protected $parameterValidators = [
        self::HELPER_STATES_LIST => ListStatesParameterValidator::class,

        self::LIMELIGHT_ORDERS_CREATE => CreateOrderParameterValidator::class,
        self::LIMELIGHT_ORDERS_ADDON => AddonOrderParameterValidator::class,
        self::LIMELIGHT_ORDERS_ADDON_MULTIPLE => AddonMultipleOrderParameterValidator::class,
        self::LIMELIGHT_ORDERS_CALCULATE_TOTAL => CalculateOrderTotalParameterValidator::class,
        self::LIMELIGHT_ORDERS_FIND => FindOrderParameterValidator::class,
        self::LIMELIGHT_ORDERS_UPDATE => UpdateOrderParameterValidator::class,
        self::LIMELIGHT_PRODUCTS_GET_BY_ID => GetProductByIdParameterValidator::class,
        self::LIMELIGHT_SUBSCRIPTION => CreateOrderWithRegistrationParameterValidator::class,

        self::CREDITS_TOP_UP => CreateOrderParameterValidator::class,
    ];

    protected $serviceCallInfo = [
        self::HELPER_STATES_LIST => ['helperStatesService', 'list'],

        self::LIMELIGHT_ORDERS_CREATE => ['limeLightOrdersService', 'create'],
        self::LIMELIGHT_ORDERS_ADDON => ['limeLightOrdersService', 'addon'],
        self::LIMELIGHT_ORDERS_ADDON_MULTIPLE => ['limeLightOrdersService', 'addonMultiple'],
        self::LIMELIGHT_ORDERS_CALCULATE_TOTAL => ['limeLightOrdersService', 'calculateTotal'],
        self::LIMELIGHT_ORDERS_FIND => ['limeLightOrdersService', 'find'],
        self::LIMELIGHT_ORDERS_UPDATE => ['limeLightOrdersService', 'update'],
        self::LIMELIGHT_PRODUCTS_GET_BY_ID => ['limeLightProductsService', 'getById'],
        self::LIMELIGHT_SUBSCRIPTION => ['limeLightOrdersService', 'subscribe'],

        self::CREDITS_TOP_UP => ['creditsService', 'topUp'],
    ];

    /** @var ValidatorInterface */
    protected $validator;

    /** @var RouteCollection */
    protected $routeCollection;

    /** @var HelperStatesService */
    protected $helperStatesService;

    /** @var LimeLightOrdersService */
    protected $limeLightOrdersService;

    /** @var LimeLightProductsService */
    protected $limeLightProductsService;

    /** @var CreditsService */
    protected $creditsService;

    public function __construct(
        ValidatorInterface $validator,

        HelperStatesService $helperStatesService,
        LimeLightOrdersService $limeLightOrdersService,
        LimeLightProductsService $limeLightProductsService,

        CreditsService $creditsService
    ) {
        $this->routeCollection = new RouteCollection();
        foreach ($this->routes as $name => $config) {
            $this->routeCollection->add($name, new Route(...$config));
        }

        $this->validator = $validator;

        $this->helperStatesService = $helperStatesService;
        $this->limeLightOrdersService = $limeLightOrdersService;
        $this->limeLightProductsService = $limeLightProductsService;
        $this->creditsService = $creditsService;
    }

    public function getRouteCollection(): RouteCollection
    {
        return $this->routeCollection;
    }

    public function getParameterValidator(string $routeName): AbstractParameterValidator
    {
        $className = $this->parameterValidators[$routeName];

        return new $className($this->validator);
    }

    public function callService(string $routeName, array $params): ServiceResult
    {
        list ($service, $method) = $this->serviceCallInfo[$routeName];

        return $this->$service->$method($params);
    }
}
