<?php

namespace Authorify\Service\HubSpot;

use Authorify\Provider\HubSpot\Client;
use Authorify\Provider\HubSpot\Validator\FormValidator;

class FormsService
{
    /** @var string */
    protected $portalId;

    /** @var array */
    protected $forms;

    /** @var Client */
    protected $client;

    /** @var FormValidator */
    protected $validator;

    public function __construct(
        ConfigService $configService,
        Client $client,
        FormValidator $validator
    ) {
        $this->portalId = $configService->getPortalId();
        $this->forms = $configService->getForms();
        $this->client = $client;
        $this->validator = $validator;
    }

    public function v1($data, $hubSpotUtk, $ipAddress)
    {
        $result = ['status' => 'success', 'message' => '', 'data' => null];

        $formParams = $this->extractFormParams($data);

        if (isset($this->forms[$formParams['id']])) {
            $formConfig = $this->forms[$formParams['id']];

            $validationResult = $this->validator->validate($formConfig['fields'], $data);
            if (!$validationResult['missing'] && !$validationResult['invalid']) {
                list ($endpoint, $payload, $headers) = $this->buildRequestParamsV1(
                    $validationResult['valid'],
                    $formConfig,
                    $formParams,
                    $hubSpotUtk,
                    $ipAddress
                );

                $response = $this->client->request(
                    $endpoint,
                    $payload,
                    $headers
                );

                if (isset($formConfig['redirectUrl']) && !is_null($formConfig['redirectUrl'])) {
                    $response['redirectUrl'] = $formConfig['redirectUrl'];

                    if (!empty($formConfig['successMessage'])) {
                        $response['redirectUrl'] .= '?' . http_build_query(['msg' => $formConfig['successMessage']]);
                    }
                }

                $result['message'] = $formConfig['successMessage'];
                $result['data'] = $response;
            } else {
                $result['status'] = 'fail';
                $result['message'] = 'validation failed.';
                $result['data'] = $validationResult;
            }
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'unknown form.';
        }

        return $result;
    }

    protected function buildRequestParamsV1(array $values, array $formConfig, array $formParams, $hubSpotUtk, $ipAddress)
    {
        $context = [
            'hutk' => $hubSpotUtk,
            'ipAddress' => $ipAddress,
            'pageUrl' => $formParams['pageUrl'],
            'pageName' => $formParams['pageName'],
        ];
        $values['hs_context'] = json_encode($context);

        $endpoint = sprintf('https://forms.hubspot.com/uploads/form/v2/%s/%s', $this->portalId, $formConfig['formGuid']);
        $payload = http_build_query($values);
        $headers = ['Content-Type: application/x-www-form-urlencoded'];

        return [$endpoint, $payload, $headers];
    }

    protected function extractFormParams(array $data)
    {
        $params = ['id' => null, 'pageUrl' => null, 'pageName' => null];

        foreach ($params as $key => &$value) {
            $dataKey = '_' . $key;
            if (isset($data[$dataKey])) {
                $value = $data[$dataKey];

                unset($data[$dataKey]);
            }
        }

        return $params;
    }
}