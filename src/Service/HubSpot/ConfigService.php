<?php

namespace Authorify\Service\HubSpot;

use Symfony\Component\HttpKernel\KernelInterface;

class ConfigService
{
    protected $config;

    public function __construct(KernelInterface $kernel)
    {
        $env = $kernel->getEnvironment() == 'prod' ? 'prod' : 'dev';
        $projectDir = $kernel->getRootDir() . '/..';

        $this->config = require sprintf('%s/hubspot_%s.php', $projectDir, $env);
    }

    public function getPortalId() : int
    {
        return $this->config['portalId'];
    }

    public function getForms() : array
    {
        return $this->config['forms'];
    }
}