<?php

namespace Authorify\Service\HubSpot;


use Authorify\Model\ServiceResult;
use Authorify\Provider\HubSpot\Manager\ContactsManager;

class ContactsService
{
    /** @var ContactsManager */
    protected $contactsManager;

    public function __construct(ContactsManager $contactsManager)
    {
        $this->contactsManager = $contactsManager;
    }

    public function getContactByEmail(string $email): ServiceResult
    {
        $response = $this->contactsManager->getContactByEmail($email);

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }

    public function updateContactByEmail(string $email, array $properties): ServiceResult
    {
        $response = $this->contactsManager->updateContactByEmail($email, $properties);

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData($response);

        return $serviceResult;
    }
}