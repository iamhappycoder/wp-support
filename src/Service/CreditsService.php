<?php

namespace Authorify\Service;

use Authorify\Model\ServiceResult;
use Authorify\Provider\HubSpot\Manager\ContactsManager;
use Authorify\Provider\LimeLight\Model\Address;
use Authorify\Provider\LimeLight\Model\CreditCard;
use Authorify\Provider\LimeLight\Model\Customer;
use Authorify\Provider\LimeLight\Model\Order;
use Authorify\Provider\LimeLight\Model\Product;
use Authorify\Service\LimeLight\Service\OrdersService;

class CreditsService
{
    protected $ordersService;
    protected $contactsManager;

    public function __construct(
        OrdersService $ordersService,
        ContactsManager $contactsManager
    ) {
        $this->ordersService = $ordersService;
        $this->contactsManager = $contactsManager;
    }

    public function topUp(array $params): ServiceResult
    {
        // Get existing credits, if any
        ['result' => $getContactResult, 'statusCode' => $getContactStatusCode] = $this->contactsManager->getContactByEmail($params['email']);
        $currentBookCredits = isset($getContactResult['properties']['afy_book_credits']) ? (int)$getContactResult['properties']['afy_book_credits']['value'] : 0;

        // Pay for credits
        $createOrderResult = $this->ordersService->create($params);

        // Update credits
        $newBookCredits = $currentBookCredits + $params['productQty'][$params['productId'][0]];
        ['result' => $updateContactResult, 'statusCode' => $updateContactStatusCode] = $this->contactsManager->updateContactByEmail($params['email'], ['afy_book_credits' => $newBookCredits]);

        $serviceResult = (new ServiceResult(ServiceResult::RESULT_SUCCESS))
            ->setData(['newBookCredits' => $newBookCredits]);

        return $serviceResult;
    }
}