<?php

namespace Authorify\Exception\Validation;

use Throwable;

class ParametersValidationException extends \Exception
{
    protected $data;

    public function __construct(array $data, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
