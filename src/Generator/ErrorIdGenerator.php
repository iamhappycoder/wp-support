<?php

namespace Authorify\Generator;


class ErrorIdGenerator implements GeneratorInterface
{
    public function generate(\DateTimeImmutable $utcDateTime): string
    {
        return $utcDateTime->format('YmdHis') . '-' . mt_rand(9999999999, 9999999999);
    }
}
