<?php

namespace Authorify\Generator;


interface GeneratorInterface
{
    public function generate(\DateTimeImmutable $utcDateTime): string;
}
