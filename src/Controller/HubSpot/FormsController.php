<?php

namespace Authorify\Controller\HubSpot;

use Authorify\Service\HubSpot\FormsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FormsController
{
    /** @var FormsService */
    protected $formsService;

    public function __construct(
        FormsService $formsService
    ) {
        $this->formsService = $formsService;
    }

    public function v1(Request $request) : JsonResponse
    {
        $result = $this->formsService->v1(
            $request->request->all(),
            $request->cookies->all(),
            $request->getClientIp()
        );

        return new JsonResponse($result);
    }
}