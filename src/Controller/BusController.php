<?php

namespace Authorify\Controller;

use Authorify\Model\Bus\BusInterface;
use Authorify\Model\Bus\BusItemInterface;
use Authorify\Provider\HubSpot\Model\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BusController extends AbstractController
{
    public function finalize($email, BusInterface $busService, ValidatorInterface $validator, $urlBookEditorAuthToken)
    {
        $constraints = [
            new Email(),
            new NotBlank()
        ];

        $error = [
            'status' => BusItemInterface::NOTFOUND,
            'data' => [],
        ];

        $errors = $validator->validate($email, $constraints);

        if (count($errors)) {
            return $this->json($error);
        }

        $item = $busService->readItem($email, Contact::LOGIN_FLOW_AUTOLOGIN);

        if (!$item) {
            return $this->json($error);
        }

        $status = $item->getStatus();

        $data = $item->getData();
        if ($status === BusItemInterface::READY) {
            $data = [
                'email' => $item->getEmail(),
                'token' => $item['token'],
                'link' => $item->getAutologinLink($urlBookEditorAuthToken),
            ];
        } else {
            $data = [];
        }

        $result = [
            'status' => $status,
            'data' => $data,
        ];

        return $this->json($result);
    }
}
