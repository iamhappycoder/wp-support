<?php

namespace Authorify\Controller;

use Authorify\Exception\ClientException;
use Authorify\Exception\InternalErrorException;
use Authorify\Exception\ServerException;
use Authorify\Exception\ServiceException;
use Authorify\Exception\Validation\InvalidParametersException;
use Authorify\Exception\Validation\MissingParametersException;
use Authorify\Exception\Validation\UnrecognizedParametersException;
use Authorify\Generator\ErrorIdGenerator;
use Authorify\Model\ServiceResult;
use Authorify\Service\ServiceConfig;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiController
{
    /** @var TranslatorInterface */
    protected $translator;

    /** @var ServiceConfig */
    protected $serviceConfig;

    /** @var LoggerInterface */
    protected $logger;

    /** @var ErrorIdGenerator */
    protected $errorIdGenerator;

    /** @var \DateTimeImmutable */
    protected $utcDateTime;

    public function __construct(
        TranslatorInterface $translator,
        LoggerInterface $logger,
        ServiceConfig $serviceConfig,
        ErrorIdGenerator $errorIdGenerator
    ) {
        $this->translator = $translator;
        $this->logger = $logger;
        $this->serviceConfig = $serviceConfig;
        $this->errorIdGenerator = $errorIdGenerator;
    }

    public function handleRequestV1(Request $request): JsonResponse
    {
        try {
            //
            // Validate route.
            //
            $attributes = $this->getUrlMatcher($request)->match('/' . $request->get('uri'));
            $routeName = $attributes['_route'];
            unset($attributes['_route']);

            //
            // Get parameters.
            //
            $parameters = array_merge($this->getParameters($request), $attributes);

            $this->utcDateTime = new \DateTimeImmutable($parameters['utcDateTime'] ?? 'now', new \DateTimeZone('UTC'));
            unset($parameters['utcDateTime']);

            //
            // Validate parameters.
            //
            // TODO: replace with factory pattern.
            $this->serviceConfig->getParameterValidator($routeName)->validate($parameters);

            $parameters['ipAddress'] = $request->getClientIp();
            $parameters['utcDateTime'] = $this->utcDateTime;

            $result = $this->serviceConfig->callService($routeName, $parameters);
        } catch (ResourceNotFoundException $resourceNotFoundException) {
            $result = new ServiceResult(ServiceResult::RESULT_FAILURE, 'Invalid endpoint.');
        } catch (MethodNotAllowedException $methodNotAllowedException) {
            $result = new ServiceResult(ServiceResult::RESULT_FAILURE, 'HTTP method not allowed.');
        } catch (MissingParametersException | InvalidParametersException $parametersException) {
            $data = $parametersException->getData();
            $errorFormat = $parameters['errorFormat'] ?? false;
            if ($errorFormat === 'multi') {
                $messages = [];
                foreach ($data as $item) {
                    $messages[$item] = $this->translator->trans(
                        $item,
                        [],
                        'validators'
                    );
                }
                $errorMessage = json_encode($messages);
            } else {
                $errorMessage = $this->translator->trans(
                    $data[0],
                    [],
                    'validators'
                );
            }

            $result = new ServiceResult(ServiceResult::RESULT_FAILURE, $errorMessage);
        } catch (UnrecognizedParametersException $unrecognizedParametersException) {
            $errorMessage = $this->translator->trans(
                'unrecognized.parameter',
                ['%1' => $unrecognizedParametersException->getData()[0]],
                'validators'
            );
            $result = new ServiceResult(ServiceResult::RESULT_FAILURE, $errorMessage);
        } catch (ClientException $clientException) {
            $result = new ServiceResult(
                ServiceResult::RESULT_FAILURE,
                'Client error, reason: ' . $clientException->getMessage(),
                $clientException->getCode()
            );
        } catch (ServerException $serverException) {
            $result = new ServiceResult(
                ServiceResult::RESULT_FAILURE,
                $serverException->getMessage(),
                $serverException->getCode()
            );

            if ($serverException instanceof \Authorify\Provider\LimeLight\Exception\ServerException &&
                $declineReason = $serverException->getDeclineReason()) {
                $result->setData(['declineReason' => $declineReason]);
            }
        } catch (ServiceException $serviceException) {
            $result = new ServiceResult(
                ServiceResult::RESULT_FAILURE,
                $serviceException->getMessage()
            );
        } catch (InternalErrorException | \Exception $e) {
            $errorId = $this->errorIdGenerator->generate($this->utcDateTime);
            $this->logger->critical('[' . $errorId . '] ' . $this->translator->trans($e->getMessage(), [], 'messages') . "\n" . $e->getTraceAsString());

            $result = new ServiceResult(
                ServiceResult::RESULT_FAILURE,
                $this->translator->trans('internal.error', ['%1%' => $errorId], 'messages')
            );
        }

        return new JsonResponse($result->toArray());
    }

    protected function getUrlMatcher(Request $request): UrlMatcher
    {
        $context = new RequestContext();
        $context->fromRequest($request);

        return new UrlMatcher($this->serviceConfig->getRouteCollection(), $context);
    }

    protected function getParameters(Request $request): array
    {
        $parameterBag = $request->getMethod() == Request::METHOD_GET ? 'query' : 'request';

        $parameters = $request->$parameterBag->all();
        foreach ($parameters as $key => &$value) {
            if (is_string($value)) {
                $value = urldecode($value);
            }
        }

        return $parameters;
    }
}
