<?php

namespace Authorify\Command;

use Authorify\Model\Bus\BusLifecycleInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

final class ProcessRegistrationsCommand extends Command
{
    protected static $defaultName = 'authorify:registration:process-registrations';

    private $bus;
    private $container;
    private $log;

    public function __construct(BusLifecycleInterface $busService, ContainerInterface $container, LoggerInterface $log)
    {
        parent::__construct();
        $this->bus = $busService;
        $this->container = $container;
        $this->log = $log;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $phpBinaryFinder = new PhpExecutableFinder();
        $phpBinaryPath = $phpBinaryFinder->find();
        $command = $this->container->getParameter('kernel.project_dir') . '/bin/console';
        while (true) {
            $this->bus->markExpiredAsFailed();
            $this->bus->garbageCollect();
            $queue = $this->bus->getQueue();
            foreach ($queue as $key) {
                $arguments = [
                    'authorify:registration:registration-listener',
                    $key,
                ];
                $this->log->debug("Registration listener spawned", [$key, __CLASS__, __LINE__]);
                exec(implode(' ', ['nohup', $phpBinaryPath, $command, ...$arguments, '> /dev/null 2>/dev/null &']));
            }
            sleep(1);
        }
    }
}
