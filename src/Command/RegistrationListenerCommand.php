<?php

namespace Authorify\Command;

use Authorify\Model\Bus\BusAbstract;
use Authorify\Model\Bus\BusException;
use Authorify\Model\Bus\BusItemInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Authorify\Provider\HubSpot\Manager\ContactsManager;
use Authorify\Provider\HubSpot\Model\Contact;
use Psr\Log\LoggerInterface;

final class RegistrationListenerCommand extends Command
{
    protected static $defaultName = 'authorify:registration:registration-listener';

    private BusAbstract $bus;
    private ContactsManager $contactsManager;
    private LoggerInterface $log;
    private string $urlBookEditorAuthToken;
    private string $urlBookEditorForgotPassword;
    private string $emailTemplateIdRegistration;
    private string $emailTemplateIdForgotPassword;
    private string $apiKeyEmail;

    public function __construct(
        BusAbstract $busService,
        ContactsManager $contactsManager,
        LoggerInterface $log,
        $urlBookEditorAuthToken,
        $urlBookEditorForgotPassword,
        $emailTemplateIdRegistration,
        $emailTemplateIdForgotPassword,
        $hubSpotApiKeyEmail
    ) {
        parent::__construct();
        $this->bus = $busService;
        $this->contactsManager = $contactsManager;
        $this->log = $log;
        $this->urlBookEditorAuthToken = $urlBookEditorAuthToken;
        $this->urlBookEditorForgotPassword = $urlBookEditorForgotPassword;
        $this->emailTemplateIdRegistration = $emailTemplateIdRegistration;
        $this->emailTemplateIdForgotPassword = $emailTemplateIdForgotPassword;
        $this->apiKeyEmail = $hubSpotApiKeyEmail;
    }

    protected function configure()
    {
        $this->addArgument('key', InputArgument::REQUIRED, 'Bus key for the registration that should be monitored');
    }

    private function hasCredits($contact)
    {
        $credits = (int) $contact['properties']['afy_book_credits']['value'];

        return $credits > 0;
    }

    private function hasPackages($contact)
    {
        return (bool) $contact['properties']['afy_package']['value'];
    }

    private function isActive($contact)
    {
        $status = strtolower(trim($contact['properties']['afy_customer_status']['value']));

        return $status === 'active';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $key = $input->getArgument('key');

        $this->bus->beginTransaction();

        $item = $this->bus->getItemForProcessing($key);

        if (!$item) {
            $msg = "Key not found";
            $this->log->error($msg, [$key, __CLASS__, __LINE__]);
            $this->bus->rollback();
            throw new BusException($msg);
        }

        $email = $item->getEmail();

        try {
            $data = $this->contactsManager->getContactByEmail($email);
        } catch (Exception $e) {
            $this->log->debug('Contact not found', [$key, $email, __CLASS__, __LINE__]);
            $this->bus->markForRetrial($key);
            $this->bus->commit();
            return;
        }

        $contact = $data['result'];

        $isOk = $this->hasCredits($contact) && $this->hasPackages($contact) && $this->isActive($contact);

        if (!$isOk) {
            $this->log->debug('Not enough credits', [$key, $email, __CLASS__, __LINE__]);
            $this->bus->markForRetrial($key);
            $this->bus->commit();
            return;
        }

        $updatePayload = $this->createUpdatePayload($item, $contact);

        try {
            $this->contactsManager->updateContactByEmail($email, $updatePayload);
        } catch (Exception $e) {
            $this->log->error($e->getMessage(), [$key, $email, __CLASS__, __LINE__]);
            $this->bus->markForRetrial($key);
            $this->bus->commit();
            return;
        }


        $flow = $item->getFlow();

        if ($flow === Contact::LOGIN_FLOW_AUTOLOGIN) {
            $emailTemplateId = $this->emailTemplateIdRegistration;
            $existingToken = $this->getExistingToken($contact);
            $customData = [];
            if ($existingToken) {
                $customData['token'] = $existingToken;
            }
            $url = $item->getAutologinLink($this->urlBookEditorAuthToken, $customData);
            $logMessage = 'Autologin email sent';
        } else { // basic
            $emailTemplateId = $this->emailTemplateIdForgotPassword;
            $url = $item->getForgotPasswordLink($this->urlBookEditorForgotPassword);
            $logMessage = 'Forgot password email sent';
        }

        $properties = [
            [
                'name' => 'url',
                'value' => $url,
            ],
        ];

        try {
            if ($flow !== Contact::LOGIN_FLOW_AUTOLOGIN) { // currently autologin email is disabled, remove this conditional to enable it
                $this->contactsManager->sendEmailSingleSend($emailTemplateId, $email, $properties, $this->apiKeyEmail);
                $this->log->debug($logMessage, [$key, $email, __CLASS__, __LINE__]);
            }
        } catch (Exception $e) {
            $this->log->error($e->getMessage(), [$key, $email, __CLASS__, __LINE__]);
            $this->bus->markForRetrial($key);
            $this->bus->commit();
            return;
        }

        $this->log->debug('Registration successful', [$key, $email, __CLASS__, __LINE__]);
        $this->bus->finish($key);
        $this->bus->commit();
    }

    private function getExistingToken($contact)
    {
        return $contact['properties']['afy_customer_login_token']['value'];
    }

    private function createUpdatePayload(BusItemInterface $busItem, array $contact)
    {
        $flow = $busItem->getFlow();
        $data = $busItem->getData();
        if ($flow === Contact::LOGIN_FLOW_AUTOLOGIN) {
            $existingToken = $this->getExistingToken($contact);
            $payload = [
                'afy_password' => $data['password'],
                'afy_password_encrypted' => $data['encryptedPassword'],
                'afy_customer_login_flow' => $flow,
            ];
            if ($existingToken === '') {
                $payload['afy_customer_login_token'] = $data['token'];
            }

            return $payload;
        }

        if ($flow === Contact::LOGIN_FLOW_BASIC) {
            return [
                'passcode' => $data['passcode'],
                'afy_customer_login_flow' => $flow,
            ];
        }

        return [];
    }
}
