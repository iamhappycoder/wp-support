<?php

namespace Authorify\Command;

use Authorify\Model\Bus\BusAbstract;
use Authorify\Model\Bus\BusItemFactory;
use Authorify\Provider\HubSpot\Model\Contact;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateRegistrationCommand extends Command
{
    protected static $defaultName = 'authorify:registration:create-registration';

    private BusAbstract $bus;

    public function __construct(BusAbstract $busService)
    {
        parent::__construct();
        $this->setDescription('Arguments will be passed directly to the corresponding factory method for the selected flow.');
        $this->bus = $busService;
    }

    private function getFlows()
    {
        return implode(', ', Contact::LOGIN_FLOWS);
    }

    protected function configure()
    {
        $this->addArgument('flow', InputArgument::REQUIRED, "One of [{$this->getFlows()}]");
        $this->addArgument('arguments', InputArgument::IS_ARRAY);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $arguments = $input->getArgument('arguments');
        $flow = $input->getArgument('flow');

        if (!in_array($flow, Contact::LOGIN_FLOWS)) {
            $output->writeln("<error>Unknown flow: {$flow}. Valid flows are: {$this->getFlows()}</error>");
            return 1;
        }

        $item = BusItemFactory::fromFlow($flow, ...$arguments);
        $this->bus->queueItem($item);
    }
}
