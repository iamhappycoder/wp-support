<?php

namespace Authorify\Model;

class ServiceResult
{
    const RESULT_SUCCESS = 'success';
    const RESULT_FAILURE = 'failure';

    /** @var string */
    protected $status;

    /** @var string */
    protected $message;

    /** @var string */
    protected $code;

    /** @var mixed[] */
    protected $data;

    public function __construct(
        string $status,
        string $message = null,
        string $code = null
    ) {
        $this->status = $status;
        $this->message = $message;
        $this->code = $code;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return mixed[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(
        array $data
    ): self {
        $this->data = $data;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'status' => $this->status,
            'message' => $this->message,
            'code' => $this->code,
            'data' => $this->data
        ];
    }
}
