<?php

namespace Authorify\Model\Bus;

interface BusItemInterface
{
    const NOTFOUND = 'notfound';
    const CREATED = 'created';
    const PROCESSING = 'processing';
    const READY = 'ready';
    const FAILED = 'failed';

    const STATUSES = [
        self::NOTFOUND => self::NOTFOUND,
        self::CREATED => self::CREATED,
        self::PROCESSING => self::PROCESSING,
        self::READY => self::READY,
        self::FAILED => self::FAILED,
    ];

    public function __construct(string $email, array $data, string $id = null, ?string $status = null, ?\DateTimeImmutable $createdAt = null);

    public function getId(): string;

    public function getEmail(): string;

    public function getData(): array;

    public function setData(array $data): self;

    public function getCreatedAt(): ?\DateTimeImmutable;

    public function setStatus(string $status): self;

    public function getStatus(): string;

    public function getFlow(): string;

    public static function createData(array $data): array;

}
