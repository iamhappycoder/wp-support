<?php

namespace Authorify\Model\Bus;

use Authorify\Provider\HubSpot\Model\Contact;

final class BusItemAutologin extends BusItemAbstract
{
    protected static function getRequiredKeys(): array
    {
        return ['password', 'encryptedPassword', 'token'];
    }

    public function getAutologinLink($baseUrl, $customData = [])
    {
        $data = array_merge($this->getData(), $customData);
        $queryString = http_build_query(['username' => $this->getEmail(), 'token' => $data['token']]);

        return "{$baseUrl}?{$queryString}";
    }

    public function getFlow(): string
    {
        return Contact::LOGIN_FLOW_AUTOLOGIN;
    }
}
