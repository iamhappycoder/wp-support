<?php
namespace Authorify\Model\Bus;

use Ramsey\Uuid\Uuid;

abstract class BusItemAbstract implements BusItemInterface
{
    private \DateTimeImmutable $createdAt;

    private string $status;

    private string $email;

    private array $data;

    private string $id;

    public function __construct(string $email, array $data, ?string $id = null, ?string $status = null, ?\DateTimeImmutable $createdAt = null)
    {
        $this->email = $email;
        $this->setData($data);
        $this->createdAt = $createdAt ?: new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
        $this->id = $id ?: Uuid::uuid4();
        $this->setStatus($status ?: BusItemInterface::CREATED);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    abstract protected static function getRequiredKeys() : array;

    public static function createData(array $data): array {
        $requiredKeys = static::getRequiredKeys();
        $intersection = array_intersect_key($requiredKeys, array_keys($data));
        $diff = array_diff_key($requiredKeys, $intersection);
        if ($diff) {
            throw new BusException("Missing keys: " . implode(', ', $diff));
        }

        $result = [];
        foreach ($requiredKeys as $key) {
            $result[$key] = $data[$key];
        }

        return $result;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): BusItemInterface
    {
        $this->data = self::createData($data);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setStatus(string $status): BusItemInterface
    {
        if (!in_array($status, BusItemAbstract::STATUSES)) {
            throw new BusException("Unknown status: {$status}");
        }
        $this->status = $status;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}