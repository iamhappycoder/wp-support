<?php

namespace Authorify\Model\Bus;

use Authorify\Provider\HubSpot\Model\Contact;

final class BusItemBasic extends BusItemAbstract
{
    protected static function getRequiredKeys(): array
    {
        return ['passcode'];
    }

    public function getForgotPasswordLink($baseUrl)
    {
        $data = $this->getData();
        $queryString = http_build_query(['username' => $this->getEmail(), 'passcode' => $data['passcode']]);

        return "{$baseUrl}?{$queryString}";
    }

    public function getFlow(): string
    {
        return Contact::LOGIN_FLOW_BASIC;
    }
}
