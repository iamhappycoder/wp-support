<?php
namespace Authorify\Model\Bus;

use Authorify\Provider\HubSpot\Model\Contact;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class BusItemFactory
{
    public static function createBusItemForAutoLogin($email, $password): BusItemAutologin
    {
        $encryptedPassword = password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);
        $token = Uuid::uuid4()->toString();
        $data = [
            'password' => $password,
            'encryptedPassword' => $encryptedPassword,
            'token' => $token,
        ];
        return new BusItemAutologin($email, $data);
    }

    public static function createBusItemForBasic($email): BusItemBasic
    {
        $data = [
            'passcode' => md5(uniqid(random_bytes(20))),
        ];

        return new BusItemBasic($email, $data);

    }

    public static function fromFlow($flow, ...$arguments): BusItemInterface
    {
        if ($flow === Contact::LOGIN_FLOW_AUTOLOGIN) {
            return self::createBusItemForAutoLogin(...$arguments);
        }
        return self::createBusItemForBasic(...$arguments);
    }

    public static function fromArray(array $data): BusItemInterface
    {
        $busItemData = $data['data'];
        if (!is_array($data['data'])) {
            $busItemData = json_decode($data['data'], JSON_OBJECT_AS_ARRAY);
        }
        if ($data['created_at']) {
            $createdAt = new DateTimeImmutable($data['created_at'], new \DateTimeZone('UTC'));
        } else {
            $createdAt = new DateTimeImmutable('now', new \DateTimeZone('UTC'));
        }
        if ($data['flow'] === Contact::LOGIN_FLOW_AUTOLOGIN) {
            return new BusItemAutologin($data['email'], BusItemAutologin::createData($busItemData), $data['id'], $data['status'], $createdAt);
        } else {
            return new BusItemBasic($data['email'], BusItemBasic::createData($busItemData), $data['id'], $data['status'], $createdAt);
        }
    }
}
