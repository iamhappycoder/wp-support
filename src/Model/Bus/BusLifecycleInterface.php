<?php
namespace Authorify\Model\Bus;

interface BusLifecycleInterface
{
    public function exec(BusItemInterface $busItem);

    public function getItemForProcessing(string $id): ?BusItemInterface;

    public function delete(string $id);

    public function finish(string $id);

    /**
     * @return BusItemInterface[]
     */
    public function getQueue(): array;

    public function markForRetrial(string $id);

    public function markExpiredAsFailed($maxAge = '5 minutes');

    public function garbageCollect($maxAge = '2 hours');

    public function beginTransaction();

    public function commit();

    public function rollback();
}
