<?php

namespace Authorify\Model\Bus;

interface BusInterface
{
    public function queueItem(BusItemInterface $busItem);

    public function readItem(string $email, string $flow): ?BusItemInterface;
}
