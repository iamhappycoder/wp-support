<?php

namespace Authorify\Provider\HubSpot;

use Authorify\Exception\ClientException;
use Authorify\Exception\ServerException;

class Client
{
    public const ENDPOINT_DOMAIN_API = 'api.hubapi.com';
    public const ENDPOINT_DOMAIN_FORMS = 'forms.hubspot.com';

    /**
     *  Form submissions is successful.
     */
    public const FORMS_STATUS_SUCCESS = 204;

    /**
     *  Form submissions is successful and a redirectUrl is included or set in the form settings.
     */
    public const FORMS_STATUS_SUCCESS_REDIRECT = 302;

    /**
     *  Form GUID is not found for the provided Portal ID.
     */
    public const FORMS_STATUS_NOT_FOUND = 404;

    /**
     *  Form submission results in an internal server error.
     */
    public const FORMS_STATUS_ERROR = 500;

    protected $apiKey;

    public function __construct(string $hubSpotApiKey)
    {
        $this->apiKey = $hubSpotApiKey;
    }

    public function request($endpoint, $payload, $headers, $alternativeApiKey = null)
    {
        $this->appendApiKey($endpoint, $alternativeApiKey);

        $ch = @curl_init();

        if ($payload) {
            @curl_setopt($ch, CURLOPT_POST, true);
            @curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        }

        @curl_setopt($ch, CURLOPT_URL, $endpoint);

        if ($headers) {
            @curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = @curl_exec($ch);
        $statusCode = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = @curl_error($ch);

        @curl_close($ch);

        if ($err) {
            throw new ClientException($err, 0);
        }

        $result = json_decode($result, true);

        if ($statusCode >= 400 && $statusCode <= 599) {
            throw new ServerException($result['message'], $statusCode);
        }

        $response = [
            'result' => $result,
            'statusCode' => $statusCode
        ];

        return $response;
    }

    public function appendApiKey(string &$endpoint, $alternativeApiKey = null): void
    {
        if (strstr($endpoint, self::ENDPOINT_DOMAIN_API)) {
            if ($alternativeApiKey) {
                $apiKey = $alternativeApiKey;
            } else {
                $apiKey = $this->apiKey;
            }
            $endpoint .= (isset(parse_url($endpoint)['query']) ? '&' : '?') . 'hapikey=' . $apiKey;
        }
    }
}
