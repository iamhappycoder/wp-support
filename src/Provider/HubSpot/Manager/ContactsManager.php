<?php

namespace Authorify\Provider\HubSpot\Manager;

class ContactsManager extends AbstractManager
{
    public function getContactByEmail(string $email): array
    {
        $endpoint = $this->buildEndpoint('contacts', 'v1', 'contact/email/' . $email . '/profile');
        $payload = null;
        $headers = [];

        $response = $this->client->request($endpoint, $payload, $headers);

        return $response;
    }

    public function updateContactByEmail(string $email, array $properties): array
    {
        $endpoint = $this->buildEndpoint('contacts', 'v1', 'contact/email/' . $email . '/profile');

        $payload = ['properties' => []];
        foreach ($properties as $property => $value) {
            $payload['properties'][] = ['property' => $property, 'value' => $value];
        }

        $headers = ['Content-Type: application/json'];
        $response = $this->client->request($endpoint, json_encode($payload), $headers);

        return $response;
    }

    public function createContact(string $email, array $properties): array
    {
        $endpoint = $this->buildEndpoint('contacts', 'v1', 'contact');

        $properties['email'] = $email;

        $payload = ['properties' => []];
        foreach ($properties as $property => $value) {
            $payload['properties'][] = ['property' => $property, 'value' => $value];
        }

        $headers = ['Content-Type: application/json'];
        $response = $this->client->request($endpoint, json_encode($payload), $headers);

        return $response;
    }

    public function createContactProperty(array $properties): array
    {
        $endpoint = $this->buildEndpoint('properties', 'v1', 'contacts/properties');

        $headers = ['Content-Type: application/json'];
        $response = $this->client->request($endpoint, json_encode($properties), $headers);

        return $response;
    }

    public function sendEmailSingleSend($templateId, $to, array $customProperties, $apiKey = null)
    {
        $emailMetadata = [
            'emailId' => $templateId,
            'message' => [
                'to' => $to,
            ],
            'customProperties' => $customProperties,
        ];

        $endpoint = $this->buildEndpoint('email', 'public/v1', 'singleEmail/send');

        $headers = ['Content-Type: application/json'];
        $response = $this->client->request($endpoint, json_encode($emailMetadata), $headers, $apiKey);

        return $response;
    }
}
