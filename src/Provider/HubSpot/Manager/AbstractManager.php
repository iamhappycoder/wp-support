<?php

namespace Authorify\Provider\HubSpot\Manager;

use Authorify\Provider\HubSpot\Client;

abstract class AbstractManager
{
    /** @var Client */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    protected function buildEndpoint(string $api, string $version, string $path, array $params = []): string
    {
        return sprintf('https://%s/%s/%s/%s%s', Client::ENDPOINT_DOMAIN_API, $api, $version, $path, !empty($params) ? '?' . http_build_query($params) : '');
    }
}
