<?php

namespace Authorify\Provider\HubSpot\Model;

class Contact
{
    const LOGIN_FLOW_BASIC = 'basic';
    const LOGIN_FLOW_AUTOLOGIN = 'auto-login';

    const LOGIN_FLOWS = [
        self::LOGIN_FLOW_BASIC => self::LOGIN_FLOW_BASIC,
        self::LOGIN_FLOW_AUTOLOGIN => self::LOGIN_FLOW_AUTOLOGIN,
    ];

    protected $properties;

    public function __construct(array $properties)
    {
        $this->properties = [
            'email' => null,
            'firstname' => null,
            'lastname' => null,
            'phone' => null,
        ];

        foreach ($this->properties as $key => &$value) {
            $value = $properties[$key]['value'] ?? null;
        }
    }

    public function getEmail(): ?string
    {
        return $this->properties['email'];
    }

    public function getFirstName(): ?string
    {
        return $this->properties['firstname'];
    }

    public function getLastName(): ?string
    {
        return $this->properties['lastname'];
    }

    public function getPhone(): ?string
    {
        return $this->properties['phone'];
    }
}
