<?php

namespace Authorify\Provider\HubSpot\Validator;

class FormValidator
{
    protected static $validationMessages = [
        'zipCode' => 'Please include a 5-digit zip code for the zip field.',
        'email' => 'Please include a properly formatted email address for the email field.',
        'phoneNumber' => 'Please include an 11-digit phone number for the phone field.',
        'best_meet_time' => 'Please select best time to meet.'
    ];

    public function validate($fields, $data)
    {
        $result = ['missing' => [], 'invalid' => [], 'valid' => []];

        foreach ($fields as $field => $config) {
            $isRequired = isset($config['required']) ? $config['required'] : false;
            if ($isRequired && (!isset($data[$field]) || empty($data[$field]))) {
                $result['missing'][] = $field;

                continue;
            }

            if (isset($data[$field])) {
                $value = $data[$field];

                $isValid = true;
                $validation = isset($config['validation']) ? $config['validation'] : 'none';

                //
                // TODO: simplify by eliminating this switch statement.
                //
                switch ($validation) {
                    case 'zipCode':
                        $isValid = $this->validateZipCode($value);

                        break;

                    case 'email':
                        $isValid = $this->validateEmail($value);

                        break;

                    case 'phoneNumber':
                        $validatedPhoneNumber = $this->validatePhoneNumber($value);

                        $isValid = $validatedPhoneNumber !== false;
                        if ($isValid) {
                            $value = $validatedPhoneNumber;
                        }

                        break;

                    case 'best_meet_time':
                        $isValid = $this->validateSelection($value, $config['values']);
                        break;

                    case 'none':
                        // Fall through default.

                    default:
                        $result['valid'][$field] = $data[$field];
                }

                if ($isValid) {
                    $result['valid'][$field] = $value;
                } else {
                    $result['invalid'][$field] = isset($config['validationMessage']) ?
                        $config['validationMessage'] :
                        self::$validationMessages[$config['validation']];
                }

                unset($data['field']);
            }
        }

        return $result;
    }

    protected function validateZipCode($zipCode)
    {
        return is_numeric($zipCode) && mb_strlen($zipCode) == 5;
    }

    protected function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function validatePhoneNumber($phoneNumber)
    {
        $validatedPhoneNumber = preg_replace('/[^0-9]+/', '', $phoneNumber);

        $length = mb_strlen($validatedPhoneNumber);
        if ($length == 10 || $length == 11) {
            if ($length == 11) {
                $validatedPhoneNumber = substr($validatedPhoneNumber, 1);
            }

            $validatedPhoneNumber = '+1' . $validatedPhoneNumber;

            return $validatedPhoneNumber;
        } else {
            return false;
        }
    }

    protected function validateSelection(string $needle, array $haystack) : bool
    {
        return in_array($needle, $haystack);
    }
}