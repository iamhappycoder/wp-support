<?php

namespace Authorify\Provider\LimeLight\Model;

class CreditCard
{
    public const TYPE_AMEX = 'amex';
    public const TYPE_DISCOVER = 'discover';
    public const TYPE_MASTERCARD = 'master';
    public const TYPE_VISA = 'visa';

    public const MASK_CHARACTER = 'X';

    /** @var string */
    protected $type;

    /** @var string */
    protected $number;

    /** @var \DateTimeImmutable */
    protected $expiration;

    /** @var string */
    protected $cvv;

    public function __construct(
        string $type = null,
        string $number = null,
        \DateTimeImmutable $expiration = null,
        string $cvv = null
    ) {
        $this->type = $type;

        $this->setNumber($number);

        $this->number = $number;
        $this->expiration = $expiration;
        $this->cvv = $cvv;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNumber(bool $isMasked = false): ?string
    {
        if ($isMasked) {
            $length = strlen($this->number);
            $lastFourDigits = substr($this->number, -4);
            $this->number = str_pad($lastFourDigits, $length, self::MASK_CHARACTER, STR_PAD_LEFT);
        }

        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        if ($number !== null) {
            switch ($number[0]) {
                case '2':
                    // Fall through '5'
                case '5':
                    $this->type = self::TYPE_MASTERCARD;
                    break;
                case '3':
                    $this->type = self::TYPE_AMEX;
                    break;
                case '4':
                    $this->type = self::TYPE_VISA;
                    break;
                case '6':
                    $this->type = self::TYPE_DISCOVER;
                    break;
                default:
                    throw new \InvalidArgumentException('Cannot identify card type.');
            }
        }

        $this->number = $number;

        return $this;
    }

    public function getExpiration(): ?\DateTimeImmutable
    {
        return $this->expiration;
    }

    public function setExpiration(?\DateTimeImmutable $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }
    
    public function getCvv(bool $isMasked = false): ?string
    {
        if ($isMasked) {
            $this->cvv = str_repeat(
                self::MASK_CHARACTER,
                strlen($this->cvv)
            );
        }

        return $this->cvv;
    }

    public function setCvv(?string $cvv): self
    {
        $this->cvv = $cvv;

        return $this;
    }
}