<?php

namespace Authorify\Provider\LimeLight\Model;

class Order
{
    const STATUS_DECLINED = '7';

    /** @var int */
    protected $previousOrderId;

    /** @var int */
    protected $shippingId;

    /** @var string */
    protected $transactionType;

    /** @var string */
    protected $ipAddress;

    /** @var int */
    protected $campaignId;

    /** @var bool */
    protected $isBillingSameAsShipping;

    /** @var Customer */
    protected $customer;

    /** @var Product[] */
    protected $products;

    /** @var string */
    protected $affiliateId;

    /** @var Product */
    protected $productBump;

    /** @var string[] */
    protected $utm;

    public function __construct(
        int $shippingId = null,
        string $transactionType = null,
        string $ipAddress = null,
        int $campaignId = null,
        bool $isBillingSameAsShipping = null,
        Customer $customer = null,

        /** @var Product[] */
        array $products = []
    ) {
        $this->shippingId = $shippingId;

        $this->transactionType = ($transactionType === null) ? 'Sale' : $transactionType;

        $this->ipAddress = $ipAddress;
        $this->campaignId = $campaignId;
        $this->isBillingSameAsShipping = $isBillingSameAsShipping;

        $this->customer = $customer;
        $this->products = $products;

        $this->productBump = null;
        $this->utm = [];
    }

    public function getShippingId(): ?int
    {
        return  $this->shippingId;
    }

    public function setShippingId(?int $shippingId): self
    {
        $this->shippingId = $shippingId;

        return $this;
    }

    public function getTransactionType(): ?string
    {
        return $this->transactionType;
    }

    public function setTransactionType(?string $transactionType): self
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getCampaignId(): ?int
    {
        return $this->campaignId;
    }

    public function setCampaignId(?int $campaignId): self
    {
        $this->campaignId = $campaignId;

        return $this;

    }

    public function isBillingSameAsShipping(): ?bool
    {
        return $this->isBillingSameAsShipping;
    }

    public function setIsBillingSameAsShipping(?bool $isBillingSameAsShipping): self
    {
        $this->isBillingSameAsShipping = $isBillingSameAsShipping;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function setProducts(
        /** @var Product[] */
        array $products
    ): self {
        $this->products = $products;

        return $this;
    }

    public function getPreviousOrderId(): ?int
    {
        return  $this->previousOrderId;
    }

    public function setPreviousOrderId(?int $previousOrderId): self
    {
        $this->previousOrderId = $previousOrderId;

        return $this;
    }

    public function getAffiliateId(): ?string
    {
        return $this->affiliateId;
    }

    public function setAffiliateId(?string $affilidateId): self
    {
        $this->affiliateId = $affilidateId;

        return $this;
    }

    public function getProductBump(): ?Product
    {
        return $this->productBump;
    }

    public function setProductBump(Product $productBump): self
    {
        $this->productBump = $productBump;

        return $this;
    }

    public function getAllUtm(): array
    {
        return $this->utm;
    }

    public function addUtm(string $name, string $value): self
    {
        $this->utm[$name] = $value;

        return $this;
    }
}