<?php

namespace Authorify\Provider\LimeLight\Model;

class Product
{
    /** @var int */
    protected $id;

    /** @var int */
    protected $offerId;

    /** @var int */
    protected $billingModelId;

    /** @var int */
    protected $quantity;

    /** @var int */
    protected $trialProductId;

    public function __construct(
        int $id = null,
        int $offerId = null,
        int $billingModelId = null,
        int $quantity = null,
        int $trialProductId = null
    ) {
        $this->id = $id;
        $this->offerId = $offerId;
        $this->billingModelId = $billingModelId;
        $this->quantity = $quantity;
        $this->trialProductId = $trialProductId;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOfferId(): ?int
    {
        return $this->offerId;
    }

    public function setOfferId(?int $offerId): self
    {
        $this->offerId = $offerId;

        return $this;
    }

    public function getBillingModelId(): ?int
    {
        return $this->billingModelId;
    }

    public function setBillingModelId(?int $billingModelId): self
    {
        $this->billingModelId = $billingModelId;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTrialProductId(): ?int
    {
        return $this->trialProductId;
    }

    public function setTrialProductId(?int $trialProductid): self
    {
        $this->trialProductId = $trialProductid;

        return $this;
    }
}