<?php

namespace Authorify\Provider\LimeLight\Model;

class Customer
{
    /** @var string */
    protected $email;

    /** @var string */
    protected $firstName;

    /** @var string */
    protected $lastName;

    /** @var string */
    protected $phone;

    /** @var CreditCard[] */
    protected $creditCards;

    /** @var Address[] */
    protected $addresses;

    /**
     * @param CreditCard[] $creditCards
     * @param Address[] $addresses
     */
    public function __construct(
        string $email = null,
        string $firstName= null,
        string $lastName= null,
        string $phone= null,
        array $creditCards = [],
        array $addresses = []
    ) {
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;
        $this->creditCards = $creditCards;
        $this->addresses = $addresses;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }
    
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }
    
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        
        return $this;
    }
    
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;
        
        return $this;
    }
    
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        
        return $this;
    }

    /**
     * @return CreditCard[]
     */
    public function getCreditCards(): array
    {
        return $this->creditCards;
    }

    public function setCreditCards(array $creditCards): self
    {
        $this->creditCards = $creditCards;
        return $this;
    }

    /**
     * @return Address[]
     */
    public function getAddresses(): array
    {
        return $this->addresses;
    }

    /**
     * @param Address[] $addresses
     *
     * @return self
     */
    public function setAddresses(array $addresses): self
    {
        $this->addresses = $addresses;

        return $this;
    }
}