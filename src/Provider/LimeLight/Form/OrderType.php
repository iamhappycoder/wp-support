<?php

namespace Authorify\Provider\LimeLight\Form;

use Authorify\Provider\LimeLight\Model\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('shippingId')
            ->add('transactionType')
            ->add('ipAddress')
            ->add('campaignId')
            ->add('isBillingSameAsShipping', CheckboxType::class, ['label' => 'Billing same as shipping', 'required' => false])

            ->add('customer', CustomerType::class)
            ->add('products', CollectionType::class, ['entry_type' => ProductType::class, 'allow_add' => true])

            ->add('placeOrder', SubmitType::class, ['label' => 'Place Order']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class
        ]);
    }
}