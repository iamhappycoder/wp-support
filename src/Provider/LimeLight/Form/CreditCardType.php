<?php

namespace Authorify\Provider\LimeLight\Form;

use Authorify\Provider\LimeLight\Model\CreditCard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type')
            ->add('number')
            ->add('expiration')
            ->add('cvv');

        $builder->get('expiration')
            ->addModelTransformer(
                new CallbackTransformer(
                    function (?\DateTimeImmutable $expiration) {
                        return null === $expiration ? '' : $expiration->format('my');
                    },
                    function (?string $expiration) {
                        return \DateTimeImmutable::createFromFormat('my', $expiration, new \DateTimeZone('UTC'));
                    }
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreditCard::class
        ]);
    }
}