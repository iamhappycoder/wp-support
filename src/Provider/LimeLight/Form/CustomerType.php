<?php

namespace Authorify\Provider\LimeLight\Form;

use Authorify\Provider\LimeLight\Model\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('firstName')
            ->add('lastName')
            ->add('phone')
            ->add('creditCards', CollectionType::class, ['entry_type' => CreditCardType::class, 'allow_add' => true])
            ->add('addresses', CollectionType::class, ['entry_type' => AddressType::class, 'allow_add' => true]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class
        ]);
    }
}