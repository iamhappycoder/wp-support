<?php

namespace Authorify\Provider\LimeLight\Manager;

use Authorify\Provider\LimeLight\Client;
use Authorify\Provider\LimeLight\Model\Order;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrderManager extends AbstractManager
{
    protected static $utmSubAffiliateMapper = [
        'utm_source' => 'AFFID',
        'utm_campaign' => 'C1',
        'utm_medium' => 'C2',
        'utm_term' => 'C3',
    ];

    public function __construct(Client $client, TranslatorInterface $translator)
    {
        parent::__construct($client, $translator);
    }

    public function create(
        Order $order
    ): array {
        $params = [];

        $customer = $order->getCustomer();
        $params['firstName'] = $customer->getFirstName();
        $params['lastName'] = $customer->getLastName();
        $params['phone'] = $customer->getPhone();
        $params['email'] = $customer->getEmail();

        $creditCard = $customer->getCreditCards()[0];
        $params['creditCardType'] = $creditCard->getType();
        $params['creditCardNumber'] = $creditCard->getNumber();
        $params['expirationDate'] = $creditCard->getExpiration()->format('my');
        $params['CVV'] = $creditCard->getCvv();

        foreach ($customer->getAddresses() as $address) {
            $type = $address->getType();

            $params[$type . 'FirstName'] = $address->getFirstName();
            $params[$type . 'LastName'] = $address->getLastName();
            $params[$type . 'Address1'] = $address->getAddress1();

            if ('' !== $address->getAddress2()) {
                $params[$type . 'Address2'] = $address->getAddress2();
            }

            $params[$type . 'City'] = $address->getCity();
            $params[$type . 'State'] = $address->getState();
            $params[$type . 'Zip'] = $address->getZip();
            $params[$type . 'Country'] = $address->getCountry();
        }

        $params['shippingId'] = $order->getShippingId();
        $params['tranType'] = $order->getTransactionType();
        $params['ipAddress'] = $order->getIpAddress();
        $params['campaignId'] = $order->getCampaignId();
        $params['billingSameAsShipping'] = $order->isBillingSameAsShipping() ? 'YES' : 'NO';

        if ($order->getAffiliateId()) {
            $params['AFID'] = $order->getAffiliateId();
        }

        foreach ($order->getAllUtm() as $utmName => $utmValue) {
            $params[self::$utmSubAffiliateMapper[$utmName]] = $utmValue;
        }

        $params['products'] = [];
        foreach ($order->getProducts() as $product) {
            $params['products'][$product->getId()] = [
                'offer_id' => $product->getOfferId(),
                'billing_model_id' => $product->getBillingModelId(),
                'quantity' => 1,
            ];

            if ($product->getTrialProductId()) {
                $params['products'][$product->getId()]['trial'] = [
                    'product_id' => $product->getTrialProductId(),
                ];
            }
        }

        if ($productBump = $order->getProductBump()) {
            $params['products'][$productBump->getId()] = [
                'offer_id' => $productBump->getOfferId(),
                'billing_model_id' => $productBump->getBillingModelId(),
                'quantity' => 1,
            ];
        }

        return $this->client->request('new_order', $params);
    }

    public function addon(
        Order $order
    ): array {
        $params = [];

        $params['campaignId'] = $order->getCampaignId();
        $params['shippingId'] = $order->getShippingId();
        $params['previousOrderId'] = $order->getPreviousOrderId();

        $products = [];
        foreach ($order->getProducts() as $product) {
            $products[$product->getId()] = [
                'offer_id' => $product->getOfferId(),
                'billing_model_id' => $product->getBillingModelId(),
                'quantity' => 1,
            ];
        }
        $params['products'] = $products;

        $params['initializeNewSubscription'] = 1;

        return $this->client->request('new_order_card_on_file', $params);
    }

    public function calculateTotal(
        array $params,
        int $productQuantityOverride = null
    ): array {
        $offers = [];
        foreach ($params['offers'] as $offer) {
            $offers[] = [
                'id' => $offer['id'],
                'product_id' => $offer['productId'],
                'billing_model_id' => $offer['billingModelId'],
                'quantity' => 1,
            ];
        }

        $clientParams = [
            'campaign_id' => $params['campaignId'],
            'shipping_id' => $params['shippingId'],
            'use_tax_provider' => '1',
            'offers' => $offers,
            'location' => [
                'state' => $params['location']['state'],
                'country' => $params['location']['country'],
                'postal_code' => $params['location']['zip']
            ]
        ];

        return $this->client->request('order_total/calculate', $clientParams, '2');
    }

    public function find(string $email, array $criteria = []): array
    {
        $criteria['email'] = $email;

        $params = [
            'start_date' => '01/01/1971',
            'end_date' => '12/31/2099',
            'campaign_id' => 'all',
            'criteria' => $criteria,
            'search_type' => 'all',
            'return_type' => 'order_view',
        ];

        $response = $this->client->request('order_find', $params);

        return $response;
    }

    public function findRecurring(string $email): array
    {
        return $this->find($email, ['recurring' => '1']);
    }

    public function findOverdue(string $email): array
    {
        return $this->find($email, ['declines' => '1']);
    }

    public function update(
        int $orderId,
        array $customerInfo,
        array $billingInfo,
        array $paymentInfo
    ): array {
        if (empty($billingInfo['billing_address2'])) {
            unset($billingInfo['billing_address2']);
        }

        $params = [
            'order_id' => [
                $orderId => array_merge($customerInfo, $billingInfo, $paymentInfo),
            ],
        ];

        return $this->client->request('order_update', $params);
    }

    public function view(
        int $orderId
    ): array {
        $params = [
            'order_id' => $orderId,
        ];

        return $this->client->request('order_view', $params);
    }

    public function forceBill(int $orderId) : array
    {
        return $this->client->request('order_force_bill', ['order_id' => $orderId]);
    }

    public function updateSubscription(
        int $orderId,
        int $productId
    ): array {
        $params = [
            'order_id' => $orderId,
            'product_id' => $productId,
            'status' => 'reset',
        ];

        return $this->client->request('subscription_order_update', $params);
    }
}