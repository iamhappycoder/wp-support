<?php

namespace Authorify\Provider\LimeLight\Manager;

use Authorify\Provider\LimeLight\Client;
use Symfony\Contracts\Translation\TranslatorInterface;

class PaymentsManager extends AbstractManager
{
    public function __construct(Client $client, TranslatorInterface $translator)
    {
        parent::__construct($client, $translator);
    }

    public function authorize($params): array
    {
        return $this->client->request('authorize_payment', $params);
    }
}