<?php

namespace Authorify\Provider\LimeLight\Manager;

use Authorify\Provider\LimeLight\Client;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractManager
{
    /** @var Client */
    protected $client;

    /** @var TranslatorInterface */
    protected $translator;

    public function __construct(Client $client, TranslatorInterface $translator)
    {
        $this->client = $client;
        $this->translator = $translator;
    }
}