<?php

namespace Authorify\Provider\LimeLight\Manager;

class CustomerManager extends AbstractManager
{
    public function find(string $email): array
    {
        $params = [
            'campaign_id' => 'all',
            'start_date' => '01/01/1971',
            'end_date' => '12/31/2099',
            'criteria' => [
                'email' => $email,
            ],
            'search_type' => 'all',
            'return_type' => 'customer_view',
        ];

        return $this->client->request('customer_find', $params);
    }
}