<?php

namespace Authorify\Provider\LimeLight\Manager;


use Authorify\Provider\LimeLight\Client;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductsManager extends AbstractManager
{
    public function __construct(Client $client, TranslatorInterface $translator)
    {
        parent::__construct($client, $translator);
    }

    public function getById(int $id) : array
    {
        $params = [
            'product_id' => [$id]
        ];

        return $this->client->request('product_index', $params);
    }
}