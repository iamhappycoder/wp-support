<?php

namespace Authorify\Provider\LimeLight\Exception;

use Throwable;

class ServerException extends \Authorify\Exception\ServerException
{
    protected $declineReason;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, string $declineReason = null)
    {
        parent::__construct($message, $code, $previous);

        $this->declineReason = $declineReason;
    }

    public function getDeclineReason(): ?string
    {
        return $this->declineReason;
    }
}