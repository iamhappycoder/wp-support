<?php

namespace Authorify\Provider\LimeLight;

use Authorify\Exception\TransportException;
use Authorify\Provider\LimeLight\Exception\ClientException;
use Authorify\Provider\LimeLight\Exception\ServerException;
use Authorify\Transport\HttpTransport;
use Authorify\Transport\TransportInterface;
use Psr\Log\LoggerInterface;

class Client
{
    const BASE_ENDPOINT = 'https://authorify.limelightcrm.com/api';

    /** @var TransportInterface */
    protected $transport;

    /** @var LoggerInterface */
    protected $limeLightLogger;

    protected $username;
    protected $password;

    public function __construct(
        TransportInterface $transport,
        LoggerInterface $limeLightLogger,
        string $limeLightApiUsername,
        string $limeLightApiPassword
    ) {
        $this->transport = $transport;
        $this->limeLightLogger = $limeLightLogger;

        $this->username = $limeLightApiUsername;
        $this->password = $limeLightApiPassword;
    }

    /**
     * @throws ClientException
     * @throws ServerException
     */
    public function request(string $method, array $params, string $version = '1'): array
    {
        $url = self::BASE_ENDPOINT . '/v' . $version . '/' . $method;
        try {
            $response = $this->transport->request(
                HttpTransport::METHOD_POST,
                $url,
                $params,
                ['Content-Type: application/json'],
                $this->username,
                $this->password
            );
        } catch (TransportException $te) {
            throw new ClientException($te->getMessage());
        }

        $response = json_decode($response, true);

        list ($errorCode, $errorMessage) = $this->parseError($method, $version, $response);

//        if ($errorCode > 100) {
//            $log = json_encode([
//                'url' => $url,
//                'params' => $params,
//                'response' => $response,
//            ]);
//
//            $this->limeLightLogger->debug($log);
//        }

        if ($errorMessage) {
            throw new ServerException($errorMessage, $errorCode,null, $errorMessage);
        }

        return $response;
    }

    private function parseError(string $method, int $version, array $response): array
    {
        $errorCode = null;
        $errorMessage = null;

        switch ($version) {
            case 1:
                if ($response['response_code'] == 10669) {
                    $errorMessage = 'API user is not authorized to access campaign';
                } elseif (isset($response['error_found']) && $response['error_found'] == '1') {
                    $errorMessageIndex = null;
                    if (isset($response['decline_reason'])) {
                        $errorMessageIndex = 'decline_reason';
                    } elseif (isset($response['error_message'])) {
                        $errorMessageIndex = 'error_message';
                    }

                    //
                    // order_view returns decline_reason which is actually part of the order and not as a result of the request being declined.
                    // For this reason, we use error_message instead.
                    //
                    if ($method == 'order_view') {
                        $errorMessageIndex = 'error_message';
                    }

                    $errorMessage = str_replace('errorFound=0', '', $response[$errorMessageIndex]);
                }

                $errorCode = $response['response_code'];
                break;

            case 2:
                if ($response['status'] == 'FAILURE') {
                    $errorCode = 0;
                    $errorMessage = array_values($response['data'])[0][0];
                }
                break;

            default:
                // Do nothing.
        }

        return [$errorCode, $errorMessage];
    }
}