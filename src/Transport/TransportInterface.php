<?php

namespace Authorify\Transport;

use Authorify\Exception\TransportException;

interface TransportInterface
{
    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @param array|null $headers
     * @param string|null $username
     * @param string|null $password
     *
     * @return string
     *
     * @throws TransportException
     */
    public function request(string $method, string $url, array $params = null, array $headers = null, string $username = null, string $password = null): string;
}