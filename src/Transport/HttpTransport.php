<?php

namespace Authorify\Transport;

use Authorify\Exception\TransportException;

class HttpTransport implements TransportInterface
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    public function request(string $method, string $url, array $params = null, array $headers = null, string $username = null, string $password = null): string
    {
        $curl = curl_init();

        $opts = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        ];

        if ($username && $password) {
            $opts[CURLOPT_USERPWD] = $username . ':' . $password;
        }

        if ($method == self::METHOD_POST) {
            $opts[CURLOPT_CUSTOMREQUEST] = $method;
        }

        if ($params) {
            $opts[CURLOPT_POSTFIELDS] = json_encode($params);
        }

        if ($headers) {
            $opts[CURLOPT_HTTPHEADER] = $headers;
        }

        curl_setopt_array(
            $curl,
            $opts
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new TransportException($err);
        }

        return $response;
    }
}