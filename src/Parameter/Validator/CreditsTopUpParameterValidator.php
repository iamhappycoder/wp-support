<?php

namespace Authorify\Parameter\Validator;

use Authorify\Parameter\Validator\LimeLight\CreateOrderParameterValidator;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class CreditsTopUpParameterValidator extends CreateOrderParameterValidator
{
    public function getConstraintsList($params): array
    {
        $constraintsList = parent::getConstraintsList($params);

        $constraintsList['required']['credits'] = [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
        ];

        return $constraintsList;
    }
}
