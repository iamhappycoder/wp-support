<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\Country;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

abstract class AbstractCreditCardParameterValidator extends AbstractParameterValidator
{
    /**
     * NOTE: dirty hack, fix when there's time.
     *
     * @var string
     */
    protected $stateType = 'usstatecode';
    protected $zipType = 'uszip';

    public function preValidate(array &$params): void
    {
        if (isset($params['phone'])) {
            $params['phone'] = preg_replace('/[^0-9]/', '', $params['phone']);
        }

        $params['expirationDate'] = '';
        if (isset($params['expirationMonth']) && isset($params['expirationYear'])) {
            $params['expirationDate'] = sprintf('20%s-%02s-01', $params['expirationYear'], $params['expirationMonth']);
        }
        unset($params['expirationMonth'], $params['expirationYear']);

        if (isset($params['billingCountry'])) {
            $params['billingCountry'] = strtoupper($params['billingCountry']);

            // NOTE: dirty hack, fix when there's time.
            $this->stateType = strtolower($params['billingCountry']) . 'statecode';
            $this->zipType = strtolower($params['billingCountry']) . 'zip';
        }

        if (isset($params['billingState'])) {
            $params['billingState'] = strtoupper($params['billingState']);
        }

        if (!isset($params['billingAddress2'])) {
            $params['billingAddress2'] = '';
        }


        $params = array_merge(
            ['firstName' => null, 'lastName' => null, 'phone' => null, 'email' => null, 'billingAddress1' => null, 'billingAddress2' => null, 'billingCity' => null, 'billingCountry' => null, 'billingState' => null, 'billingZip' => null, 'creditCardNumber' => null, 'expirationDate' => null, 'cvv' => null],
            $params
        );
    }

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'firstName' => [
                    new NotBlank(),
                    new Length(['max' => 50]),
                ],
                'lastName' => [
                    new NotBlank(),
                    new Length(['max' => 50]),
                ],
                'phone' => [
                    new NotBlank(),
                    new Length(['min' => 10, 'max' => 11])
                ],
                'email' => [
                    new NotBlank(),
                    new Email(),
                ],
                'creditCardNumber' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                    new Length(['min' => 12, 'max' => 19])
                ],
                'expirationDate' => [
                    new NotBlank(),
                    new Date()
                ],
                'cvv' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'billingAddress1' => [
                    new NotBlank(),
                ],
                'billingCity' => [
                    new NotBlank(),
                ],
                'billingCountry' => [
                    new NotBlank(),
                    new Country(),
                ],
                'billingState' => [
                    new NotBlank(),
                    new Type(['type' => $this->stateType]),
                ],
                'billingZip' => [
                    new NotBlank(),
                    new Type(['type' => $this->zipType]),
                ],
            ],
            'optional' => [
                'billingAddress2' => [],
                'errorFormat' => [],
            ],
        ];
    }

    public function postValidate(array &$params): void
    {
        $params['expirationDate'] = new \DateTimeImmutable($params['expirationDate'], new \DateTimeZone('UTC'));
    }
}
