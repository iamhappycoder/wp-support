<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class AddonOrderParameterValidator extends AbstractParameterValidator
{
    public function preValidate(array &$params): void
    {
        if (isset($params['productId']) && !is_array($params['productId']) &&
            isset($params['productQty']) && !is_array($params['productQty'])) {
            $productId = $params['productId'];

            $params['productId'] = [$productId];
            $params['productQty'] = [$productId => $params['productQty']];
        }
    }

    public function getTranslationId(string $key): string
    {
        return 'orders.addon.' . $key;
    }

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'email' => [
                    new NotBlank(),
                    new Email(),
                ],
                'previousOrderId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'productId' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        'constraints' => [
                            new NotBlank(),
                            new Type(['type' => 'digit']),
                        ],
                    ])
                ],
                'productQty' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        'constraints' => [
                            new NotBlank(),
                            new Type(['type' => 'digit']),
                        ],
                    ]),
                ],
                'offerId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'billingModelId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'shippingId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'campaignId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ]
            ],
            'optional' => [
            ],
        ];
    }

    public function postValidate(array &$params): void {}
}
