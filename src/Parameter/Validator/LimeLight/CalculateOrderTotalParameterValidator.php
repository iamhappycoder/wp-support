<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Country;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class CalculateOrderTotalParameterValidator extends AbstractParameterValidator
{
    /**
     * NOTE: dirty hack, fix when there's time.
     *
     * @var string
     */
    protected $stateType = 'usstatecode';
    protected $zipType = 'uszip';

    public function preValidate(array &$params): void
    {
        if (isset($params['location'])) {
            if (isset($params['location']['country'])) {
                $params['location']['country'] = strtoupper($params['location']['country']);

                // NOTE: dirty hack, fix when there's time.
                $this->stateType = strtolower($params['location']['country']) . 'statecode';
                $this->zipType = strtolower($params['location']['country']) . 'zip';
            }

            if (isset($params['location']['state'])) {
                $params['location']['state'] = strtoupper($params['location']['state']);
            }
        }
    }

    public function getTranslationId(string $key): string
    {
        return 'orders.calculate.total.' . $key;
    }

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'campaignId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'shippingId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'offers' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        new Collection([
                            'id' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ],
                            'productId' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ],
                            'billingModelId' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ],
                            'productQty' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ]
                        ])
                    ])
                ],
                'location' => [
                    new Collection([
                        'state' => [
                            new NotBlank(),
                            new Type(['type' => $this->stateType]),
                        ],
                        'country' => [
                            new NotBlank(),
                            new Country()
                        ],
                        'zip' => [
                            new NotBlank(),
                            new Type(['type' => $this->zipType]),
                        ],
                    ])
                ],
            ],
            'optional' => [
            ],
        ];
    }

    public function postValidate(array &$params): void
    {
        // TODO: Implement postValidate() method.
    }
}
