<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class GetProductByIdParameterValidator extends AbstractParameterValidator
{
    public function getTranslationId(string $key): string
    {
        return 'products.' . $key;
    }

    public function preValidate(array &$params): void {}

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'id' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ]
            ],

            'optional' => [

            ],
        ];
    }

    public function postValidate(array &$params): void {}
}
