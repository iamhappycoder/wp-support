<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class FindOrderParameterValidator extends AbstractParameterValidator
{
    public function preValidate(array &$params): void
    {
        // Do nothing.
    }

    public function getTranslationId(string $key): string
    {
        return 'orders.find.' . $key;
    }

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'email' => [
                    new NotBlank(),
                    new Email(),
                ],
                'campaignId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
            ],
            'optional' => [
            ],
        ];
    }

    public function postValidate(array &$params): void
    {
        // Do nothing.
    }
}
