<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;
use Authorify\Provider\HubSpot\Model\Contact;

class CreateOrderWithRegistrationParameterValidator extends CreateOrderParameterValidator
{
    public function getConstraintsList($params): array
    {
        $constraintsList = parent::getConstraintsList($params);

        if (isset($params['flow']) && $params['flow'] === 'auto-login') {
            $constraintsList['required'] = array_merge(
                $constraintsList['required'],
                [
                    'password' => [
                        new NotBlank(),
                        new Length(['min' => 8]),
                    ],
                    'confirmPassword' => [
                        new NotBlank(),
                        new EqualTo(['value' => $params['password']]),
                    ]
                ],
            );
        }

        $constraintsList['optional'] = array_merge(
            $constraintsList['optional'],
            [
                'flow' => [
                    new Choice(['choices' => Contact::LOGIN_FLOWS]),
                ],
            ]
        );

        return $constraintsList;
    }

    public function postValidate(array &$params): void
    {
        parent::postValidate($params);

        $params['flow'] ??='basic';
    }
}
