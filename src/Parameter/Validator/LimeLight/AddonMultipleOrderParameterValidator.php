<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class AddonMultipleOrderParameterValidator extends AbstractParameterValidator
{
    public function preValidate(array &$params): void
    {
        foreach ($params['offers'] as $productId => &$offer) {
            $offer['productId'] = (string)$productId;
        }
        $params['offers'] = array_values($params['offers']);
    }

    public function getTranslationId(string $key): string
    {
        return 'orders.addon-multiple.' . $key;
    }

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'offers' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        new Collection([
                            'offerId' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ],
                            'productId' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ],
                            'billingModelId' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ],
                            'productQty' => [
                                new NotBlank(),
                                new Type(['type' => 'digit']),
                            ]
                        ])
                    ])
                ],
                'email' => [
                    new NotBlank(),
                    new Email(),
                ],
                'previousOrderId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'shippingId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'campaignId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ]
            ],
            'optional' => [
            ],
        ];
    }

    public function postValidate(array &$params): void {}
}
