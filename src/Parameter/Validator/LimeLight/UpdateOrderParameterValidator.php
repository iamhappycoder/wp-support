<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Symfony\Component\Validator\Constraints\Choice;

class UpdateOrderParameterValidator extends AbstractCreditCardParameterValidator
{
    public function getTranslationId(string $key): string
    {
        return 'orders.update.' . $key;
    }

    public function getConstraintsList($params): array
    {
        $constraintList = parent::getConstraintsList($params);
        $constraintList['optional']['isCalculateOnly'] = [
                    new Choice(['choices' => ['0', '1']]),
        ];

        return $constraintList;
    }

    public function postValidate(array &$params): void
    {
        parent::postValidate($params);

        $params['isCalculateOnly'] = isset($params['isCalculateOnly']) ? !!$params['isCalculateOnly'] : false;
    }
}
