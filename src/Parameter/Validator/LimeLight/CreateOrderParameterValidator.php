<?php

namespace Authorify\Parameter\Validator\LimeLight;

use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class CreateOrderParameterValidator extends AbstractCreditCardParameterValidator
{
    public function preValidate(array &$params): void
    {
        parent::preValidate($params);

        if (!isset($params['affiliateId'])) {
            $params['affiliateId'] = '';
        }

        if (isset($params['productId']) && !is_array($params['productId']) &&
            isset($params['productQty']) && !is_array($params['productQty'])) {
            $productId = $params['productId'];

            $params['productId'] = [$productId];
            $params['productQty'] = [$productId => $params['productQty']];
        }

        if (isset($params['trialProductId'])) {
            if (!is_array($params['trialProductId'])) {
                $params['trialProductId'] = [$params['productId'][0] => $params['trialProductId']];
            }

            foreach ($params['trialProductId'] as $productId => $trialProductId) {
                if (!$trialProductId) {
                    unset($params['trialProductId'][$productId]);
                }
            }

            if (empty($params['trialProductId'])) {
                unset($params['trialProductId']);
            }
        }
    }

    public function getTranslationId(string $key): string
    {
        return 'orders.' . $key;
    }

    public function getConstraintsList($params): array
    {
        $constraintsList = parent::getConstraintsList($params);
        $constraintsList['required'] = array_merge(
            $constraintsList['required'],
            [
                'productId' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        'constraints' => [
                            new NotBlank(),
                            new Type(['type' => 'digit']),
                        ],
                    ])
                ],
                'productQty' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        'constraints' => [
                            new NotBlank(),
                            new Type(['type' => 'digit']),
                        ],
                    ]),
                ],
                'offerId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'billingModelId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'shippingId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'campaignId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
            ]
        );

        $constraintsList['optional'] = array_merge(
            $constraintsList['optional'],
            [
                'affiliateId' => [],

                'productBumpId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'productBumpQty' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'productBumpOfferId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'productBumpBillingModelId' => [
                    new NotBlank(),
                    new Type(['type' => 'digit']),
                ],
                'trialProductId' => [
                    new NotBlank(),
                    new Type(['type' => 'array']),
                    new All([
                        'constraints' => [
                            new NotBlank(),
                            new Type(['type' => 'digit']),
                        ],
                    ])
                ],
                'utm_source' => [
                    new Regex(['pattern' => '/[?&=#%]/', 'match' => false]),
                ],
                'utm_campaign' => [
                    new Regex(['pattern' => '/[?&=#%]/', 'match' => false]),
                ],
                'utm_medium' => [
                    new Regex(['pattern' => '/[?&=#%]/', 'match' => false]),
                ],
                'utm_term' => [
                    new Regex(['pattern' => '/[?&=#%]/', 'match' => false]),
                ]
            ]
        );

        return $constraintsList;
    }
}
