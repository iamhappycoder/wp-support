<?php

namespace Authorify\Parameter\Validator;

use Authorify\Exception\Validation\InvalidParametersException;
use Authorify\Exception\Validation\MissingParametersException;
use Authorify\Exception\Validation\UnrecognizedParametersException;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractParameterValidator
{
    /** @var ValidatorInterface */
    protected $validator;

    abstract public function preValidate(array &$params): void;
    abstract public function getTranslationId(string $key): string;
    abstract public function getConstraintsList(array $params): array;
    abstract public function postValidate(array &$params): void;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @throws InvalidParametersException
     * @throws MissingParametersException
     * @throws UnrecognizedParametersException
     */
    public function validate(array &$params): void
    {
        $this->preValidate($params);

        $validationData = ['missing' => [], 'invalid' => [], 'unrecognized' => [],];
        $constraintsList = $this->getConstraintsList($params);

        //
        // Find missing required parameters.
        //
        foreach ($constraintsList['required'] as $key => $value) {
            if (!array_key_exists($key, $params)) {
                $validationData['missing'][] = $this->getTranslationId($key);
            }
        }

        //
        // Validate parameters.
        //
        foreach ($params as $key => $value) {
            if (array_key_exists($key, $constraintsList['required']) || array_key_exists($key, $constraintsList['optional'])) {
                $constraints = $constraintsList['required'][$key] ?? $constraintsList['optional'][$key];

                try {
                    $violations = $this->validator->validate($value, $constraints);

                    if (count($violations)) {
                        foreach ($violations as $violation) {
                            $keys = $key;
                            if ($violation->getPropertyPath()) {
                                $propertyPath = new PropertyPath($violation->getPropertyPath());
                                foreach ($propertyPath->getElements() as $element) {
                                    if (!is_numeric($element)) {
                                        $keys .= '.' . $element;
                                    }
                                }
                            }

                            $translationId = $this->getTranslationId($keys);
                            if (!in_array($translationId, $validationData['invalid'])) {
                                $validationData['invalid'][] = $translationId;
                            }
                        }
                    }
                } catch (UnexpectedTypeException $unexpectedTypeException) {
                    $validationData['invalid'][] = $this->getTranslationId($key);
                }
            } else {
                $validationData['unrecognized'][] = $key;
            }
        }

        if ($validationData['missing']) {
            throw new MissingParametersException($validationData['missing']);
        } elseif ($validationData['invalid']) {
            throw new InvalidParametersException($validationData['invalid']);
        } elseif ($validationData['unrecognized']) {
            throw new UnrecognizedParametersException($validationData['unrecognized']);
        }

        $this->postValidate($params);
    }
}
