<?php

namespace Authorify\Parameter\Validator\Helper;

use Authorify\Parameter\Validator\AbstractParameterValidator;
use Symfony\Component\Validator\Constraints\Country;
use Symfony\Component\Validator\Constraints\NotBlank;

class ListStatesParameterValidator extends AbstractParameterValidator
{
    public function preValidate(array &$params): void
    {
        $params['countryCode'] = strtoupper($params['countryCode']);
    }

    public function getTranslationId(string $key): string
    {
        return 'states.' . $key;
    }

    public function getConstraintsList($params): array
    {
        return [
            'required' => [
                'countryCode' => [
                    new NotBlank(),
                    new Country()
                ],
            ],

            'optional' => [

            ],
        ];
    }

    public function postValidate(array &$params): void {}
}
