<?php

namespace Authorify\Formatter;

use Authorify\Provider\LimeLight\Model\CreditCard;
use Monolog\Formatter\LineFormatter;

class LimeLightFormatter extends LineFormatter
{
    public function format(array $record)
    {
        $data = json_decode($record['message'], true);
        $creditCard = new CreditCard(null, $data['params']['creditCardNumber'], null, $data['params']['CVV']);

        $data['params']['creditCardNumber'] = $creditCard->getNumber(true);
        $data['params']['CVV'] = $creditCard->getCvv(true);

        $record['message'] = json_encode($data);

        return parent::format($record);
    }
}
